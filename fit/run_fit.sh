#!/bin/bash

if (( $# != 22 )); then
    exit -1
fi

BATCH="BATCH$1"
LOAD="LOAD$2"

INPUT_FOLDER="$3"
DATA_FOLDER="$4"
APP_FOLDER="$5"

MAPPING1="$6"
MAPPING2="$7"

RUNS=( "${@:8}" )

HV=(1.10kV 1.05kV 1.00kV 0.95kV 0.90kV 0.85kV)

# The following line redirects stdout and error messages. Can be changed.
# Do not redirect stdin. It used for communication with the GUI
LOG_FILE=${INPUT_FOLDER}/log/fitter_${RUN_NUMBER}.log
exec 1>>"${LOG_FILE}" 2>>"${LOG_FILE}"

# Change dir manually to the current folder
# cd /home/lhcbuser/lib-proxy
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# echo $DIR
cd "$DIR" || return 1

# echo $@

# Create configuration file
CONF_FILE="${DATA_FOLDER}/offline_monitor_${BATCH}_${LOAD}.conf"
touch "${CONF_FILE}"

echo "# Config file for monitor" > "${CONF_FILE}"
{
    echo "ConfName Run ${BATCH} ${LOAD}"
    echo "Workdir ${DATA_FOLDER}"
    echo "Datasheets ${APP_FOLDER}/Datasheets"
    if [[ ${MAPPING1} == "2i" ]]; then
        echo "2inchPMT TRUE"
    else
        echo "2inchPMT FALSE"
    fi

    echo "RunAnalysis yes"

    for (( i = 6; i < 15; i++ )); do
        if (( ${RUNS[i]} == 0 )); then
            continue
        fi
        echo "Run ${RUNS[i]} Dark"
    done

    printf "\n"

    for (( i = 0; i < 6; i++ )); do
        if (( ${RUNS[i]} == 0 )); then
            continue
        fi
        echo "Run ${RUNS[i]} LED ${HV[i-6]}"
    done

    printf "\n"
} >> "${CONF_FILE}"

./fit.sh "${DATA_FOLDER}" ${RUNS[@]}

../monitor/offline/bin/offline_monitor "${CONF_FILE}"

# not needed
# cp "${INPUT_FOLDER}/Offline_monitor_summary.pdf" "${DATA_FOLDER}/Offline_monitor_summary_${BATCH}_${LOAD}.pdf"

evince "${DATA_FOLDER}/Offline_monitor_analysis_summary_Run_${BATCH}_${LOAD}.pdf" &
