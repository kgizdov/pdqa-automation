#!/bin/bash

# Helper
usage() { echo "Usage: $0 datadir run1 run2 ..." 1>&2; exit 1; }

# Get PMTs from run log
get_pmts() {
  run=$1
  pmts=($(awk -v string='PMT' 'NR!=1 {print $5;}' "$datadir/$run/log/run_environment.log";));  # parse run log file
  pmts=($(echo "${pmts[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' '))  # remove duplicates (in case of 2inch PMTs)
}

# Fit dir
fitdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# External arguments
datadir=$1
runs=${@:1,2}
if [ -z "${datadir}" ] || [ -z "${runs}" ]; then usage; fi

# Start fit
echo ====================================
echo Fit to MaPMTs is started!
echo ====================================
echo Data path: $datadir
echo Number of runs to be processed: ${#runs[@]}
for run in ${runs[@]}; do echo Run $run; done

# Loop over runs
for run in ${runs[@]}; do

  # Get PMTs of the run
  get_pmts $run
  echo ""
  echo Fitting run $run...
  echo Number of PMTs to be processed: ${#pmts[@]}
  for pmt in ${pmts[@]}; do echo PMT $pmt; done

  # Loop over PMTs
  for pmt in ${pmts[@]}; do
    # Run fit
    rundir="${datadir}/${run}"
    printf -v run_num "%06d" "${run}"  # zero-pad run number
    "/${fitdir}/bin/fit" "${pmt}" "${rundir}/${pmt}_${run_num}.root" "${rundir}" &> "$rundir/log/fit_$pmt.log" &
  done
  wait # wait until all PMTs are done
done
