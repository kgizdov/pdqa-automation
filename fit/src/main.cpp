#include "Fit.h"

//========================
// Run Online Monitor
//========================
int main(int argc, char **argv) {
  if (argc < 4) {
    std::cout << "[ERROR] main(): Missing arguments!" << std::endl;
    std::cout << "[ERROR] main(): Usage: ./fit PMT_NAME INPUT_FILE OUTPUT_DIR [PIXEL]" << std::endl;
    abort();
  }

  // The fit
  Fit *fit = new Fit();
  fit->setPMT(argv[1]);
  fit->setInputFile(argv[2]);
  fit->setOutputDir(argv[3]);
  if (argc == 5) fit->setPixel(atoi(argv[4]));

  // Run fit
  fit->run();

  return 0;
}
