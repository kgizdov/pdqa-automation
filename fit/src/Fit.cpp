#include "Fit.h"

/////////////////
// Constructor //
/////////////////
Fit::Fit() {
  std::cout << "==================================================" << std::endl;
  std::cout << " MaPMT Fit                                        " << std::endl;
  std::cout << "==================================================" << std::endl;

  // Silent RooFit (stdout warnings/errors only)
  RooMsgService::instance().setGlobalKillBelow(RooFit::WARNING);
}


////////////////
// Destructor //
////////////////
Fit::~Fit() {}


////////////////////
// Set input data //
////////////////////
void Fit::setInputFile(const TString &file) {
  if (gSystem->AccessPathName(file)) {
    std::cout << "[ERROR] setInputData(): " << file << " does not exist! Exit!" << std::endl;
    abort();
  }
  m_inputFile = file;
  std::cout << "[INFO] Set input file: " << file << std::endl;
}


///////////////////////
// Set output folder //
///////////////////////
void Fit::setOutputDir(const TString &dir) {
  std::cout << "[INFO] Set output folder: " << dir << std::endl;
  m_outputDir = dir;
}


/////////////////////////////////
// Set branches of output file //
/////////////////////////////////
void Fit::setBranches() {
  // Create tree
  m_tree = new TTree("tree", "");

  // Set branches
  m_tree->Branch("npx", &m_npx, "npx/I");

  m_tree->Branch("status", &m_status, "status[npx]/I");
  m_tree->Branch("chi2", &m_chi2, "chi2[npx]/F");

  m_tree->Branch("navgph", &m_navgph, "navgph[npx]/F");
  m_tree->Branch("err_navgph", &m_err_navgph, "err_navgph[npx]/F");
  m_tree->Branch("dnavgph", &m_dnavgph, "dnavgph[npx]/F");

  m_tree->Branch("navgct", &m_navgct, "navgct[npx]/F");
  m_tree->Branch("err_navgct", &m_err_navgct, "err_navgct[npx]/F");
  m_tree->Branch("dnavgct", &m_dnavgct, "dnavgct[npx]/F");

  m_tree->Branch("mean_n", &m_mean_n, "mean_n[npx]/F");
  m_tree->Branch("err_mean_n", &m_err_mean_n, "err_mean_n[npx]/F");
  m_tree->Branch("dmean_n", &m_dmean_n, "dmean_n[npx]/F");

  m_tree->Branch("sigma_n", &m_sigma_n, "sigma_n[npx]/F");
  m_tree->Branch("err_sigma_n", &m_err_sigma_n, "err_sigma_n[npx]/F");
  m_tree->Branch("dsigma_n", &m_dsigma_n, "dsigma_n[npx]/F");

  m_tree->Branch("mean_1ph", &m_mean_1ph, "mean_1ph[npx]/F");
  m_tree->Branch("err_mean_1ph", &m_err_mean_1ph, "err_mean_1ph[npx]/F");
  m_tree->Branch("dmean_1ph", &m_dmean_1ph, "dmean_1ph[npx]/F");

  m_tree->Branch("sigma_1ph", &m_sigma_1ph, "sigma_1ph[npx]/F");
  m_tree->Branch("err_sigma_1ph", &m_err_sigma_1ph, "err_sigma_1ph[npx]/F");
  m_tree->Branch("dsigma_1ph", &m_dsigma_1ph, "dsigma_1ph[npx]/F");

  m_tree->Branch("mean_1ct", &m_mean_1ct, "mean_1ct[npx]/F");
  m_tree->Branch("err_mean_1ct", &m_err_mean_1ct, "err_mean_1ct[npx]/F");
  m_tree->Branch("dmean_1ct", &m_dmean_1ct, "dmean_1ct[npx]/F");

  m_tree->Branch("sigma_1ct", &m_sigma_1ct, "sigma_1ct[npx]/F");
  m_tree->Branch("err_sigma_1ct", &m_err_sigma_1ct, "err_sigma_1ct[npx]/F");
  m_tree->Branch("dsigma_1ct", &m_dsigma_1ct, "dsigma_1ct[npx]/F");

  m_tree->Branch("Pmiss", &m_Pmiss, "Pmiss[npx]/F");
  m_tree->Branch("err_Pmiss", &m_err_Pmiss, "err_Pmiss[npx]/F");
  m_tree->Branch("dPmiss", &m_dPmiss, "dPmiss[npx]/F");

  m_tree->Branch("pv_ratio", &m_pv_ratio, "pv_ratio[npx]/F");
  m_tree->Branch("dpv_ratio", &m_dpv_ratio, "dpv_ratio[npx]/F");

  m_tree->Branch("mean_1ph_raw", &m_mean_1ph_raw, "mean_1ph_raw[npx]/F");
  m_tree->Branch("mean_sig_pdf", &m_mean_sig_pdf, "mean_sig_pdf[npx]/F");
}


//////////////////
// Set PMT name //
//////////////////
void Fit::setPMT(const TString &pmt) {
  m_pmt = pmt;
  std::cout << "[INFO] Set PMT: " << pmt << std::endl;
}


///////////////
// Set pixel //
///////////////
void Fit::setPixel(const int ipx) {
  if (ipx != -1 && (ipx < 1 || ipx > NPX)) {
    std::cout << "[ERROR] setPixel(): Wrong pixel number!" << std::endl;
    abort();
  }
  m_ipx = ipx;
  std::cout << "[INFO] Analyzing pixel: " << ((ipx == -1) ? "all" : std::to_string(ipx)) << std::endl;
}


//////////////////////////////////////
// Return prefix of output filename //
//////////////////////////////////////
TString Fit::outputPrefix() {
  TString prefix = "fit_" + m_pmt;
  if (m_ipx != -1) prefix += Form("_px%i", m_ipx);
  return prefix;
}


/////////////////
// Run the fit //
/////////////////
void Fit::run() {
  // Start!
  time_t start, end;
  time(&start);

  // Create output file
  m_outFile = new TFile(m_outputDir + "/" + outputPrefix() + ".root", "RECREATE");

  // Set branches
  setBranches();

  // Fit!
  if (m_ipx != -1) fit(m_ipx);
  else {
    for (int ipx = 1; ipx <= NPX; ipx++) fit(ipx);
  }

  // Save and close output file
  m_outFile->cd();
  m_tree->Fill();
  m_tree->Write();
  m_outFile->Close();

  // Stop!
  time(&end);
  int timing = difftime(end, start);

  // Print timing
  std::cout << std::endl;
  std::cout << "[INFO] Fitting time: " << int(timing / 60) << " min"
       << " and " << timing % 60 << " sec" << std::endl;

  std::cout << "[INFO] ----- End of program -----" << std::endl;
  std::cout << std::endl;
}
