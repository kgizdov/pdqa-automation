#ifndef FIT_H
#define FIT_H 1

// C++
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>

// ROOT
#include "TROOT.h"
#include "TSystem.h"
#include "TFile.h"
#include "TTree.h"
#include "TPaveStats.h"
#include "TPad.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TH1F.h"
#include "TF1.h"
#include "TMath.h"
#include "TSpectrum.h"
#include "TChain.h"
#include "TAxis.h"
#include "TLatex.h"
#include "TPolyMarker.h"

// RooFit
// #ifndef __CINT__
// #include "RooGlobalFunc.h" // togliere?
// #endif
#include "RooRealVar.h"
#include "RooConstVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooGaussian.h"
#include "RooConstVar.h"
#include "RooAddPdf.h"
#include "RooPlot.h"
#include "RooHist.h"
#include "RooProduct.h"
#include "RooFormulaVar.h"
#include "RooFitResult.h"
#include "RooCustomizer.h"
#include "RooFFTConvPdf.h"
#include "RooAbsMoment.h"

#define NPX 64  // Number of pixels

class Fit {
 public:
    // Constructor/destructor
    Fit();
    ~Fit();

    // Set PMT name
    void setPMT(const TString &pmt);

    // Set input file
    void setInputFile(const TString &file);

    // Set output directory
    void setOutputDir(const TString &dir);

    // Set pixel number (1..64)
    void setPixel(const int ipx);

    // Run the fit
    void run();

 private:
    // Run the fit for a pixel
    void fit(const int ipx);

    // Set branches of output file
    void setBranches();

    // Return prefix of output filename
    TString outputPrefix();

    // Members
    int m_ipx = -1;        // pixel to fit (-1 means all pixels)
    TFile *m_outFile;      // output file
    TTree *m_tree;         // output tree
    TString m_outputDir;   // output directory
    TString m_inputFile;   // input file
    TString m_pmt;         // PMT name

    Int_t m_npx = NPX;     // Move to a struct FitResult. Move to int and float!!!
    Int_t m_status[NPX] = {0};
    Float_t m_chi2[NPX] = {0.};
    Float_t m_pv_ratio[NPX] = {0.};
    Float_t m_dpv_ratio[NPX] = {0.};
    Float_t m_mean_1ph_raw[NPX] = {0.};
    Float_t m_mean_sig_pdf[NPX] = {0.};

    Float_t m_navgph[NPX] = {0.};
    Float_t m_dnavgph[NPX] = {0.};
    Float_t m_err_navgph[NPX] = {0.};

    Float_t m_navgct[NPX] = {0.};
    Float_t m_dnavgct[NPX] = {0.};
    Float_t m_err_navgct[NPX] = {0.};

    Float_t m_mean_n[NPX] = {0.};
    Float_t m_dmean_n[NPX] = {0.};
    Float_t m_err_mean_n[NPX] = {0.};

    Float_t m_sigma_n[NPX] = {0.};
    Float_t m_dsigma_n[NPX] = {0.};
    Float_t m_err_sigma_n[NPX] = {0.};

    Float_t m_mean_1ph[NPX] = {0.};
    Float_t m_dmean_1ph[NPX] = {0.};
    Float_t m_err_mean_1ph[NPX] = {0.};

    Float_t m_sigma_1ph[NPX] = {0.};
    Float_t m_dsigma_1ph[NPX] = {0.};
    Float_t m_err_sigma_1ph[NPX] = {0.};

    Float_t m_mean_1ct[NPX] = {0.};
    Float_t m_dmean_1ct[NPX] = {0.};
    Float_t m_err_mean_1ct[NPX] = {0.};

    Float_t m_sigma_1ct[NPX] = {0.};
    Float_t m_dsigma_1ct[NPX] = {0.};
    Float_t m_err_sigma_1ct[NPX] = {0.};

    Float_t m_Pmiss[NPX] = {0.};
    Float_t m_dPmiss[NPX] = {0.};
    Float_t m_err_Pmiss[NPX] = {0.};
};
#endif
