// Copyright 2016 Stefano Gallorini University of Padua
// 2016 Konstantin Gizdov University of Edinburgh

#include <iostream>
#include <string>

#include "../include/gain.hpp"


int main(int argc, char const *argv[]) {
    if (argc != 3) {
        std::cerr << "Usage " << argv[0] << " <DATASHEET DIR> <PMT NUMBER>" << std::endl;
        return -1;
    } else {
        const TString top_dir(argv[1]);
        const std::string pmt_id(argv[2]);
        unsigned gain = readDatasheets(top_dir, pmt_id);
        std::cout << gain << std::endl;
        return gain;
    }
    std::cout << "0" << std::endl;
    return 0;
}
