// Copyright 2016 Stefano Gallorini University of Padua
// 2016 Konstantin Gizdov University of Edinburgh

#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include <TString.h>

#include "../include/gain.hpp"

////////////////////
// Read datasheet //
////////////////////
uint32_t readDatasheet(const TString &datasheet, const std::string &pmt_id) {
  const float p0 = 334.8;             // MAROC gain vs datasheet gain fit parameters
  const float p1 = -0.6927;
  const size_t nDataPerPmt = 7 + NPX;  // data per PMT
  std::string line;
  std::vector<std::string> bad_headers = {"parameter", "unit", "error(max)", "failure"};

  std::ifstream file(datasheet, std::ios::in);
  if (!file.good()) {
    std::cerr << "[ERROR] Monitor::readDatasheet(): Cannot find datasheet: "
      << datasheet << std::endl;
    abort();
  }

  // Read datasheet
  std::cerr << "[INFO] Reading datasheet: " << datasheet << std::endl;
  while (std::getline(file, line)) {
    if (line.empty()) continue;  // Skip empty line

    // Tokenize line
    std::istringstream iss(line);
    std::string token;
    std::vector<std::string> tokens;
    while (std::getline(iss, token, ',')) {
      if (token != "") {
        token.erase(std::remove_if(token.begin(), token.end(), isspace), token.end());
        tokens.push_back(token);
      }
    }
    size_t nTokens = tokens.size();
    if (nTokens == 0) continue;  // Skip line w/o tokens

    // Skip lines we don't want
    std::string header = tokens[0];
    if (std::find(bad_headers.begin(), bad_headers.end(), header) != bad_headers.end())
      continue;
    std::cerr << "Found PMT " << header << std::endl;
    if (header != pmt_id) continue;

    // Load datasheet specs
    if (nTokens == nDataPerPmt) {
      std::cerr << "Gain is " << (p0 / (std::stod(tokens[5]) - p1)) << std::endl;
      return (p0 / (std::stod(tokens[5]) - p1));
    } else {
      std::cerr << "[ERROR] Monitor::readDatasheet(): Cannot parse datasheet! Check csv format!" << std::endl;
      std::cerr << "[ERROR] Monitor::readDatasheet(): Cannot parse this line: " << line << std::endl;
      std::cerr << "[ERROR] Monitor::readDatasheet(): Expected tokens = " << nDataPerPmt
                << ", found = " << nTokens << std::endl;
      // abort();
    }
  }  // file
  return 0;
}


//////////////////////////////////////////////////////////
// Read all datasheets recursively from a top directory //
//////////////////////////////////////////////////////////
uint32_t readDatasheets(const TString &path, const std::string &pmt_id) {
  DIR *top_dir = opendir(path.Data());
  struct dirent *ent = readdir(top_dir);
  struct dirent *sub_ent = nullptr;

  std::cerr << "[INFO] Reading all datasheets from: " << path << std::endl;
  while ((ent = readdir(top_dir)) != nullptr) {
    if (ent->d_type == DT_DIR) {  // it should be a dir
      // Get dir name
      TString dir_name = path + "/" + TString(ent->d_name);
      if (dir_name.Contains("/..")) continue;  // Skip .. folders

      // Look inside the subdir
      DIR *sub_dir = opendir(dir_name.Data());
      while ((sub_ent = readdir(sub_dir)) != nullptr)  {
        if (sub_ent->d_type != DT_DIR) {  // it's should be a file
          TString file_name = dir_name + "/" + TString(sub_ent->d_name);

          // Read datasheet in .csv format
          if (file_name.Contains(".csv")) {
            uint32_t gain = readDatasheet(file_name, pmt_id);
            if (gain != 0) return gain;  // return gain asap
          }
        }
      }
      closedir(sub_dir);
    }
  }
  closedir(top_dir);
  return 0;
}
