// Copyright 2016 Stefano Gallorini University of Padua
// 2016 Konstantin Gizdov University of Edinburgh

// C inc
#include <dirent.h>
#include <stdio.h>

// C++ inc
#include <string>

#include <TString.h>

// Useful constants
#define PMTDIM 8  // PMT side dimension (in pixels)
#define NPX PMTDIM*PMTDIM  // Number of pixels of PMT

// Read datseheet (.csv format)
uint32_t readDatasheet(const TString &filename, const std::string &pmt_id);

// Read all datasheets recursively from a top directory
uint32_t readDatasheets(const TString &top_dir, const std::string &pmt_id);
