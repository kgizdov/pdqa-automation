#!/bin/bash

if [[ $# != 3 ]]; then
    exit -1
fi

APP_FOLDER="$1"
PMT_NUM="$2"

INPUT_FOLDER="$3"


# The following line redirects stdout and error messages. Can be changed.
# Do not redirect stdin. It used for communication with the GUI
LOG_FILE=${INPUT_FOLDER}/log/gain_${PMT_NUM}.log
truncate -s 0 "${LOG_FILE}"
exec 2>>"${LOG_FILE}"

# Change dir manually to the current folder
# cd /home/lhcbuser/lib-proxy
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# echo $DIR
cd "$DIR" || return 1

# echo $@

./bin/gain "${APP_FOLDER}/Datasheets" "${PMT_NUM}"
