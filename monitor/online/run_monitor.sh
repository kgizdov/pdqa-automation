#!/bin/bash

if [[ $# != 7 ]]; then
    exit -1
fi

RUN_NUMBER="$1"

INPUT_FOLDER="$2"
DATA_FOLDER="$3"
APP_FOLDER="$4"

MAPPING1="$5"
MAPPING2="$6"

LEDON="$7"

# The following line redirects stdout and error messages. Can be changed.
# Do not redirect stdin. It used for communication with the GUI
LOG_FILE=${INPUT_FOLDER}/log/online_monitor_${RUN_NUMBER}.log
exec 1>>"${LOG_FILE}" 2>>"${LOG_FILE}"

# Change dir manually to the current folder
# cd /home/lhcbuser/lib-proxy
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# echo $DIR
cd "$DIR" || return 1

# echo $@

# Create configuration file
CONF_FILE="${INPUT_FOLDER}/monitor.conf"
touch "${CONF_FILE}"

echo "# Config file for monitor" > "${CONF_FILE}"
{
    echo "ConfName Run ${RUN_NUMBER}"
    echo "Workdir ${DATA_FOLDER}"
    echo "Datasheets ${APP_FOLDER}/Datasheets"
    if [[ ${MAPPING1} == "2i" ]]; then
        echo "2inchPMT TRUE"
    else
        echo "2inchPMT FALSE"
    fi

    if [[ ${LEDON} == "1" ]]; then
        echo "Run ${RUN_NUMBER} LED"
    else
        echo "Run ${RUN_NUMBER} Dark"
    fi

    printf "\n"
} >> "${CONF_FILE}"


./bin/online_monitor "${CONF_FILE}"

evince "${INPUT_FOLDER}/Online_monitor_summary.pdf" &
