// Copyright 2016 Stefano Gallorini University of Padua
// 2016 Konstantin Gizdov University of Edinburgh

#include "OnlineMonitor.h"


////////////////
// Make plots //
////////////////
void OnlineMonitor::makePlots(const Run &run) {
  // Plot PMTs spectra
  plotPmtHisto(run);

  // Plot test-box and pedestal by default
  TCanvas *canv = new TCanvas("Canv_summary", "Monitor summary", 1300, 800);
  TString pdfname = Form("%s/%s_monitor_summary.pdf", dir(run).Data(), m_name.Data());
  canv->SaveAs(pdfname + "[");

  canv->Divide(3, 2);
  canv->cd(1);
  plotBox(run);

  canv->cd(2);
  plotMoniMap(run, "Raw Pedestal pos");
  plotMoniMap(run, "Average Raw Pedestal pos", "", "text same");

  // New calibration plots
  if (run.isDark() && run.globalGain()) {
    canv->cd(3);
    plotMoniMap(run, "Average MAROC gains", "[ADC]", "text");

    canv->SaveAs(pdfname);
    canv->Clear();
  }

  // Calib run plots
  else if (run.isCalib()) {
    canv->cd(3);
    plotMoniMap(run, "Raw Gain ADC", "[ADC]");
    plotMoniMap(run, "Average Raw Gain ADC", "[ADC]", "text same");

    canv->cd(4);
    plotMoniGraph(run, "Average Raw Gain ADC", "", kBlue, "AP");

    canv->cd(5);
    plotMoniMap(run, "Average MAROC gains", "[ADC]", "text");

    canv->SaveAs(pdfname);
    canv->Clear();
  }

  // LED run plots
  else if (run.isLED()) {
    canv->cd(3);
    plotMoniMap(run, "Raw Gain ADC", "[ADC]");
    plotMoniMap(run, "Average Raw Gain ADC", "[ADC]", "text same");

    canv->cd(4);
    plotMoniMap(run, "Raw Gain", "[x10^{6}]");
    plotMoniMap(run, "Average Raw Gain", "[x10^{6}]", "text same");

    canv->cd(5);
    plotMoniMap(run, "Raw Peak to valley ratio");
    plotMoniMap(run, "Average Raw Peak to valley ratio", "", "text same");

    canv->SaveAs(pdfname);
    canv->Clear();

    canv->Divide(3, 2);
    canv->cd(1);
    plotMoniGraph(run, "Average Raw Gain", "[x10^{6}]", kBlue, "AP");

    canv->cd(2);
    plotMoniGraph(run, "Raw Uniformity", "", kBlue, "AP");

    canv->cd(3);
    plotMoniGraph(run, "Raw Number of pixels low pv ratio", "", kBlue, "AP");

    canv->cd(4);
    plotMoniCorrel(run, "Average Gain", "Average Raw Gain", "[x10^{6}]", "[x10^{6}]");

    canv->cd(5);
    plotMoniCorrel(run, "Average Gain", "Raw Uniformity", "[x10^{6}]", "");

    canv->SaveAs(pdfname);
    canv->Clear();

    plotMoniHisto(run, "Raw Peak to valley ratio", "Peak to valley ratio");

    canv->SaveAs(pdfname);
    canv->Clear();
    
    plotMoniHisto(run, "Raw Gain wrt max", "Gain wrt maximum [%]");

    canv->SaveAs(pdfname);
    canv->Clear();
  }

  // Dark run plots
  else if (run.isDark()) {
    canv->cd(3);
    plotMoniMap(run, "Dark count rate", "[kHz]");
    plotMoniMap(run, "Total Dark count rate", "", "text same");

    canv->cd(4);
    plotMoniGraph(run, "Total Dark count rate", "", kBlue, "AP");

    canv->cd(5);
    plotMoniCorrel(run, "Average Gain", "Total Dark count rate", "[x10^{6}]", "[kHz]");

    canv->cd(6);
    plotMoniCorrel(run, "Dark current", "Total Dark count rate", "[nA]", "[kHz]");

    canv->SaveAs(pdfname);
    canv->Clear();
  }

  else {
    std::cout << "[INFO] OnlineMonitor::makePlots(): Nothing to draw for run "
      << run << std::endl;
  }

  canv->SaveAs(pdfname + "]");
  canv->Write();
}


/////////////////////////////
// Process data of a MaPMT //
/////////////////////////////
void OnlineMonitor::monitor(const Run &run, const TString &pmt) {
  // const int ADC_max = 256;            // max ADC value we want
  const float p0 = 334.8;             // MAROC gain vs datasheet gain fit parameters
  const float p1 = -0.6927;

  // Run peak finding algorithm
  runPeakFinding(run, pmt);

  // Compute MAROC gain if it's calibration run
  // --- NEW METHOD ---
  if (run.isDark() && run.globalGain()) {
    float calib_gain = p0 / (m_datasheet["Average Gain"][pmt][0] - p1);
    Data marocGains(NPX, calib_gain);

    fillMoniVar(run, "MAROC gains", pmt, marocGains);
  }

  //  --- OLD METHOD ---
  // if (run.isCalib()) {
  //   Data marocGains;
  //   const Data &pedestal = m_data[run]["Raw Pedestal pos"][pmt];
  //   const Data &init_gain = m_data[run]["Raw Gain ADC"][pmt];
  //   const float marocGain = run.marocGain(pmt);

  //   for (int ipx = 0; ipx < NPX; ipx++) {
  //     float calib_gain = (ADC_max - pedestal[ipx]) / 3.; // 3p.e. peak at ADC max
  //     float newMarocGain = init_gain[ipx] > 0. ? (calib_gain / init_gain[ipx]) * marocGain : 0.;
  //     marocGains.add( newMarocGain );
  //   }//ipx

  //   fillMoniVar(run, "MAROC gains", pmt, marocGains);
  // }
}


//////////////////
// Analyze data //
//////////////////
void OnlineMonitor::analyze() {
  DataMap times, rates, erates;

  // Loop over PMTs
  for (const auto &pmt : pmts()) {
    // Analyze dark runs
    const auto &dcr_runs = runs(pmt, DarkRun);
    if (dcr_runs.size() == 0) {
      std::cout << "[WARNING] analyze(): No dark current runs found for PMT: " << pmt << std::endl;
      continue;
    }

    for (const auto &run : dcr_runs) { 
      float dcr_time(0.); // get run time and DCR
      TString fname = Form("%s/%i/%s_000%i.root", m_workDir.Data(), run.number(), pmt.Data(), run.number());
      struct stat st; int ierr = stat(fname.Data(), &st);
      if (ierr == 0) dcr_time = static_cast<float>(st.st_mtime);

      times.add(pmt, dcr_time);
      rates.add(pmt, m_data[run]["Total Dark count rate"][pmt]);
      erates.add(pmt, m_data[run]["Total Dark count rate error"][pmt]);
    }//dcr

    times[pmt] -= times[pmt].min();
    times[pmt] /= 3600.;
    
    // Analyze nominal runs
    const auto &nominal_runs = nominalRuns(pmt);
    if (nominal_runs.size() == 0) {
      std::cout << "[WARNING] analyze(): No nominal runs found for PMT: " << pmt << std::endl;
      continue;
    }

    // Analysis to be implemented for each PMT...
  }//pmts

  
  // Create canvas
  TCanvas *canv = new TCanvas("Canv_summary", "Monitor summary", 1000, 800);
  TString pdfname = Form("%s/%s_monitor_analysis_summary_%s.pdf",
                         m_workDir.Data(), m_name.Data(), m_configName.Data());
  canv->SaveAs(pdfname + "[");

  // Plot DCR
  int icol(0);
  for (const auto &pmt : times.keys()) {
    if (icol == 0 || icol == 10 || icol == 19) icol++;  // avoid white-ish colors
    TString opt = (pmt == times.keys().front()) ? "APL" : "PL same";
    TGraphErrors *gr = plotGraph(times[pmt], rates[pmt], {}, erates[pmt], "Dark count rate " + pmt,
	"Dark count rate over time", "Hours", "Dark count rate [kHz]", rates.range(), icol++, opt);
    gr->Write();
  }

  canv->SaveAs(pdfname);
  canv->Clear();

  // Plot nominal runs
  for (const auto &run : nominalRuns()) {
    canv->Divide(2, 2);
    canv->cd(1);
    plotMoniCorrel(run, "Average Gain", "Average Raw Gain", "[x10^{6}]", "[x10^{6}]");

    canv->cd(2);
    plotMoniCorrel(run, "Average Gain", "Raw Uniformity", "[x10^{6}]", "");

    canv->cd(3);
    plotMoniHisto(run, "Average Gain", "Average Raw Gain [x10^{6}]", 25, {0., 12.});

    canv->cd(4);
    plotMoniHisto(run, "Uniformity", "Uniformity", 25, {0., 100.});
    canv->SaveAs(pdfname);
    canv->Clear();

    plotMoniHisto(run, "Raw Peak to valley ratio", "Peak to valley ratio", 25, {0., 12.});
    canv->SaveAs(pdfname);
    canv->Clear();
  }

  // Save canvas
  canv->SaveAs(pdfname + "]");
}
