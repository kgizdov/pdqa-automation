// Copyright 2016 Stefano Gallorini University of Padua
// 2016 Konstantin Gizdov University of Edinburgh

#include "OnlineMonitor.h"

//========================
// Run Online Monitor
//========================
int main(int argc, char **argv) {
  int run(0);
  TString configfile(argv[1]);
  if (argc >= 3) run = atoi(argv[2]);

  // The monitor
  Monitor *moni = new OnlineMonitor("Online");

  // Read config
  moni->readConf(configfile);
  // moni->printDatasheet();

  // Define tolerances
  moni->setTolerance("Average Raw Gain", 1., HUGE_VAL);  // Me-
  moni->setTolerance("Raw Number of pixels low pv ratio", 0, 3);
  moni->setTolerance("Raw Uniformity", 25., 100., 33., 100.);
  moni->setTolerance("Total Dark count rate", 0., 16., 0., 70.);  // kHz
  moni->setTolerance("Dark count rate", 0., 1., 0., 4.);  // kHz

  // Run monitor!
  if (run) {
    moni->deleteRuns();  // remove read runs
    moni->addRun(run);
  }
  moni->run();

  return 0;
}
