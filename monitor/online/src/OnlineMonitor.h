// Copyright 2016 Stefano Gallorini University of Padua
// 2016 Konstantin Gizdov University of Edinburgh

#ifndef MONITOR_ONLINE_SRC_ONLINEMONITOR_H_
#define MONITOR_ONLINE_SRC_ONLINEMONITOR_H_

#include "Monitor.h"
#include "TSpectrum.h"
#include "TPolyMarker.h"
#include <sys/stat.h>


//=================
// Online monitor
//=================
class OnlineMonitor: public Monitor {
 public:
    OnlineMonitor(TString name = "Online Monitor") : Monitor(name) {}
    ~OnlineMonitor() {}

    // Process input data (user-defined)
    void monitor(const Run &run, const TString &pmt);

    // Analyze data (user-defined)
    void analyze();

    // Make monitoring plots (user-defined)
    void makePlots(const Run &run);

    // Write analysis info on file
    void writeAnalysis() {}
};
#endif  // MONITOR_ONLINE_SRC_ONLINEMONITOR_H_
