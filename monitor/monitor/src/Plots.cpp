// Copyright 2016 Stefano Gallorini University of Padua
// 2016 Konstantin Gizdov University of Edinburgh

#include "Plots.h"


///////////////////////
// Plot graph (Data) //
///////////////////////
TGraphErrors* plotGraph(const Data &x,
                        const Data &y,
                        const Data &ex,
                        const Data &ey,
                        const TString &name,
                        const TString &title,
                        const TString &xtitle,
                        const TString &ytitle,
                        Range yrange,
                        const Color_t col,
                        const TString &opt) {
  // Sanity checks
  if (x.size() != y.size()) {
    std::cout << "[WARNING] plotGraph(): " << name
      << " has different x, y sizes!" << std::endl;
    return nullptr;
  }

  if (!x.isFinite() || !y.isFinite()) {
    std::cout << "[WARNING] plotGraph(): " << name
      << " contains not finite values!" << std::endl;
    return nullptr;
  }

  // Set proper range
  if (!yrange) yrange = y.range();
  if (yrange.isLowerInf()) yrange.setMin(y.range().min());  // prevent inf
  if (yrange.isUpperInf()) yrange.setMax(y.range().max());
  yrange.enlarge(RANGE_TOL);

  // Make graph
  TGraphErrors *gr = new TGraphErrors(x.size(), x.ptr(), y.ptr(), ex.ptr(), ey.ptr());
  gr->SetName(("Graph " + name).ReplaceAll(" ", "_"));
  gr->SetTitle(title + ";" + xtitle + ";" + ytitle);
  gr->SetMinimum(yrange.min());
  gr->SetMaximum(yrange.max());

  // Set style
  gStyle->SetTitleFontSize(0.07);
  gStyle->SetTitleW(1);
  gr->GetXaxis()->SetTitleSize(0.045);
  gr->GetXaxis()->SetTitleOffset(0.8);
  gr->GetYaxis()->SetTitleSize(0.045);
  gr->GetYaxis()->SetTitleOffset(1.1);
  gr->SetMarkerStyle(21);
  gr->SetMarkerSize(2);
  gr->SetMarkerColor(col);
  gr->SetLineColor(col);
  gr->SetLineWidth(2.0);

  gr->Draw(opt);
  gr->Write();  // ROOT does not save TGraphs automatically...

  return gr;
}


//////////////////////////
// Plot graph (DataMap) //
//////////////////////////
TGraphErrors* plotGraph(const DataMap &xmap,
                        const DataMap &ymap,
                        const DataMap &exmap,
                        const DataMap &eymap,
                        const TString &name,
                        const TString &title,
                        const TString &xtitle,
                        const TString &ytitle,
                        const Range &range,
                        const Color_t col,
                        const TString &opt) {
  Data x, y, ex, ey;
  for (const auto &data : xmap) x.add(data.second);
  for (const auto &data : ymap) y.add(data.second);
  for (const auto &data : exmap) ex.add(data.second);
  for (const auto &data : eymap) ey.add(data.second);

  TGraphErrors *gr = plotGraph(x, y, ex, ey, name, title, xtitle, ytitle, range, col, opt);
  return gr;
}


//////////////////////////////
// Plot 1D histogram (Data) //
//////////////////////////////
TH1F* plot1DHisto(const Data &data,
                  const TString &name,
                  const TString &title,
                  const TString &xtitle,
                  int nBins,
                  Range range,
                  const Color_t col,
                  const TString &opt) {
  // Sanity checks
  if (!data.isFinite()) {
    std::cout << "[WARNING] plot1DHisto(): " << name
      << " contains not finite values!" << std::endl;
    return nullptr;
  }

  // Binning (if not range provided/wrong, use data range)
  if (!range) range = data.range();
  if (range.isLowerInf()) range.setMin(data.range().min());  // prevent inf
  if (range.isUpperInf()) range.setMax(data.range().max());
  range.enlarge(RANGE_TOL);
  float xmin = range.min();
  float xmax = range.max();
  if (nBins == 0) nBins = data.size();

  // Book histogram
  TH1F *h = new TH1F(("Histo1D " + name).ReplaceAll(" ", "_"), title, nBins, xmin, xmax);
  h->SetTitle(title + ";" + xtitle);

  // Set style
  gStyle->SetTitleFontSize(0.07);
  gStyle->SetTitleW(1);
  h->GetXaxis()->SetTitleSize(0.045);
  h->GetXaxis()->SetTitleOffset(0.8);
  h->GetYaxis()->SetTitleSize(0.045);
  h->GetYaxis()->SetTitleOffset(1.1);
  h->SetLineWidth(2);
  h->SetLineColor(col);
  h->SetMarkerColor(col);

  // Fill histogram
  for (const auto &val : data) h->Fill(val);

  // Draw it
  h->Draw(opt);

  return h;
}


/////////////////////////////////
// Plot 1D histogram (DataMap) //
/////////////////////////////////
TH1F* plot1DHisto(const DataMap &dmap,
                  const TString &name,
                  const TString &title,
                  const TString &xtitle,
                  const int nBins,
                  const Range &range,
                  const Color_t col,
                  const TString &opt) {
  Data totdata;
  for (const auto &data : dmap) totdata.add(data.second);

  TH1F* h = plot1DHisto(totdata, name, title, xtitle, nBins, range, col, opt);
  return h;
}


//////////////////////////////
// Plot 2D histogram (Data) //
//////////////////////////////
TH2F* plot2DHisto(const Data &xdata,
                  const Data &ydata,
                  const TString &name,
                  const TString &title,
                  const TString &xtitle,
                  const TString &ytitle,
                  const TString &opt) {
  // Sanity checks
  if (!xdata.isFinite() || !ydata.isFinite()) {
    std::cout << "[WARNING] plot2DHisto(): " << name
      << " contains not finite values!" << std::endl;
    return nullptr;
  }

  // Binning
  Range xrange = xdata.range();
  xrange.enlarge(RANGE_TOL);
  float xmin = xrange.min();
  float xmax = xrange.max();
  int nBinx = xdata.size();

  Range yrange = ydata.range();
  yrange.enlarge(RANGE_TOL);
  float ymin = yrange.min();
  float ymax = yrange.max();
  int nBiny = ydata.size();

  // Book histogram
  TH2F *h = new TH2F(("Histo2D " + name).ReplaceAll(" ", "_"), title,
      nBinx, xmin, xmax, nBiny, ymin, ymax);
  h->SetTitle(title + ";" + xtitle + ";" + ytitle);

  // Set style
  gStyle->SetOptStat(0);
  gStyle->SetTitleFontSize(0.07);
  gStyle->SetTitleW(1);
  h->GetXaxis()->SetTitleSize(0.045);
  h->GetXaxis()->SetTitleOffset(0.8);
  h->GetYaxis()->SetTitleSize(0.045);
  h->GetYaxis()->SetTitleOffset(1.1);

  // Fill histogram
  for (const auto &xval : xdata) {
    for (const auto &yval : ydata) {
      h->Fill(xval, yval);
    }
  }

  // Draw it
  h->Draw(opt);

  return h;
}
