// Copyright 2016 Stefano Gallorini University of Padua
// 2016 Konstantin Gizdov University of Edinburgh

#include <string>
#include <vector>

#include "Monitor.h"


/////////////////
// Constructor //
/////////////////
Monitor::Monitor(const TString &name) {
  std::cout << "==================================================" << std::endl;
  std::cout <<  name  << " Monitor                                " << std::endl;
  std::cout << "==================================================" << std::endl;

  gROOT->SetBatch();

  setName(name);
  setRunAnalysis(false);
}


////////////////
// Destructor //
////////////////
Monitor::~Monitor() {}


/////////////////////////
// Read monitor config //
/////////////////////////
void Monitor::readConf(const TString &configfile) {
  std::ifstream file(configfile.Data(), std::ios::in);
  std::string line;
  if (!file.good()) {
    std::cout << "[ERROR] Monitor::readConf(): Cannot find config file: "
      << configfile << std::endl;
    abort();
  }

  // Read config file
  std::cout << "[INFO] Reading config file: " << configfile << std::endl;
  while (std::getline(file, line)) {
    if (line.empty()) continue;  // Skip empty line
    if (TString(line).BeginsWith("#")) continue;  // Skip comment

    std::istringstream iss(line);
    std::string tag, value, extra;
    iss >> tag >> value;
    std::getline(iss, extra);  // the rest of the line (extra args)
    std::string path = value + extra;

    if (tag == "ConfName") setConfigName(path);  // set config name
    else if (tag == "Workdir") setWorkDir(path);  // set workdir
    else if (tag == "Datasheet") readDatasheet(path);  // read datasheet
    else if (tag == "Datasheets") readDatasheets(path);  // read all datasheets recursively
    else if (tag == "Run") addRun( std::stoi(value) );  // add run
    else if (tag == "RunAnalysis") setRunAnalysis(value);  // set analysis flag
    else {
      std::cout << "[WARNING] Monitor::readConf(): " << tag << " not a config parameter!" << std::endl;
      continue;
    }
  }
}


////////////////////
// Read datasheet //
////////////////////
void Monitor::readDatasheet(const TString &datasheet) {
  const size_t nDataPerPmt = 7 + NPX;  // data per PMT
  std::string line;
  std::vector<std::string> bad_headers = {"parameter", "unit", "error(max)", "failure"};

  std::ifstream file(datasheet, std::ios::in);
  if (!file.good()) {
    std::cout << "[ERROR] Monitor::readDatasheet(): Cannot find datasheet: "
      << datasheet << std::endl;
    abort();
  }

  // Read datasheet
  std::cout << "[INFO] Reading datasheet: " << datasheet << std::endl;
  while (std::getline(file, line)) {
    if (line.empty()) continue;  // Skip empty line

    // Tokenize line
    std::istringstream iss(line);
    std::string token;
    std::vector<std::string> tokens;
    while (std::getline(iss, token, ',')) {
      if (token != "") {
        token.erase(std::remove_if(token.begin(), token.end(), isspace), token.end());
        tokens.push_back(token);
      }
    }
    size_t nTokens = tokens.size();
    if (nTokens == 0) continue;  // Skip line w/o tokens

    // Skip lines we don't want
    std::string header = tokens[0];
    if (std::find(bad_headers.begin(), bad_headers.end(), header) != bad_headers.end())
      continue;

    // Load datasheet specs
    if (nTokens == nDataPerPmt) {
      std::string pmt = header;
      if (isInDatasheet(pmt)) continue;  // don't add same pmt twice
      m_datasheet.add("Sk", pmt, std::stod(tokens[1]));
      m_datasheet.add("Sp", pmt, std::stod(tokens[2]));
      m_datasheet.add("Dark current", pmt, std::stod(tokens[3]));
      m_datasheet.add("Skb", pmt, std::stod(tokens[4]));
      m_datasheet.add("Average Gain", pmt, std::stod(tokens[5]));
      m_datasheet.add("Uniformity", pmt, std::stod(tokens[6]));
      for (size_t ipx = 0; ipx < NPX; ipx++) {
        float gain = std::stod(tokens[7 + ipx]);
        m_datasheet.add("Normalized Gain", pmt, gain);
      }
      float dark_rate = std::stod(tokens[3]) / 1.6e-4 / std::stod(tokens[5]);
      m_datasheet.add("Dark current rate", pmt, dark_rate / 1e3);  // kHz
    }

    else {
      std::cout << "[ERROR] Monitor::readDatasheet(): Cannot parse datasheet! Check csv format!" << std::endl;
      std::cout << "[ERROR] Monitor::readDatasheet(): Cannot parse this line: " << line << std::endl;
      std::cout << "[ERROR] Monitor::readDatasheet(): Expected tokens = " << nDataPerPmt
                << ", found = " << nTokens << std::endl;
      abort();
    }
  }  // file
}


//////////////////////////////////////////////////////////
// Read all datasheets recursively from a top directory //
//////////////////////////////////////////////////////////
void Monitor::readDatasheets(const TString &path) {
  DIR *top_dir = opendir(path.Data());
  struct dirent *ent = readdir(top_dir);
  struct dirent *sub_ent = nullptr;

  std::cout << "[INFO] Reading all datasheets from: " << path << std::endl;
  while ((ent = readdir(top_dir)) != nullptr) {
    if (ent->d_type == DT_DIR) {  // it should be a dir
      // Get dir name
      TString dir_name = path + "/" + reinterpret_cast<char*>(ent->d_name);
      if (dir_name.Contains("/..")) continue;  // Skip . & .. folders

      // Look inside the subdir
      DIR *sub_dir = opendir(dir_name.Data());
      while ((sub_ent = readdir(sub_dir)) != nullptr)  {
        if (sub_ent->d_type != DT_DIR) {  // it's should be a file
          TString file_name = dir_name + "/" + reinterpret_cast<char*>(sub_ent->d_name);

          // Read datasheet in .csv format
          if (file_name.Contains(".csv")) readDatasheet(file_name);
        }
      }
      closedir(sub_dir);
    }
  }
  closedir(top_dir);
}


//////////////////////////////////////
// Set tolerance range for variable //
//////////////////////////////////////
void Monitor::setTolerance(const TString &var,
                           const float min, const float max,
                           const float min2, const float max2) {
  Range r1 = {min, max};
  Range r2 = {min2, max2};

  m_tol1inch.set(var, r1);
  if (!r2) m_tol2inch.set(var, r1);
  else m_tol2inch.set(var, r2);

  std::cout << "[INFO] " << var << " tolerance for 1 inch PMTs set to: ["
            << m_tol1inch[var].min() << ", " <<  m_tol1inch[var].max() << "]" << std::endl;

  std::cout << "[INFO] " << var << " tolerance for 2 inch PMTs set to: ["
            << m_tol2inch[var].min() << ", " <<  m_tol2inch[var].max() << "]" << std::endl;
}


///////////////////////////////////
// Decide if to run the analysis //
///////////////////////////////////
void Monitor::setRunAnalysis(const bool flag) {
  m_runAnalysis = flag;

  std::cout << "[INFO] RunAnalysis flag set to "
    << (m_runAnalysis == true ? "true" : "false") << std::endl;
}

void Monitor::setRunAnalysis(const TString &flag) {
  if (flag == "y" || flag == "yes" || flag == "Yes")
    setRunAnalysis(true);
  else setRunAnalysis(false);
}


/////////////////////////
// Add runs to monitor //
/////////////////////////
void Monitor::addRun(const int runNb) {
  // NB: assume that run info are stored in runNb/log/run_environment.log!!!
  // NB NB: assume m_workDir already loaded!!! Fix!!!
  std::cout << "[INFO] Loading run " << runNb << "..." << std::endl;
  Run run(runNb, dir(runNb) + "/log/run_environment.log");
  m_data.set(run);
  std::cout << run << std::endl;
}


////////////////////////////////////
// Returns PMTs outside tolerance //
////////////////////////////////////
const std::vector<TString> Monitor::badPMTs(const Run &run) const {
  std::vector<TString> bad_pmts;
  bad_pmts.reserve(run.nMaxPMTs());
  const auto &tol = tolerance(run.are2inchPMTs());

  // Bad PMTs defined only for nominal and dark runs!
  if (!run.isNominal() && !run.isDark()) return bad_pmts;
  if (run.globalGain()) return bad_pmts;  // temporary hack!!!

  for (const auto &var : moniVars(run)) {
    if (!tol.contains(var)) continue;
    for (const auto &pmt : run.pmts()) {
      if (m_data[run][var][pmt].range() > tol[var]) {
        bad_pmts.push_back(pmt);
      }
    }  // pmt
  }  // var

  return bad_pmts;
}


//////////////////////////////////
// Print list of monitored runs //
//////////////////////////////////
void Monitor::printRuns() const {
  std::cout << "[INFO] Runs to be monitored:" << std::endl;
  for (const auto &run : runs())
    std::cout << "[INFO] " << run.number() << std::endl;
}


/////////////////
// Delete runs //
/////////////////
void Monitor::deleteRuns() {
  m_data.clear();
  setRunAnalysis(false);
  std::cout << "[INFO] All runs have been deleted" << std::endl;
}


//////////////////////////////
// Fill monitoring variable //
//////////////////////////////
void Monitor::fillMoniVar(const Run &run,
                          const TString &var,
                          const TString &pmt,
                          const Data &data) {
  // Add the data
  MoniMap &mmap = m_data[run];  // get current DataMap
  mmap.add(var, pmt, data);

  // Add average, min/max values
  mmap.add("Average " + var, pmt, data.average());
  mmap.add("Total " + var, pmt, data.sum());
  mmap.add("Min " + var, pmt, data.min());
  mmap.add("Max " + var, pmt, data.max());
}


////////////////////////////////////////
// Write monitoring info on text file //
////////////////////////////////////////
void Monitor::write(const Run &run) {
  TString fname = Form("%s/%s_monitor_data.dat", dir(run).Data(), m_name.Data());
  FILE *outfile = fopen(fname.Data(), "w");

  const auto &data = m_data[run];

  // Loop on moni vars
  for (const auto &var : data.keys()) {
    fprintf(outfile, "%s \n", var.Data());  // write var

    // Loop on data
    for (const auto &pmt : data[var].keys()) {
      fprintf(outfile, "%s \n", pmt.Data());  // write pmt

      // Loop on values
      for (int i = 0; i < data[var][pmt].size(); i++) {
        fprintf(outfile, "%i %f \n", i, data[var][pmt][i]);  // write values
      }  // val
    }  // data
  }  // dset

  fclose(outfile);
}


///////////////////////////
// Make monitoring plots //
///////////////////////////
void Monitor::plot(const Run &run) {
  m_outFile = new TFile(Form("%s/%s_monitor_plots.root",
  dir(run).Data(), m_name.Data()), "RECREATE");

  makePlots(run);

  m_outFile->Write();
  m_outFile->Close();
}


////////////////////////////
// Print datasheet values //
////////////////////////////
void Monitor::printDatasheet() const {
  const auto &pmts = m_datasheet.begin()->second.keys();
  for (const auto &pmt : pmts) {
    std::cout << "[INFO] PMT " << pmt << " has datasheet values:" << std::endl;

    for (const auto &var : m_datasheet.keys()) {
      std::cout << "[INFO] " << var << " ";
      for (const auto &val : m_datasheet[var][pmt]) std::cout << val << " ";
      std::cout << std::endl;
    }  // var

    std::cout << std::endl;
  }  // pmt
}


/////////////////
// Monitor run //
/////////////////
void Monitor::monitor(const Run &run) {
  // Start!
  time_t start, end;
  time(&start);

  std::cout << "==================================================" << std::endl;
  std::cout << "[INFO] Monitoring run: " << run.number()            << std::endl;
  std::cout << "==================================================" << std::endl;

  // Process data
  for (const auto &pmt : run.pmts()) {
    if (!isInDatasheet(pmt)) {
      std::cout << "[WARNING] Monitor::monitor(): PMT " << pmt
  << " is not in datasheet!" << std::endl;
    }

    std::cout << "[INFO] Monitoring PMT: " << pmt << std::endl;
    monitor(run, pmt);
  }

  // Make monitoring plots
  plot(run);

  // Save info on txt
  write(run);

  // Stop!
  time(&end);
  int timing = difftime(end, start);

  std::cout << "[INFO] Took me: " << int(timing / 60) << " min"
            << " and " << timing % 60 << " sec" << std::endl;
  std::cout << "[INFO] Done!" << std::endl;
  std::cout << std::endl;
}


/////////////////////
// Run the monitor //
/////////////////////
void Monitor::run() {
  // Start!
  time_t start, end;
  time(&start);

  std::cout << "==================================================" << std::endl;
  std::cout << "[INFO] Monitor started!"                            << std::endl;
  std::cout << "==================================================" << std::endl;
  printRuns();

  // Monitor each run
  for (const auto &run : runs()) monitor(run);

  // Run analysis
  if (m_runAnalysis == true) {
    std::cout << "==================================================" << std::endl;
    std::cout << "[INFO] Analysis started!"                           << std::endl;
    std::cout << "==================================================" << std::endl;
    m_anaFile = new TFile(Form("%s/%s_monitor_analysis_plots_%s.root",
    m_workDir.Data(), m_name.Data(), m_configName.Data()), "RECREATE");

    analyze();
    writeAnalysis();

    m_anaFile->Write();
    m_anaFile->Close();
  }

  // Stop!
  time(&end);
  int timing = difftime(end, start);

  // Print timing
  std::cout << "[INFO] Total monitoring time: " << int(timing / 60) << " min"
            << " and " << timing % 60 << " sec" << std::endl;
  std::cout << "[INFO] Done!" << std::endl;
}
