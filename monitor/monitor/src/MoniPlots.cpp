// Copyright 2016 Stefano Gallorini University of Padua
// 2016 Konstantin Gizdov University of Edinburgh

#include "Monitor.h"

///////////////////
// Plot test-box //
///////////////////
void Monitor::plotBox(const Run &run,
                      const bool draw_pmt_name,
                      const bool draw_bad_pmts,
                      const TString &opt) {
  TBox *box = new TBox();
  box->SetFillStyle(0);
  box->SetLineWidth(1);

  TText *text = new TText();
  text->SetTextFont(42);

  const int boxdim = run.boxDim();

  // Book empty histogram
  int nBins = boxdim*PMTDIM;
  TH2F *h = new TH2F("Box", Form("Test box (run %i)", run.number()),
      nBins, 0, boxdim*PMTDIM, nBins, 0, boxdim*PMTDIM);
  h->Draw(opt);

  // Draw test box
  for (int ipmt = 0; ipmt < run.nMaxPMTs(); ipmt++) {
    int x0 = (ipmt % boxdim) * PMTDIM;  // centre of the base-board
    int y0 = (boxdim - ipmt/boxdim) * PMTDIM;
    box->DrawBox(x0, y0 - PMTDIM, x0 + PMTDIM, y0);
  }

  // Draw bad PMTs
  if (draw_bad_pmts) {
    box->SetFillStyle(1001);
    box->SetFillColor(kRed);

    const auto &bad_pmts = badPMTs(run);
    for (const auto &pmt : bad_pmts) {
      int x0 = run.bins(pmt).first;
      int y0 = run.bins(pmt).second;
      box->DrawBox(x0, y0 - PMTDIM +1, x0 + PMTDIM, y0 +1);
    }
  }

  // Draw pmt names
  if (draw_pmt_name) {
    for (const auto &pmt : run.pmts()) {
      int x0 = run.bins(pmt).first;
      int y0 = run.bins(pmt).second;
      text->DrawText(x0 + 1, y0 - PMTDIM/2, pmt);
    }
  }
}


//////////////////////////////////////////
// Plot variable tolerance on histogram //
//////////////////////////////////////////
void Monitor::plotTolerance(const Range &tol, TH1F* h) {
  if (!tol || h == nullptr) return;

  TLine *line = new TLine();
  line->SetLineColor(kRed);
  line->SetLineWidth(3);
  line->SetLineStyle(2);

  TBox *box = new TBox();
  box->SetFillStyle(3002);
  box->SetFillColor(kRed);

  // Get new range according to tol
  Range xr = Range(h->GetXaxis()->GetXmin(), h->GetXaxis()->GetXmax());
  Range xr_new = xr & tol;

  // Corner cases when tol is infinte
  if (tol.isUpperInf()) {
    if (xr.max() < tol.min()) xr_new.setMax(tol.min());
    else xr_new.setMax(xr.max());
  }
  if (tol.isLowerInf()) {
    if (xr.min() > tol.max()) xr_new.setMin(tol.max());
    else xr_new.setMin(xr.min());
  }

  // Set histogram range to new one
  xr_new.enlarge(RANGE_TOL);
  h->GetXaxis()->SetRangeUser(xr_new.min(), xr_new.max());

  // Draw tol line and out-of-range area
  float xmin = xr_new.min();
  float xmax = xr_new.max();
  float ymin = 0.;
  float ymax = h->GetMaximum();

  if (!tol.isLowerInf()) {
    line->DrawLine(tol.min(), ymin, tol.min(), ymax);
    box->DrawBox(xmin, ymin, tol.min(), ymax);
  }
  if (!tol.isUpperInf()) {
    line->DrawLine(tol.max(), ymin, tol.max(), ymax);
    box->DrawBox(tol.max(), ymin, xmax, ymax);
  }
}


//////////////////////////////////////
// Plot variable tolerance on graph //
//////////////////////////////////////
void Monitor::plotTolerance(const Range &tol, TGraphErrors* gr) {
  if (!tol || gr == nullptr) return;

  TLine *line = new TLine();
  line->SetLineColor(kRed);
  line->SetLineWidth(3);
  line->SetLineStyle(2);

  TBox *box = new TBox();
  box->SetFillStyle(3002);
  box->SetFillColor(kRed);

  // Get new range according to tol
  Range yr = Range(gr->GetMinimum(), gr->GetMaximum());
  Range yr_new = yr & tol; 

  // Corner cases when tol is infinte
  if (tol.isUpperInf()) {
    if (yr.max() < tol.min()) yr_new.setMax(tol.min());
    else yr_new.setMax(yr.max());
  }
  if (tol.isLowerInf()) {
    if (yr.min() > tol.max()) yr_new.setMin(tol.max());
    else yr_new.setMin(yr.min());
  }

  // Set graph range to new one
  yr_new.enlarge(RANGE_TOL);
  gr->SetMinimum(yr_new.min());
  gr->SetMaximum(yr_new.max());
  
  // Draw tol line and out-of-range area
  float xmin = gr->GetHistogram()->GetXaxis()->GetXmin();
  float xmax = gr->GetHistogram()->GetXaxis()->GetXmax();
  float ymin = yr_new.min();
  float ymax = yr_new.max();

  if (!tol.isLowerInf()) {
    line->DrawLine(xmin, tol.min(), xmax, tol.min());
    box->DrawBox(xmin, ymin, xmax, tol.min());
  }
  if (!tol.isUpperInf()) {
    line->DrawLine(xmin, tol.max(), xmax, tol.max());
    box->DrawBox(xmin, tol.max(), xmax, ymax);
  }
}


//////////////////////////
// Plot PMTs histograms //
//////////////////////////
void Monitor::plotPmtHisto(const Run &run) {
  const int npadx = 3; 
  const int npady = 3; 
  const int npad = npadx*npady;

  for (const auto &pmt : run.pmts()) {
    TCanvas *canv = new TCanvas("canv_" + pmt, "Spectra", 800, 600);
    canv->Divide(npadx, npady);
    TString pdfname = dir(run) + "/" + pmt + ".pdf";
    canv->SaveAs(pdfname + "[");

    for (int ipx = 0; ipx < NPX; ipx++) {
      int ipad = (ipx%npad) + 1;
      TCanvas *ctmp = static_cast<TCanvas*>(canv->cd(ipad));
      ctmp->SetLogy();

      m_pmtHisto[pmt][ipx]->Draw();
      m_pmtHisto[pmt][ipx]->Write();

      if (ipad == npad) {
        canv->SaveAs(pdfname);
        canv->Clear();
        canv->Divide(npadx, npady);
      }
    }  // ipx

    if (NPX%npad != 0) canv->SaveAs(pdfname);  // save remaining spectra
    canv->SaveAs(pdfname + "]");
  }  // pmt

  // Delete histos
  for (auto &pmt : run.pmts())
    for (int ipx = 0; ipx < NPX; ipx++) delete m_pmtHisto[pmt][ipx];
}


///////////////////////////////////////////////////
// Plot variable on graph for all PMTs for a run //
///////////////////////////////////////////////////
TGraphErrors* Monitor::plotMoniGraph(const Run &run, const TString &var,
                                     const TString &yunit,
                                     const Color_t col, const TString &opt) {
  const auto &data = m_data[run][var];
  const auto &tol = tolerance(run.are2inchPMTs());
  Data x, y, ex, ey;  // errors not assigned yet!
  for (const auto &pmt : run.pmts()) y.add(data[pmt]);
  for (int i = 0; i < y.size(); i++) x.add(i+1);

  TString name = var;
  TString title = Form("%s (run %i)", var.Data(), run.number());
  TString xtitle = "";
  TString ytitle = var + " " + yunit;
  Range range = y.range();
  auto gr = plotGraph(x, y, ex, ey, name, title, xtitle, ytitle, range, col, opt);

  // Plot pmt name on x-axis
  if (data.oneValued()) {
    TH1F *h = gr->GetHistogram();
    for (int i = 0; i < gr->GetN(); i++) {
      gr->GetXaxis()->SetBinLabel(h->FindBin(gr->GetX()[i]), run.pmts()[i]);
    }
    gr->GetXaxis()->LabelsOption("d");
    gr->GetXaxis()->SetLabelOffset(0.01);
    if (run.are2inchPMTs()) gr->GetXaxis()->SetLabelSize(0.06);
  }

  if (tol.contains(var)) plotTolerance(tol[var], gr);

  return gr;
}


//////////////////////////////////////////////
// Plot the map of variable on the test-box //
//////////////////////////////////////////////
TH2F* Monitor::plotMoniMap(const Run &run,
                           const TString &var,
                           const TString &zunit,
                           const TString &opt,
                           const bool draw_pmt_name) {
  gStyle->SetTitleFontSize(0.07);
  gStyle->SetTitleW(1);
  gStyle->SetOptStat(0);
  gStyle->SetPaintTextFormat("4.1f");
  TGaxis::SetMaxDigits(3);

  gPad->SetTicks(1, 1);
  gPad->SetLeftMargin(0.08);
  gPad->SetRightMargin(0.13);

  // Tell if var contains one value per PMT
  const auto &dmap = m_data[run][var];
  const auto &tol = tolerance(run.are2inchPMTs());
  bool oneValued = dmap.oneValued();

  // Book histogram
  const int boxdim = run.boxDim();
  int nBins = boxdim*PMTDIM;
  if (oneValued) nBins /= PMTDIM;
  TString name = ("Map " + var).ReplaceAll(" ", "_");
  TString title = Form("%s %s (run %i)", var.Data(), zunit.Data(), run.number());
  TH2F *h = new TH2F(name, title, nBins, 0, boxdim*PMTDIM, nBins, 0, boxdim*PMTDIM);

  // Fill histogram
  for (const auto &pmt : run.pmts()) {
    const Data &data = dmap[pmt];
    int binx0 = run.bins(pmt).first;  // pos on map
    int biny0 = run.bins(pmt).second;
    if (oneValued) {  // one value per PMT
      h->SetBinContent(binx0/PMTDIM + 1, biny0/PMTDIM + 1, data[0]);
    }
    else {
      for (int i = 0; i < data.size(); i++) {
        h->SetBinContent(binx0 + i%PMTDIM + 1, biny0 - i/PMTDIM + 1, data[i]);
      }
    }
  }

  // Draw it
  h->GetXaxis()->SetTitleSize(0.045);
  h->GetXaxis()->SetTitleOffset(0.8);
  h->GetYaxis()->SetTitleSize(0.045);
  h->GetYaxis()->SetTitleOffset(1.1);

  if (oneValued) {
    if (draw_pmt_name) h->SetBarOffset(0.2);  // if pmt name it's drawn
    h->SetMarkerSize(3);
    h->SetMarkerColor(kRed);
    h->Draw(opt);
  }
  else {
    if (tol.contains(var)) {
      h->SetMinimum(tol[var].min());
      h->SetMaximum(tol[var].max());
    }
    h->GetZaxis()->SetTitle(zunit);
    h->Draw(opt);

    gPad->Update();  // adjust palette
    auto palette = static_cast<TPaletteAxis*>(h->GetListOfFunctions()->FindObject("palette"));
    palette->SetX1(h->GetXaxis()->GetXmax() + 0.3);
    palette->SetX2(h->GetXaxis()->GetXmax() + 1.2);
    h->Draw(opt);
  }

  plotBox(run, draw_pmt_name, false, "same");

  return h;
}


/////////////////////////////////////////
// Plot correlation graph wrt datsheet //
/////////////////////////////////////////
void Monitor::plotMoniCorrel(const Run &run,
                             const TString &dsvar, const TString &var,
                             const TString &xunit, const TString &yunit) {
  // Check datasheet
  if (!m_datasheet.contains(dsvar)) {
    std::cout << "[WARNING] Monitor::plotMoniCorrel(): " << dsvar
              << " not in datasheet! Cannot plot!" << std::endl;
    return;
  }

  const auto &data = m_data[run][var];
  const auto &dsheet = m_datasheet[dsvar].submap(data.keys());
  const auto &tol = tolerance(run.are2inchPMTs());

  TString xtitle = "Datasheet " + dsvar + " " + xunit;
  TString ytitle = "Measured " + var + " " + yunit;
  Range range = data.range();

  // If one-valued dataset, draw correl. for all PMTs
  if (data.oneValued()) {
    TString name = "Datasheet " + dsvar + " vs " + var;
    TString title = Form("Correlation with datasheet (run %i)", run.number());
    auto gr = plotGraph(dsheet, data, {}, {}, name, title, xtitle, ytitle, range);
    if (tol.contains(var)) plotTolerance(tol[var], gr);
  }

  // Otherwise draw correl. for each pmt
  else {
    const int boxdim = run.boxDim();
    const int npadx = boxdim, npady = boxdim;
    TVirtualPad *pad = gPad;  // get current pad
    pad->Divide(npadx, npady);

    // Loop on pmts
    for (const auto &pmt : run.pmts()) {
      int binx = run.bins(pmt).first / PMTDIM + 1;
      int biny = run.bins(pmt).second / PMTDIM + 1;
      int ipad = (boxdim - biny)*boxdim + binx;
      pad->cd(ipad);  // draw each graph at right position on the box

      TString name = "Datasheet " + dsvar + " vs " + var + " PMT " + pmt;
      TString title = "Correlation with datasheet: " + pmt;
      auto gr = plotGraph(dsheet[pmt], data[pmt], {}, {},
                          name, title, xtitle, ytitle, range, kBlue, "AP");
      if (tol.contains(var)) plotTolerance(tol[var], gr);
    }  // pmt
  }
}


///////////////////////////////
// Plot monitoring histogram //
///////////////////////////////
void Monitor::plotMoniHisto(const Run &run,
                            const TString &var,
                            const TString &xtitle,
                            const int nBins,
                            Range range,
                            const Color_t col,
                            const TString &opt) {
  const auto &data = m_data[run][var];
  const auto &tol = tolerance(run.are2inchPMTs());

  // If range not given, use data range and tolerance
  if (!range) range = data.range();

  // If one-valued dataset, draw all pmts
  if (data.oneValued()) {
    TString title = Form("%s (run %i)", var.Data(), run.number());
    TH1F *h = plot1DHisto(data, var, title, xtitle, nBins, range, col, opt);
    if (tol.contains(var)) plotTolerance(tol[var], h);
  }

  // Otherwise draw each pmt
  else {
    const int boxdim = run.boxDim();
    const int npadx = boxdim, npady = boxdim;
    TVirtualPad *pad = gPad;  // get current pad
    pad->Divide(npadx, npady);

    // Loop on pmts
    for (const auto &pmt : run.pmts()) {
      int binx = run.bins(pmt).first / PMTDIM + 1;
      int biny = run.bins(pmt).second / PMTDIM + 1;
      int ipad = (boxdim - biny)*boxdim + binx;
      pad->cd(ipad);  // draw each graph at right position on the box

      TH1F *h = plot1DHisto(data[pmt], var + " " + pmt, var + " " + pmt, xtitle, nBins, range, col, opt);
      if (tol.contains(var)) plotTolerance(tol[var], h);
    }  // pmt
  }
}


////////////////////////////////////////////
// Plot analysis variable on 1D histogram //
////////////////////////////////////////////
// PLEASE FIX BINNING FOR THIS FUNCTION!
void Monitor::plotAnaHisto(const TString &var,
                           const TString &xtitle,
                           const int nBins,
                           Range range,
                           const Color_t col,
                           const TString &opt) {
  // const DataMap &data = m_anaData[var];
  // const auto &tol = tolerance( run.are2inchPMTs() );

  // // If range not given, use data range and tolerance
  // if (!range && tol.contains(var)) range = data.range() & tol[var];

  // // If one-valued dataset, draw all pmts
  // if (data.oneValued()) {
  //   TH1F *h = plot1DHisto(data, var, var, xtitle, nBins, range, col, opt);
  //   plotTolerance(tol[var], h);
  // }

  // // Otherwise draw each pmt
  // else {
  //   const int boxdim = 0; // run.boxDim(); // FIX!!!!!!
  //   const int npadx = boxdim, npady = boxdim;
  //   TVirtualPad *pad = gPad; // get current pad
  //   pad->Divide(npadx, npady);

  //   // Loop on pmts
  //   for (const auto &pmt : data.keys()) {
  //     int binx = 0; //run.bins(pmt).first / PMTDIM + 1;  // FIX!!!!!
  //     int biny = 0; //run.bins(pmt).second / PMTDIM + 1;
  //     int ipad = (boxdim - biny)*boxdim + binx;
  //     pad->cd(ipad); // draw each graph at right position on the box

  //     TH1F *h = plot1DHisto(data[pmt], var + " " + pmt, var + " " + pmt,
  //         xtitle, nBins, range, col, opt);
  //     plotTolerance(tol[var], h);
  //   }  // pmt
  // }
}
