// Copyright 2016 Stefano Gallorini University of Padua
// 2016 Konstantin Gizdov University of Edinburgh

#ifndef MONITOR_MONITOR_SRC_MONITOR_H_
#define MONITOR_MONITOR_SRC_MONITOR_H_

#include "Data.h"
#include "Run.h"
#include "Plots.h"

// C inc
#include <dirent.h>
#include <stdio.h>

// C++ inc
#include <algorithm>
#include <iomanip>
#include <iostream>
#include <vector>
#include <list>
#include <string>
#include <fstream>
#include <sstream>
#include <iterator>

// ROOT inc
#include "TROOT.h"
#include "TSystem.h"
#include "TStyle.h"
#include "TTree.h"
#include "TPad.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TF1.h"
#include "TMath.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TBox.h"
#include "TLine.h"
#include "TGaxis.h"
#include "TPaletteAxis.h"
#include "TText.h"
#include "TPolyMarker.h"
#include "TSpectrum.h"


//==============================
// Base class for Q&A monitor
//==============================
// TODO:
//  - Add copy, assignment & move constructors to Data.h
//  - Use smart pointers as much as possible
//  - Use template function to filter over runs?
class Monitor {
 public:
    // Cons/destr
    Monitor(const TString &name);
    ~Monitor();

    // Read monnitor config
    void readConf(const TString &configfile);

    // Read datseheet (.csv format)
    void readDatasheet(const TString &filename);

    // Read all datasheets recursively from a top directory
    void readDatasheets(const TString &top_dir);

    // Set monitor name
    void setName(const TString &name) {
      m_name = name;
      m_name.ReplaceAll(" ", "_");
    }

    // Set monitor config name
    void setConfigName(const TString &name) {
      m_configName = name;
      m_configName.ReplaceAll(" ", "_");
    }

    // Set working directory
    void setWorkDir(const TString &path) { m_workDir = path; }

    // Set tolerance for variable
    void setTolerance(const TString &,
                      const float min, const float max,
                      const float min2 = 0., const float max2 = 0.);

    // Set run analysis flag
    void setRunAnalysis(const bool flag);
    void setRunAnalysis(const TString &flag);

    // Add run to monitor
    void addRun(const int runNb);

    // Returns all monitored PMTs
    std::vector<TString> pmts() const {
      std::vector<TString> pmts;
      for (const auto &run : this->runs()) {
        pmts.insert(pmts.end(), run.pmts().begin(), run.pmts().end());
      }
      std::sort(pmts.begin(), pmts.end());
      pmts.erase(std::unique(pmts.begin(), pmts.end()), pmts.end());
      return pmts;
    }

    // Returns all monitored runs
    const std::vector<Run> &runs() const { return m_data.keys(); }

    // Returns runs of a given type
    std::vector<Run> runs(const RunType &type) const {
      std::vector<Run> runs;
      for (const auto &run : this->runs())
        if (run.type() == type) runs.push_back(run);
      return runs;
    }

    // Returns all monitored runs of a PMT
    std::vector<Run> runs(const TString &pmt) const {
      std::vector<Run> runs;
      for (const auto &run : this->runs()) {
	auto pmts = run.pmts();
        if (std::find(pmts.begin(), pmts.end(), pmt) != pmts.end()) runs.push_back(run);
      }
      return runs;
    }

    // Returns all monitored runs of given type of a PMT
    std::vector<Run> runs(const TString &pmt, const RunType &type) const {
      auto runs = this->runs(pmt);
      auto pred = [&](const Run& run) { return run.type() != type; };
      runs.erase( std::remove_if(runs.begin(), runs.end(), pred), runs.end() ); 
      return runs;
    }

    // Returns all monitored nominal runs of a PMT
    std::vector<Run> nominalRuns(const TString &pmt) const {
      auto runs = this->runs(pmt);
      auto pred = [&](const Run& run) { return !run.isNominal(); };
      runs.erase( std::remove_if(runs.begin(), runs.end(), pred), runs.end() ); 
      return runs;
    }

    // Returns all monitored nominal runs
    std::vector<Run> nominalRuns() const {
      std::vector<Run> runs;
      for (const auto &run : this->runs()) 
	if (run.isNominal()) runs.push_back(run);
      return runs;
    }

    // Delete runs
    void deleteRuns();

    // Print list of monitored runs
    void printRuns() const;

    // Print datsheet values for MaPMTs
    void printDatasheet() const;

    // Returns PMTs outside tolerance (only for nominal and dark runs!)
    const std::vector<TString> badPMTs(const Run &run) const;

    // Run monitor
    void run();


 protected:
    // Monitor PMT of a run (user-defined)
    virtual void monitor(const Run &run, const TString &pmt) = 0;

    // Plot all plots needed (user-defined)
    virtual void makePlots(const Run &run) = 0;

    // Analyze monitored data (user-defined)
    virtual void analyze() = 0;

    // Write analysis info on file
    virtual void writeAnalysis() = 0;

    // Monitor a run
    void monitor(const Run &run);

    // Write monitoring info on file
    void write(const Run &run);

    // Fill monitoring variable and related quantities (mean, ...)
    void fillMoniVar(const Run &run, const TString &var,
                     const TString &pmt, const Data &data);

    // Tell if PMT is available in datasheet
    inline bool isInDatasheet(const TString &pmt) const {
      return m_datasheet.begin()->second.contains(pmt);
    }

    // Returns run directory
    inline TString dir(const Run &run) const {
      return Form("%s/%i/", m_workDir.Data(), run.number());
    }

    // Returns monitored variables
    const std::vector<TString> moniVars(const Run &run) const {
      if (m_data.contains(run)) return m_data[run].keys();
      else return {};
    }

    // Returns appropiate tolerances according to PMTs
    const Map<TString, Range> &tolerance(const bool are2inch) const
    { return (are2inch ? m_tol2inch : m_tol1inch); }

    // --- UTILITY FUNCTIONS ---
    // Run peak finding algorithm
    void runPeakFinding(const Run &run, const TString &pmt);

    // Returns charge (Me-)
    float charge(const float G_MAROC, const float G_ADC) const;
    Data charge(const float G_MAROC, const Data &adcs) const;

    // --- PLOTTING FUNCTIONS ---
    // Make monitoring plots of the run
    void plot(const Run &run);

    // Plot PMTs histograms
    void plotPmtHisto(const Run &run);

    // Plot variable on graph for all PMTs for a run
    TGraphErrors* plotMoniGraph(const Run &run, const TString &var,
                                const TString &yunit,
                                const Color_t col = kBlue, const TString &opt = "AP");

    // Plot variable on 1D histogram
    void plotMoniHisto(const Run &run,
                       const TString &var,
                       const TString &xtitle,
                       int nBins = 0,
                       Range range = {},
                       const Color_t col = kBlue,
                       const TString &opt = "");

    // Plot the map of variable on the test-box
    TH2F* plotMoniMap(const Run &run,
                      const TString &var,
                      const TString &zunit = "",
                      const TString &opt = "COLZ",
                      const bool draw_pmt_name = false);
                      // const bool new_canv = true);

    // Plot correlation graph wrt datasheet
    void plotMoniCorrel(const Run &run,
                        const TString &dsvar, const TString &var,
                        const TString &xunit, const TString &yunit);

    // Plot analysis variable on 1D histogram
    void plotAnaHisto(const TString &val,
                      const TString &xtitle,
                      int nBins = 0,
                      Range range = {},
                      const Color_t col = kBlue,
                      const TString &opt = "");

    // Plot test-box on canvas
    void plotBox(const Run &run,
                 const bool draw_pmt_name = true,
                 const bool draw_bad_pmts = true,
                 const TString &opt = "");

    // Plot variable tolerance on histogram
    void plotTolerance(const Range &tol, TH1F* h);

    // Plot variable tolerance on graph
    void plotTolerance(const Range &tol, TGraphErrors* gr);


    // Members
    TString m_name;                       // Monitor name
    TString m_configName;                 // Name of config. to monitor
    TString m_workDir;                    // Working directory

    TFile *m_outFile;                     // Output file
    TFile *m_anaFile;                     // Output analysis file

    bool m_runAnalysis;                   // Flag to run the analysis

    Map<TString, Range> m_tol1inch;       // Map var -> 1inch tolerances
    Map<TString, Range> m_tol2inch;       // Map var -> 2inch tolerances

    Map<Run, MoniMap> m_data;             // Map run -> data
    MoniMap m_anaData;                    // Analyized data
    MoniMap m_datasheet;                  // Datasheet data

    Map<TString, std::vector<TH1F*> > m_pmtHisto;  // Map pmt -> spectrum histogram
};
#endif  // MONITOR_MONITOR_SRC_MONITOR_H_
