#include "Run.h"

#include <string>
#include <utility>

//////////////////////////
// Read run environment //
//////////////////////////
void Run::readEnvironment(const TString &filename) {
  std::ifstream file(filename.Data(), std::ios::in);
  std::string line;
  if (!file.good()) {
    std::cout << "[ERROR] Run::readEnvironment(): Cannot find run environment: "
      << filename << std::endl;
    abort();
  }

  // Read environment
  std::cout << "[INFO] Reading environment for run " << this->number()
    << ": " << filename << std::endl;

  while (std::getline(file, line)) {
    if (line.empty()) continue;  // Skip empty line
    if (TString(line).BeginsWith("#")) continue;  // Skip comment

    std::istringstream iss(line);
    TString tag;
    iss >> tag;

    // PMT type (1 inch or 2 inch)
    if (tag == "2inchPMTs" || tag == "2inchPMT") {
      TString value;
      iss >> value;
      set2inchPMTs(value.Contains("true", TString::ECaseCompare::kIgnoreCase));
    }

    // PMT mapping
    else if (tag == "PMT") {
      TString febpos, febaddr, pos, pmt, gain;
      iss >> febpos >> febaddr >> pos >> pmt >> gain;

      // Set PMT
      PmtPos pmtpos = {febpos, pos.Atoi()};
      this->setPMT(pmt, pmtpos, gain.Atoi());
    }

    // HV value (kV)
    else if (tag == "HV") {
      TString value;
      iss >> value;
      this->setHV(value.Atof() / 1000.);
    }

    // LEDs voltages
    else if (tag == "LED1") {
      TString value, isOn;
      iss >> value >> isOn;
      this->setLed1((isOn == "ON" ? value.Atof() : 0.));
    }

    else if (tag == "LED2") {
      TString value, isOn;
      iss >> value >> isOn;
      this->setLed2((isOn == "ON" ? value.Atof() : 0.));
    }

    // PMTs temperature 
    else if (tag == "Temp") {
      TString tag2, tag3, value;
      iss >> tag2 >> tag3 >> value;
      if (tag2 == "Front" && tag3 == "Start") {
        this->setTemperature(value.Atof());
      }
    }

    // Box humidity
    else if (tag == "Humidity") {
      TString value;
      iss >> value;
      this->setHumidity(value.Atof());
    }

    else if (tag == "Current") continue;  // not stored yet!

    else {
      std::cout << "[WARNING] Run::readEnvironment(): Tag " << tag
                << " not known! Ignored!" << std::endl;
      continue;
    }
  }

  file.close();

  // Set run type according to the environment
  this->setType(this->typeFromEnv());

  std::cout << "[INFO] Run environment read successfully" << std::endl;
}


/////////////////////////////////////////////////////
// Set PMT used in the run (name, pos, MAROC gain) //
/////////////////////////////////////////////////////
void Run::setPMT(const TString &pmt, const PmtPos &pos, const int gain) {
  // Skip board w/o PMT 
  if (pmt == "" || pmt.IsDigit()) { // cover also if PMT name is empty but gain is not 
    std::cout << "[WARNING] Run::setPMT(): There is a missing PMT!" << std::endl;
    return;
  }

  // Skip if MAROC gain = 0
  if (gain == 0) {
    std::cout << "[WARNING] Run::setPMT(): MAROC gain = 0 for PMT " << pmt 
      << ". Skip!" << std::endl;
    return;
  }

  // Check pmt number on board
  if (pos.second != 1 && pos.second != 2) {
    std::cout << "[ERROR] Run::setPMT(): PMT pos. on feb should be 1 or 2!" << std::endl;
    abort();
  }

  // Check MAROC gain
  if (gain < 0 || gain > 256) {
    std::cout << "[ERROR] Run::setPMT(): MAROC gain outside range [0-256]!" << std::endl;
    abort();
  }

  // Set PMT
  m_pmtMap.set(pmt, pos);
  m_marocGain.set(pmt, gain);

  std::cout << "[INFO] PMT " << pmt << " set on feb " << pos.first
    << " pos " << pos.second << " (MAROC gain = " << gain << ")" << std::endl;
}


////////////////////////////////////////////////
// Returns run type based on environment info //
////////////////////////////////////////////////
RunType Run::typeFromEnv() const {
  RunType type;
  if (this->hvON()) {
    if (this->ledsON()) {  // it's LED run
      if (this->globalGain()) type = CalibRun;  // it's calib run
      else type = LEDRun;
    }
    else type = DarkRun;  // it's dark run
  }
  else type = BadRun;
  return type;
}


///////////////////////////////////////////////////
// Returns global gain based on MAROC gains      //
// (assume default global gain if only one PMT!) //
///////////////////////////////////////////////////
bool Run::globalGain() const {
  if (this->pmts().size() > 1) return m_marocGain.min() == m_marocGain.max(); 
  else { 
    std::cout << "[WARNING] Run::globalGain(): There is only one PMT. Decision based on default MAROC gain = " 
      << default_maroc_gain << std::endl;
    return m_marocGain[this->pmts()[0]][0] == default_maroc_gain;  
  }
}


/////////////////////////////////////
// Returns PMT bins in the box map //
/////////////////////////////////////
// Bins refer to top left corner of PMT, hence -1 for biny!
std::pair<int, int> Run::bins(const TString &pmt) const {
  int binx = 0, biny = 0;
  TString febpos = m_pmtMap[pmt].first;
  int pmtpos = m_pmtMap[pmt].second;

  // Find PMT top-left position on the box
  int ifeb = 0;
  for (auto pos : febs_pos) {
    if (febpos == pos) {
      int i = 1 - ifeb/4;  // row: 1 = up, 0 = bottom
      int j = ifeb%4;  // column: 0,1,2,3 from left to right

      // if 2inch, merge columns AB, CD, EF, GH
      if (m_2inchPMTs) j /= 2;

      // if 1inch, split rows 0,1 -> 0,1,2,3
      else {
        i *= 2;
        bool top = ((ifeb%2 == 0) && (pmtpos == 2)) ||
                   ((ifeb%2 != 0) && (pmtpos == 1));
        if (top) i++;  // PMT mounted on top of feb
      }

      binx = j*PMTDIM;
      biny = (i+1)*PMTDIM -1;
      break;
    }

    ifeb++;
  }

  return {binx, biny};
}
