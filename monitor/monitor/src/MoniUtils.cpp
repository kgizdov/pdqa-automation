// Copyright 2016 Stefano Gallorini University of Padua
// 2016 Konstantin Gizdov University of Edinburgh

#include "Monitor.h"

#include <vector>

//////////////////////////
// Returns charge (Me-) //
//////////////////////////
float Monitor::charge(const float G_MAROC, const float G_ADC) const {
  float c = -8.758;
  float d = 1.302;
  float e = -0.0014;
  float f = -0.565;
  float g = 0.013;
  float h = 0.0002;

  if (G_MAROC == 0. || G_ADC == 0.) return 0.;

  float charge = G_ADC / (c + d*G_MAROC + e*pow(G_MAROC, 2))
                 - ((f + g*G_MAROC + h*pow(G_MAROC, 2)) / (c + d*G_MAROC + e*pow(G_MAROC, 2)));

  // Charge in Me-
  charge *= 10. / 1.6;  // 1e-12 (pF) / 1.6e-19 (Qe-)
  return charge;
}

Data Monitor::charge(const float G_MAROC, const Data &adcs) const {
  Data qs;
  for (const auto &adc : adcs) qs.add(charge(G_MAROC, adc));
  return qs;
}


////////////////////////////////
// Run peak finding algorithm //
////////////////////////////////
void Monitor::runPeakFinding(const Run &run, const TString &pmt) {
  const float xmin = 0., xmax = 256.;  // spectrum histo. binning
  const int nBins = xmax - xmin;
  const float dist_ped = 5.;           // ADC distance to pedestal for dark counts
  const float shaping_time = 500e-9;   // MAROC shaping time (sec)
  const float thr = 1e-3;              // find peaks w/ height >= thr*max_height
  const float sigma = 3.;              // separation between peaks
  const float pv_min = 1.3;            // peak-to-valley ratio lower limit

  Data gains_adc, pv_ratios;
  Data pedestal_pos, signal_frac;
  Data dark_rates, dark_rate_errors;
  UShort_t PMT_ADC[NPX];

  TSpectrum *s = new TSpectrum(10);

  // Open data file
  TString filename = Form("%s/%s_%06i.root", dir(run).Data(), pmt.Data(), run.number());
  if (gSystem->AccessPathName(filename)) {
    std::cout << "[ERROR] Monitor::runPeakFinding(): File " << filename
      << " does not exist!" << std::endl;
    abort();
  }
  TFile *rootfile = TFile::Open(filename, "read");
  TTree *tree = static_cast<TTree*>(rootfile->Get("mdfTree"));
  int nEvents = tree->GetEntries();

  // Load ADC values
  TString branchName = "B01_PMT1_ADC";
  tree->SetBranchStatus("*", 0);
  tree->SetBranchStatus(branchName, 1);
  tree->SetBranchAddress(branchName, &PMT_ADC[0]);

  // Book spectrum histo
  m_pmtHisto.set(pmt);
  std::vector<TH1F*> &hspectra = m_pmtHisto[pmt];
  for (int ipx = 0; ipx < NPX; ipx++) {
    TString hname = Form("hdata_%s_px%i", pmt.Data(), ipx + 1);
    TH1F *h = new TH1F(hname, Form("Spectrum PMT %s px%i", pmt.Data(), ipx + 1), nBins, xmin, xmax);
    hspectra.push_back(h);
  }

  // Fill spectrum histos (much faster!)
  for (int iev = 0; iev < nEvents; iev++) {
    tree->GetEvent(iev);
    for (int ipx = 0; ipx < NPX; ipx++) {
      hspectra[ipx]->Fill(static_cast<float>(PMT_ADC[ipx]));
    }
  }  // evts


  // Now process each pixel
  for (int ipx = 0; ipx < NPX; ipx++) {
    TH1F *hdata = hspectra[ipx];

    // Run peaks finding
    int npeaks = s->Search(hdata, sigma, "nodraw", thr);
    Double_t *x_peaks = s->GetPositionX();
    Double_t *y_peaks = s->GetPositionY();

    // Dead channel, skip!
    if (npeaks == 0) {
      std::cout << "[WARNING] Monitor::runPeakFinding(): PMT " << pmt << ", pixel " << ipx + 1
                << " w/o peaks! Skip!" << std::endl;
      continue;
    }

    // Pedestal pos
    float ped = x_peaks[0];
    pedestal_pos.add(ped);

    // Gain ADC and peak to valley-ratio
    float gain_adc = 0., pv_ratio = 0.;
    if (npeaks > 1) {
      // use third peak if below pedestal (in case of high DCR it can happen...)
      if (npeaks > 2) {
        if (x_peaks[1] <= ped) {
          std::cout << "[WARNING] Monitor::runPeakFinding(): PMT " << pmt << ", pixel " << ipx + 1
                    << " has peak 2 below pedestal. Using peak 3!" << std::endl; s->Print("v");
          x_peaks[1] = x_peaks[2];
          y_peaks[1] = y_peaks[2];
        }
      }
      gain_adc = x_peaks[1] - ped;
      float ypeak = y_peaks[1];

      // valley search
      float yvalley = 0., xvalley = 0.;
      int binmin = hdata->GetXaxis()->FindBin(x_peaks[0]);
      int binmax = hdata->GetXaxis()->FindBin(x_peaks[1]);
      yvalley = hdata->GetMaximum();
      for (int bin = binmin; bin < binmax; bin++) {
        if (hdata->GetBinContent(bin) < yvalley) {
          xvalley = bin;
          yvalley = hdata->GetBinContent(bin);
        }
      }
      pv_ratio = ypeak / yvalley;
      TList *functions = hdata->GetListOfFunctions();  // add valley marker to histogram
      TPolyMarker *pm = static_cast<TPolyMarker*>(functions->FindObject("TPolyMarker"));
      pm->SetNextPoint(xvalley, yvalley);
    }

    else {
      if (run.isLED() || run.isCalib())
        std::cout << "[WARNING] Monitor::runPeakFinding(): Cannot find 1p.e. peak for pixel "
                  << ipx + 1 << std::endl;
    }

    gains_adc.add(gain_adc);
    pv_ratios.add(pv_ratio);

    // Signal fraction
    float nSignals = hdata->Integral(0, hdata->FindBin(ped - dist_ped));
    nSignals += hdata->Integral(hdata->FindBin(ped + dist_ped), nBins + 1);
    float frac = nSignals / nEvents;
    float efrac = std::sqrt(frac * (1. - frac) / nEvents);
    signal_frac.add(frac);

    // Dark counts rate (kHz)
    dark_rates.add(frac / shaping_time / 1e3);
    dark_rate_errors.add(efrac / shaping_time / 1e3);
  }  // ipx


  // Gains
  Data gains = charge(run.marocGain(pmt), gains_adc);

  // Uniformity (use ADC gains)
  float max = gains_adc.max();
  Data gains_wrt_max = max > 0. ? (gains_adc / max) * 100. : 0.;
  Data uniformity = gains_wrt_max.min();

  // Dark current (nA)
  Data Q = 0.;
  if (m_datasheet.contains("Average Gain", pmt)) {
    Q = (m_datasheet["Average Gain"][pmt] * 1e6) * 1.6e-19;  // Hmmm! I'm using datasheet gain!
  }
  Data dark_currents = Q * (dark_rates * 1e3) * 1e9;

  // Number of pixels below peak-to-valley limit
  int nBadPV = 0;
  for (const auto &pv : pv_ratios) if (pv < pv_min) nBadPV++;

  // Fill monitoring variables
  fillMoniVar(run, "Raw Pedestal pos", pmt, pedestal_pos);
  if (!run.isDark()) {
    fillMoniVar(run, "Raw Gain ADC", pmt, gains_adc);
    fillMoniVar(run, "Raw Gain", pmt, gains);
    fillMoniVar(run, "Raw Uniformity", pmt, uniformity);
    fillMoniVar(run, "Raw Gain wrt max", pmt, gains_wrt_max);
    fillMoniVar(run, "Raw Peak to valley ratio", pmt, pv_ratios);
    fillMoniVar(run, "Raw Number of pixels low pv ratio", pmt, nBadPV);
  }
  else {
    fillMoniVar(run, "Raw Signal fraction", pmt, signal_frac);
    fillMoniVar(run, "Dark count rate", pmt, dark_rates);
    fillMoniVar(run, "Dark count rate error", pmt, dark_rate_errors);
    fillMoniVar(run, "Dark current", pmt, dark_currents);
  }
}
