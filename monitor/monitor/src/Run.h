// Copyright 2016 Stefano Gallorini University of Padua
// 2016 Konstantin Gizdov University of Edinburgh

#ifndef MONITOR_MONITOR_SRC_RUN_H_
#define MONITOR_MONITOR_SRC_RUN_H_

#include "Data.h"

#include <list>
#include <utility>
#include <vector>

// Useful constants
#define PMTDIM 8  // PMT side dimension (in pixels)
#define NPX PMTDIM*PMTDIM  // Number of pixels of PMT

// Define run type
enum RunType { Unknown, DarkRun, CalibRun, LEDRun, BadRun };

// Typedefs
typedef std::pair<TString, int> PmtPos;

// Naming of FEBs position on the test box (front view)
// |AB CD| --> |01 23|
// |EF GH| --> |45 67|
const std::list<TString> febs_pos = {"A", "B", "C", "D", "E", "F", "G", "H"};

// Default global MAROC gain 
const int default_maroc_gain = 128;


//===============================
// Class describing a DAQ run
//===============================
class Run {
 public:
    // Cons/destr
    Run() {}
    ~Run() {}
    Run(const int number) : m_number(number) {}
    Run(const int number, const TString &filename)
      : m_number(number) { readEnvironment(filename); }

    // Returns run number
    inline int number() const { return m_number; }

    // Returns HV value (kV)
    inline float hv() const { return m_hv; }

    // Returns PMTs temperature and box humidity
    inline float temperature() const { return m_temperature; }
    inline float humidity() const { return m_humidity; }

    // Returns MAROC gains
    inline const DataMap &marocGain() const { return m_marocGain; }
    inline float marocGain(const TString &pmt) const { return m_marocGain[pmt][0]; }

    // Returns PMT map
    inline const Map<TString, PmtPos> &pmtMap() const { return m_pmtMap; }
    inline const PmtPos &pmtMap(const TString &pmt) const { return m_pmtMap[pmt]; }

    // Return list of PMTs
    const std::vector<TString> &pmts() const { return m_pmtMap.keys(); }

    // Returns run type
    inline RunType type() const { return m_type; }
    inline bool isBad() const { return (this->type() == BadRun); }
    inline bool isUnknown() const { return (this->type() == Unknown); }
    inline bool isDark() const { return (this->type() == DarkRun); }
    inline bool isLED() const { return (this->type() == LEDRun); }
    inline bool isCalib() const { return (this->type() == CalibRun); }
    inline bool isNominal() const
    { return (this->isLED() && (std::abs(this->hv() - 1.0) < 0.01)); }

    // Returns LED voltages
    inline float led1() const { return m_led1; }
    inline float led2() const { return m_led2; }

    // Returns 2inch type flag
    inline bool are2inchPMTs() const { return m_2inchPMTs; }

    // Returns number of PMTs per side (box dimension)
    inline int boxDim() const { return (m_2inchPMTs ? 2 : 4); }

    // Returns max number of PMTs in the box
    inline int nMaxPMTs() const { return boxDim() * boxDim(); }

    // Tells if LED1, LED2 or both are ON
    inline bool hvON() const { return this->hv() > 0.; }
    inline bool led1ON() const { return this->led1() > 0.; }
    inline bool led2ON() const { return this->led2() > 0.; }
    inline bool ledsON() const { return (this->led1ON() && this->led2ON()); }

    // Returns global gain based on MAROC gains
    // (compare to default global gain if there is only one PMT!)
    bool globalGain() const;

    // Returns PMT bins in the box map
    std::pair<int, int> bins(const TString &pmt) const;

    // Returns run type based on environment info 
    RunType typeFromEnv() const;

    // Set members
    void setNumber(const int nb) { m_number = nb; }
    void setHV(const float hv) { m_hv = hv; }
    void setLed1(const float volt) { m_led1 = volt; }
    void setLed2(const float volt) { m_led2 = volt; }
    void setTemperature(const float t) { m_temperature = t; }
    void setHumidity(const float h) { m_humidity = h; }
    void setType(const RunType type) { m_type = type; }

    // Set PMT type as 2inch
    void set2inchPMTs(const bool flag) {
      m_2inchPMTs = flag;
      std::cout << "[INFO] PMTs set to " << (m_2inchPMTs ? 2 : 1) << " inch type" << std::endl;
    }

    // Set PMT used in the run (name, pos, MAROC gain)
    void setPMT(const TString &pmt, const PmtPos &pos, const int gain);

    // Read run environment
    void readEnvironment(const TString &filename);

    // >, <, == overload (for map sorting)
    bool operator< (const Run &rhs) const {  // sort by HV value
      if (this->m_number < rhs.m_number) return true;
      else return false;
    }
    bool operator> (const Run &rhs) const { return !(*this < rhs); }
    bool operator== (const Run &rhs) const { return (this->m_number == rhs.m_number); }

    // << overload (print class)
    friend std::ostream& operator<< (std::ostream &os, const Run &run) {
      os << "[INFO] Run " << run.number() << " info:" << std::endl;
      os << "[INFO] 2inch PMTs: " << (run.are2inchPMTs() ? "true" : "false") << std::endl;
      os << "[INFO] Type: " << run.type() << std::endl;
      os << "[INFO] HV (kV): " << run.hv() << std::endl;
      os << "[INFO] LED1 (V): " << run.led1() << std::endl;
      os << "[INFO] LED2 (V): " << run.led2() << std::endl;
      os << "[INFO] Temperature (C): " << run.temperature() << std::endl;
      os << "[INFO] Humidity: " << run.humidity() << std::endl;
      // os << "[INFO] PMT map: " << run.pmtMap() << std::endl;
      // os << "[INFO] MAROC gains: " << run.marocGain() << std::endl;
      os << "[INFO] Global gain: " << (run.globalGain() ? "true" : "false");
      return os;
    }

 private:
    // Members
    int m_number = 0;               // run number
    RunType m_type = Unknown;       // Run type
    float m_hv = 0.;                // HV (kV)
    float m_led1 = 0.;              // LED1 voltage (V)
    float m_led2 = 0.;              // LED2 voltage (V)
    float m_temperature = 0.;       // PMTs temperature (Celsius)
    float m_humidity = 0.;          // Box humidity
    bool m_2inchPMTs = false;       // Flag for 2inch type
    Map<TString, PmtPos> m_pmtMap;  // Map pmt -> feb pos
    DataMap m_marocGain;            // Map pmt -> MAROC gain
};
typedef std::vector<Run> Runs;
#endif  // MONITOR_MONITOR_SRC_RUN_H_
