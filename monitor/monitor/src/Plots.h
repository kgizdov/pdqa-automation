// Copyright 2016 Stefano Gallorini University of Padua
// 2016 Konstantin Gizdov University of Edinburgh

#ifndef PLOTS_H
#define PLOTS_H 1

#include "Data.h"

// ROOT inc
#include "TROOT.h"
#include "TStyle.h"
#include "TTree.h"
#include "TPad.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TF1.h"
#include "TMath.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TBox.h"
#include "TLine.h"

#define RANGE_TOL 0.10

///////////////////////
// Plot graph (Data) //
///////////////////////
TGraphErrors* plotGraph(const Data &x,
                        const Data &y,
                        const Data &ex,
                        const Data &ey,
                        const TString &name,
                        const TString &title,
                        const TString &xtitle,
                        const TString &ytitle,
                        Range yrange = {},
                        const Color_t col = kBlue,
                        const TString &opt = "AP");


//////////////////////////
// Plot graph (DataMap) //
//////////////////////////
TGraphErrors* plotGraph(const DataMap &xmap,
                        const DataMap &ymap,
                        const DataMap &exmap,
                        const DataMap &eymap,
                        const TString &name,
                        const TString &title,
                        const TString &xtitle,
                        const TString &ytitle,
                        const Range &range = {},
                        const Color_t col = kBlue,
                        const TString &opt = "AP");


//////////////////////////////
// Plot 1D histogram (Data) //
//////////////////////////////
TH1F* plot1DHisto(const Data &data,
                  const TString &name,
                  const TString &title,
                  const TString &xtitle,
                  int nBins,
                  Range range,
                  const Color_t col = kBlue,
                  const TString &opt = "");


/////////////////////////////////
// Plot 1D histogram (DataMap) //
/////////////////////////////////
TH1F* plot1DHisto(const DataMap &dmap,
                  const TString &name,
                  const TString &title,
                  const TString &xtitle,
                  const int nBins,
                  const Range &range,
                  const Color_t col = kBlue,
                  const TString &opt = "");


//////////////////////////////
// Plot 2D histogram (Data) //
//////////////////////////////
TH2F* plot2DHisto(const Data &xdata,
                  const Data &ydata,
                  const TString &name,
                  const TString &title,
                  const TString &xtitle,
                  const TString &ytitle,
                  const TString &opt = "COLZ");
#endif
