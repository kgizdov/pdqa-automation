// Copyright 2016 Stefano Gallorini University of Padua
// 2016 Konstantin Gizdov University of Edinburgh

#ifndef MONITOR_OFFLINE_SRC_OFFLINEMONITOR_H_
#define MONITOR_OFFLINE_SRC_OFFLINEMONITOR_H_

#include "Monitor.h"
#include <time.h>
#include "RooPlot.h"
#include "RooCurve.h"


//=================
// Offline monitor
//=================
class OfflineMonitor: public Monitor {
 public:
    OfflineMonitor(TString name = "OfflineMonitor") : Monitor(name) {}
    ~OfflineMonitor() {}

    // Plot graphs needed (user-defined)
    void makePlots(const Run &run);

    // Process input data (user-defined)
    void monitor(const Run &run, const TString &pmt);

    // Analyze data (user-defined)
    void analyze();

    // Write analysis info on file
    void writeAnalysis();
};
#endif  // MONITOR_OFFLINE_SRC_OFFLINEMONITOR_H_
