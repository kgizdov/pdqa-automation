// Copyright 2016 Stefano Gallorini University of Padua
// 2016 Konstantin Gizdov University of Edinburgh

#include "OfflineMonitor.h"

//========================
// Run Offline Monitor
//========================
int main(int argc, char **argv) {
  int run(0);
  TString configfile(argv[1]);
  if (argc >= 3) run = atoi(argv[2]);

  // The monitor
  Monitor *moni = new OfflineMonitor("Offline");

  // Read config file
  moni->readConf(configfile);
  // moni->printDatasheet();

  // Define tolerances
  moni->setTolerance("Average Gain", 1., HUGE_VAL);  // Me-
  moni->setTolerance("Number of pixels low pv ratio", 0, 3);
  moni->setTolerance("Uniformity", 25., 100., 33., 100.);
  moni->setTolerance("Total Dark count rate", 0., 16., 0., 70.);  // kHz
  moni->setTolerance("Dark count rate", 0., 1., 0., 4.);  // kHz
  moni->setTolerance("Fit failure rate", 0., 10.);
  //moni->setTolerance("Fit chi2", 0., 30.);
  //moni->setTolerance("Fit status", -1., 1.);

  // Run monitor!
  if (run) {
    moni->deleteRuns();  // remove read runs
    moni->addRun(run);
  }
  moni->run();

  return 0;
}
