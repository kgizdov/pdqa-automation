// Copyright 2016 Stefano Gallorini University of Padua
// 2016 Konstantin Gizdov University of Edinburgh

#include "OfflineMonitor.h"

#include <vector>

////////////////
// Make plots //
////////////////
void OfflineMonitor::makePlots(const Run &run) {
  // LED run plots
  if (run.type() == LEDRun) {
    TCanvas *canv = new TCanvas("Canv_summary", "Monitor summary", 1300, 800);
    TString pdfname = Form("%s/%s_monitor_summary.pdf", dir(run).Data(), m_name.Data());
    canv->SaveAs(pdfname + "[");

    canv->Divide(3, 2);
    canv->cd(1);
    plotBox(run);

    canv->cd(2);
    plotMoniGraph(run, "Fit failure rate", "", kBlue, "APL");

    canv->cd(3);
    plotMoniMap(run, "Fit status");

    canv->cd(4);
    plotMoniGraph(run, "Average Gain", "[x10^{6}]", kBlue, "AP");

    canv->cd(5);
    plotMoniGraph(run, "Uniformity", "", kBlue, "AP");

    canv->cd(6);
    plotMoniGraph(run, "Number of pixels low pv ratio", "", kBlue, "AP");

    canv->SaveAs(pdfname);
    canv->Clear();


    canv->Divide(3, 2);
    canv->cd(1);
    plotMoniCorrel(run, "Average Gain", "Average Gain", "[x10^{6}]", "[x10^{6}]");

    canv->cd(2);
    plotMoniCorrel(run, "Average Gain", "Uniformity", "[x10^{6}]", "");

    canv->cd(3);
    plotMoniMap(run, "Gain", "[x10^{6}]");
    plotMoniMap(run, "Average Gain", "[x10^{6}]", "text same");

    canv->cd(4);
    plotMoniMap(run, "Occupancy");
    plotMoniMap(run, "Average Occupancy", "", "text same");

    canv->cd(5);
    plotMoniMap(run, "Crosstalk");
    plotMoniMap(run, "Average Crosstalk", "", "text same");
    
    canv->cd(6);
    plotMoniMap(run, "Missing probability", "[%]");
    plotMoniMap(run, "Average Missing probability", "[%]", "text same");

    canv->SaveAs(pdfname);
    canv->Clear();


    canv->Divide(3, 2);
    canv->cd(1);
    plotMoniMap(run, "Fit chi2");
    
    canv->cd(2);
    plotMoniGraph(run, "Average Signal width", "[ADC]", kBlue, "APL");

    canv->cd(3);
    plotMoniGraph(run, "Average Occupancy", "", kBlue, "APL");

    canv->cd(4);
    plotMoniGraph(run, "Average Crosstalk", "", kBlue, "APL");

    canv->cd(5);
    plotMoniGraph(run, "Average Missing probability", "[%]", kBlue, "APL");

    canv->SaveAs(pdfname);
    canv->Clear();


    plotMoniHisto(run, "Peak to valley ratio", "Peak to valley ratio");

    canv->SaveAs(pdfname);
    canv->Clear();


    // Save canvas
    canv->SaveAs(pdfname + "]");
  }
}


/////////////////////////////
// Process data of a MaPMT //
/////////////////////////////
void OfflineMonitor::monitor(const Run &run, const TString &pmt) {
  //const float pv_min = 1.3;            // peak-to-valley ratio lower limit
  const float max_signal_loss = 0.10;  // max. allowed signal loss (%)
  const int sigmas_from_ped = 5;       // sigmas from pedestal

  Data gains_adc(NPX), err_gains_adc(NPX);
  Data pedestals(NPX);
  Data pedestal_widths(NPX), signal_widths(NPX);
  Data occupancies(NPX), crosstalks(NPX), pv_ratios(NPX);
  int statuses[NPX]; Data fit_statuses(NPX);
  Data chi2(NPX), pmiss(NPX);
  Data err_pedestal_widths(NPX), err_signal_widths(NPX);
  Data err_occupancies(NPX), err_crosstalks(NPX);
  Data err_pmiss(NPX);
  Data threshold_fixed_eff(NPX), threshold_ped_cut(NPX);
  Data signal_loss_ped_cut(NPX);

  // Run peak finding algorithm
  runPeakFinding(run, pmt);

  if (run.type() != LEDRun) {
    std::cout << "[WARNING] OfflineMonitor::monitor(): Cannot monitor fit results for run " 
      << run.number() << std::endl;
    return;
  }

  // Open data file
  TString filename = Form("%s/fit_%s.root", dir(run).Data(), pmt.Data());
  if (gSystem->AccessPathName(filename)) {
    std::cout << "[ERROR] OfflineMonitor::monitor(): " << filename << " does not exist!" << std::endl;
    abort();
  }
  TFile *rootfile = TFile::Open(filename, "read");
  TTree *tree = static_cast<TTree*>(rootfile->Get("tree"));

  // Set branches
  tree->SetBranchAddress("mean_1ph", gains_adc.ptr());
  tree->SetBranchAddress("mean_n", pedestals.ptr());
  tree->SetBranchAddress("sigma_1ph", signal_widths.ptr());
  tree->SetBranchAddress("sigma_n", pedestal_widths.ptr());
  tree->SetBranchAddress("navgph", occupancies.ptr());
  tree->SetBranchAddress("navgct", crosstalks.ptr());
  tree->SetBranchAddress("pv_ratio", pv_ratios.ptr());
  tree->SetBranchAddress("status", statuses);
  tree->SetBranchAddress("chi2", chi2.ptr());
  tree->SetBranchAddress("Pmiss", pmiss.ptr());
  tree->SetBranchAddress("err_navgph", err_occupancies.ptr());
  tree->SetBranchAddress("err_navgct", err_crosstalks.ptr());
  tree->SetBranchAddress("err_Pmiss", err_pmiss.ptr());
  tree->SetBranchAddress("err_mean_1ph", err_gains_adc.ptr());
  tree->SetBranchAddress("err_sigma_1ph", err_signal_widths.ptr());
  tree->SetBranchAddress("err_sigma_n", err_pedestal_widths.ptr());
  tree->GetEntry(0);

  // Gains
  Data gains = charge(run.marocGain(pmt), gains_adc);
  Data err_gains = gains * (err_gains_adc / gains_adc);

  // Occupancy/cross-talk
  for (auto &val : occupancies) val = 1. - TMath::Exp(-val);
  for (auto &val : crosstalks) val = 1. - TMath::Exp(-val);
  for (auto &val : err_occupancies) val *= TMath::Exp(-val);
  for (auto &val : err_crosstalks) val *= TMath::Exp(-val);

  // Fit failure rate
  int nFailed = 0.;
  for (int ipx = 0; ipx < NPX; ipx++) {
    fit_statuses[ipx] = statuses[ipx];
    if (fit_statuses[ipx] != 0) nFailed++;
  }
  Data failure_rate = 100. * nFailed / static_cast<float>(NPX);

  // Uniformity (use ADC gains)
  float min = gains_adc.min();
  float max = gains_adc.max();
  if (min == 0 && max > 0) {  // try to compute uniformity skipping dead pixels
    min = +HUGE_VAL;
    for (const auto &val : gains_adc) if (val > 0 && val < min) min = val;
    if (min == +HUGE_VAL) min = 0.;
  }
  Data uniformity = 100. * (min / max);

  // Number of pixels below peak-to-valley limit
  // # NOTE: 
  // # PV given by fit is not accurate: fitted function is usually below 1pe peak
  // # giving lower PV... so we prefer PV provided by TSpectrum 
  //int nBadPV = 0;
  //for (const auto &pv : pv_ratios) if (pv < pv_min) nBadPV++;
  Data nBadPV = m_data[run]["Raw Number of pixels low pv ratio"][pmt]; // PV from TSpectrum 

  // Thresholds
  for (int ipx = 0; ipx < NPX; ipx++) {
    // From signal efficiency
    const auto plot = static_cast<RooPlot*>(rootfile->Get(Form("frame_px%i", ipx + 1)));  // get signal pdf
    if (plot == nullptr) continue;

    const auto sig = static_cast<RooCurve*>(plot->getCurve("signal_pdf_1_Norm[ADC]"));
    const float tot = sig->Integral();
    float loss = 0.;
    int adc_thr = 0;
    while (loss <= max_signal_loss) loss += sig->interpolate(++adc_thr - 0.5)/tot; // in steps of 1ADC count
    threshold_fixed_eff[ipx] = charge(run.marocGain(pmt), adc_thr - 0.5); 

    // Based on pedestal distance
    loss = 0.;
    adc_thr = pedestals[ipx] + sigmas_from_ped * pedestal_widths[ipx];
    threshold_ped_cut[ipx] = charge(run.marocGain(pmt), adc_thr);
    for (int i = 0; i < adc_thr; i++) loss += sig->interpolate(i + 0.5)/tot;
    signal_loss_ped_cut[ipx] = loss;
  }

  // Add monitoring variables
  fillMoniVar(run, "Gain ADC", pmt, gains_adc);
  fillMoniVar(run, "Gain", pmt, gains);
  fillMoniVar(run, "Signal width", pmt, signal_widths);
  fillMoniVar(run, "Pedestal width", pmt, pedestal_widths);
  fillMoniVar(run, "Occupancy", pmt, occupancies);
  fillMoniVar(run, "Crosstalk", pmt, crosstalks);
  fillMoniVar(run, "Peak to valley ratio", pmt, pv_ratios);
  fillMoniVar(run, "Number of pixels low pv ratio", pmt, nBadPV); 
  fillMoniVar(run, "Uniformity", pmt, uniformity);
  fillMoniVar(run, "Fit failure rate", pmt, failure_rate);
  fillMoniVar(run, "Fit status", pmt, fit_statuses);
  fillMoniVar(run, "Fit chi2", pmt, chi2);
  fillMoniVar(run, "Missing probability", pmt, pmiss * 100.);
  fillMoniVar(run, "Gain error", pmt, err_gains);
  fillMoniVar(run, "Occupancy error", pmt, err_occupancies);
  fillMoniVar(run, "Crosstalk error", pmt, err_crosstalks);
  fillMoniVar(run, "Missing probability error", pmt, err_pmiss);
  fillMoniVar(run, "Signal width error", pmt, err_signal_widths);
  fillMoniVar(run, "Pedestal width error", pmt, err_pedestal_widths);
  fillMoniVar(run, "Threshold fixed efficiency", pmt, threshold_fixed_eff);
  fillMoniVar(run, "Signal loss pedestal cut", pmt, signal_loss_ped_cut);
  fillMoniVar(run, "Threshold pedestal cut", pmt, threshold_ped_cut);
}


//////////////////
// Analyze data //
//////////////////
void OfflineMonitor::analyze() {
  // k-factor model
  TF1 *kFun = new TF1("kFun", "[0]*TMath::Power(x, [1]*12)", 0.85, 1.1);
  kFun->SetParameters(10., 0.4);
  std::vector<TGraph*> fit_graphs;

  // Add nominal runs data to analysis data
  for (const auto &run : nominalRuns()) 
    for (const auto &var : m_data[run].keys()) m_anaData.set(var, m_data[run][var] );

  // Loop over pmts
  for (const auto &pmt : pmts()) {
    // Analyze dark runs
    const auto &dcr_runs = runs(pmt, DarkRun);
    if (dcr_runs.size() == 0) {
      std::cout << "[WARNING] No dark runs found for pmt " << pmt << std::endl;
      continue;
    }

    Run last_dcr = dcr_runs.front(); // last DCR measurement
    for (const auto &run : dcr_runs) { 
      if (run.number() > last_dcr.number()) last_dcr = run; 
    }

    // Analyze LED runs
    const auto &led_runs = runs(pmt, LEDRun);
    if (led_runs.size() == 0) {
      std::cout << "[WARNING] No LED runs found for pmt " << pmt << std::endl;
      continue;
    }
    
    Data x, y, ex, ey; // fit k-factor
    for (const auto &run : led_runs) {
      x.add(run.hv());
      y.add(m_data[run]["Average Gain"][pmt]);
    }
    TGraphErrors* gr = plotGraph(x, y, ex, ey,
	"Gain vs HV", "Gain vs HV " + pmt, "[kV]", "Me^{-}", {}, kBlack, "AP");
    gr->Fit(kFun, "", "", 0.85, 1.1);
    Data fitted_gain = kFun->GetParameter(0);
    Data kfactor = kFun->GetParameter(1);
    fit_graphs.push_back(gr);

    // Fill analysis variables
    m_anaData.add("Dark count rate", pmt, m_data[last_dcr]["Dark count rate"][pmt]);
    m_anaData.add("Total Dark count rate", pmt, m_data[last_dcr]["Total Dark count rate"][pmt]);
    m_anaData.add("k-factor", pmt, kfactor);
  }  // pmt


  // Create canvas
  TCanvas *canv = new TCanvas("Canv_summary", "Monitor summary", 1000, 800);
  TString pdfname = Form("%s/%s_monitor_analysis_summary_%s.pdf",
      m_workDir.Data(), m_name.Data(), m_configName.Data());
  canv->SaveAs(pdfname + "[");

  // Plot k-factors fits
  canv->Divide(4, 4);
  int ipad = 0;
  for (auto &gr : fit_graphs) {
    canv->cd(++ipad);
    gr->SetMarkerSize(1);
    gr->Draw("APL");
  }
  canv->SaveAs(pdfname);
  canv->Clear();

  // Plot other variables
  canv->Divide(2, 2);
  canv->cd(1);
  plotAnaHisto("k-factor", "k-factor", 25, {0.3, 0.8});

  canv->cd(2);
  plotAnaHisto("Uniformity", "Uniformity", 25, {0., 100.});

  canv->cd(3);
  plotAnaHisto("Average Gain", " Average Gain [x10^{6}]", 25, {0., 12.});
  canv->SaveAs(pdfname);
  canv->Clear();

  canv->cd(4);
  plotAnaHisto("Peak to valley ratio", "Peak to valley ratio", 25, {0., 12.});
  canv->SaveAs(pdfname);
  canv->Clear();

  for (const auto &run : nominalRuns()) {
    plotMoniCorrel(run, "Average Gain", "Average Gain", "[x10^{6}]", "[x10^{6}]");
    canv->SaveAs(pdfname);
    canv->Clear();

    plotMoniCorrel(run, "Average Gain", "Uniformity", "[x10^{6}]", "");
    canv->SaveAs(pdfname);
    canv->Clear();
  }

  // Save canvas
  canv->SaveAs(pdfname + "]");
}


/////////////////////////////////
// Write analysis info on file //
/////////////////////////////////
void OfflineMonitor::writeAnalysis() {
  const int HV_step = 50;  // HV step (V)
  const float acquisition_rate = 1.;  // Acquisition rate (1kHz)

  // Lambda to avoid writing NaN
  auto safe_fprintf = [](FILE* outfile, const float &val)
  { fprintf(outfile, "%.3f,", std::isnan(val) ? 0. : val); };

  // Output file
  TString fname = Form("%s/%s_monitor_analysis_data_%s.csv",
      m_workDir.Data(), m_name.Data(), m_configName.Data());
  FILE *outfile = fopen(fname.Data(), "w");

  //time_t theTime = time(NULL);
  //struct tm *aTime = localtime( &theTime );
  //int day = aTime->tm_mday;
  //int month = aTime->tm_mon + 1;  // Month is 0 - 11, add 1 to get a jan-dec 1-12 concept
  //int year = aTime->tm_year + 1900;  // Year is # years since 1900

  // Write file
  fprintf(outfile, "Padova,");
  fprintf(outfile, "DynodesConversionModel,");
  fprintf(outfile, "TestDate(DD/MM/YY)");
  //fprintf(outfile, "%i/%i/%i,", day, month, year);
  fprintf(outfile, "\n");

  // Loop over pmts
  for (const auto &pmt : pmts()) {
    fprintf(outfile, "%s,", pmt.Data());
    safe_fprintf(outfile, m_anaData["k-factor"][pmt][0]);
    safe_fprintf(outfile, m_anaData["Total Dark count rate"][pmt][0]);
    safe_fprintf(outfile, m_anaData["Uniformity"][pmt][0]);
    safe_fprintf(outfile, m_anaData["Average Crosstalk"][pmt][0]);
    fprintf(outfile, "%s,", (m_anaData["Number of pixels low pv ratio"][pmt].range() > 
	                     m_tol1inch["Number of pixels low pv ratio"] ? "NOT_OK" : "OK"));
    fprintf(outfile, "LoadNumber");   // Load number (to be implemented)
    fprintf(outfile, "\n");

    for (const auto &val : m_anaData["Dark count rate"][pmt]) safe_fprintf(outfile, val);
    fprintf(outfile, "\n");

    // Loop over LED runs only!
    for (const auto &run : runs(LEDRun)) {
      int round_hv = static_cast<int>(1000.*run.hv()) + HV_step/2;  // round HV value
      round_hv -= round_hv % HV_step;
      fprintf(outfile, "%i,", round_hv);
      fprintf(outfile, "%i,", run.number());
      safe_fprintf(outfile, acquisition_rate);  // Acquisition rate (kHz)
      safe_fprintf(outfile, run.temperature());
      safe_fprintf(outfile, run.humidity());
      fprintf(outfile, "\n");

      safe_fprintf(outfile, m_data[run]["Average Raw Gain"][pmt][0]);
      safe_fprintf(outfile, m_data[run]["Average Gain"][pmt][0]);
      safe_fprintf(outfile, m_data[run]["Average Gain error"][pmt][0]);
      safe_fprintf(outfile, m_data[run]["Average Threshold fixed efficiency"][pmt][0]);
      fprintf(outfile, "\n");

      for (int ipx = 0; ipx < NPX; ipx++) {
        safe_fprintf(outfile, m_data[run]["Raw Gain"][pmt][ipx]);
        safe_fprintf(outfile, m_data[run]["Gain"][pmt][ipx]);
        safe_fprintf(outfile, m_data[run]["Gain error"][pmt][ipx]);
        safe_fprintf(outfile, m_data[run]["Signal width"][pmt][ipx]);
        safe_fprintf(outfile, m_data[run]["Signal width error"][pmt][ipx]);
        safe_fprintf(outfile, m_data[run]["Occupancy"][pmt][ipx]);
        safe_fprintf(outfile, m_data[run]["Occupancy error"][pmt][ipx]);
        safe_fprintf(outfile, m_data[run]["Signal loss pedestal cut"][pmt][ipx]);
        // safe_fprintf(outfile, m_data[run]["Pedestal width"][pmt][ipx]);
        // safe_fprintf(outfile, m_data[run]["Pedestal width error"][pmt][ipx]);
        safe_fprintf(outfile, m_data[run]["Threshold pedestal cut"][pmt][ipx]);
        safe_fprintf(outfile, m_data[run]["Peak to valley ratio"][pmt][ipx]);
        safe_fprintf(outfile, m_data[run]["Threshold fixed efficiency"][pmt][ipx]);
        safe_fprintf(outfile, m_data[run]["Crosstalk"][pmt][ipx]);
        safe_fprintf(outfile, m_data[run]["Crosstalk error"][pmt][ipx]);
        safe_fprintf(outfile, m_data[run]["Missing probability"][pmt][ipx]);
        safe_fprintf(outfile, m_data[run]["Missing probability error"][pmt][ipx]);
        fprintf(outfile, "\n");
      }  // ipx
    }  // runs
  }  // pmts

  fclose(outfile);
}
