SUBDIRS = gain hv lib-proxy temp histMaker treeMaker monitor/ fit/
SUBDIRSCLEAN = $(addsuffix .clean,$(SUBDIRS))

.PHONY: all subdirs $(SUBDIRS) $(SUBDIRSCLEAN) message clean

all: subdirs message

subdirs: $(SUBDIRS)

$(SUBDIRS):
	$(MAKE) -C $@

monitor/online: monitor/monitor

monitor/offline: monitor/monitor

message:
	@echo '#########################################'
	@echo '############### COMPLETED ###############'
	@echo '#########################################'

clean: $(SUBDIRSCLEAN)

$(SUBDIRSCLEAN): %.clean:
	$(MAKE) -C $* clean
