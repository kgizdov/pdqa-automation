# Proxy Library for Photon Detector Quality Assurance (lib-proxy, hv, temp)

Copyright &copy; 2016 Konstantin Gizdov University of Edinburgh

### Build Requirements

- TinyXML 2 - for accessing XML settings (included)

- Digilent Adept SDK & libraries - for trigger board communication (download required)

#### Tested on Scientific Linux 6 (and up), C++03 (and up) compliant

=======

This is a low-level interface to the MAROC and CLARO boards
including the Chimaera2 trigger and backend boards.

It is normally operated by a LabView high-level UI,
but it can also communicate with other processes over STDIO (Unix pipes)
as long as the command protocol is followed. Therefore, it is a general
library for this purpose.

# Monitoring and Analysis Automation for Photon Detector Quality Assurance (fit, monitor, treeMaker)

Copyright &copy;

2016 Stefano Gallorini Universita Degli Studi di Padova

2016 Silvia Gambetta University of Edinburgh

2016 Konstantin Gizdov University of Edinburgh

### Build Requirements

- CERN ROOT 6 - analysis framework (download required)

#### Tested on Scientific Linux 6 (and up), C++11 (and up) compliant

=======

Online processing and analysis of acquired data

Offline analysis of PMT parameters and quality tests

Normally operated by a LabView high-level UI,
however it also provides helper scripts for manual operation
