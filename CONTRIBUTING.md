## Contributing to this project

### Table of contents
* [Workflow](#workflow)
* [Programming Standards](#programming-standards)
* [Git](#git)
* [Giving Credit](#giving-credit)

### Workflow
Projects that have many people contributing to them are difficult to manage without a reliable system to organize those contributions. For this project this hard task is accomplished by using the [Git](#git) framework.

##### Git repository structure
* The repository has 3 main branches - master, testing, dev
* `master` branch is for tried and tested stable conforming code only and is a protected branch
* `testing` branch is for code that has passed initial surface tests while in the dev branch and is being tested in the lab under real runtime conditions
* `dev` branch is a staging ground for new features/bug fixes/contributions by individual developers
* Each developer makes a personal [branch](#making-your-own-branch) to work on

##### Simple workflow definition
A simple workflow, based on know Git principles, is defined to streamline and make life easy to all participants. It is as follows:
* Each developer works on their own personal branch until they feel they have something to publish
* Once  a "feature" is ready to be tested, the developer should merge the appropriate changes to the `dev` branch
* This is accomplished by firstly [rebasing](#rebasing) their personal branch and then doing a [fast-forward](#rebasing) merge into `dev`
* The updated code is reviewed and run locally by multiple people (different machines) to make sure it works before putting it in the lab
* After this another fast-forward merge is done into `testing`
* The new code is tested in the lab
* Once proper operation is confirmed, changes are merged into `master`

### Programming Standards

**The environment in which this project will run is Scientific Linux 6 with official support of up to C++03. Therefore all code should respect those limitations and avoid syntax that is specific to C++11 and higher standards.**

There are ways to bring newer features and options into that environment, but it will be done in exceptions where it is mandatory and as the common rule.

##### Some simple rules to follow:
* Use spaces instead of tabs for indentation
* Format your code so that it is easily readable
* Do not leave empty spaces at the end of lines
* Comment your code, so that others can understand it
* Try to test your code before committing
* ROOT macros are not accepted as contributions - your code must run as a compiled binary
* Make sure there are no local dependencies that prevent your code from running elsewhere apart from your laptop
* Create clear header files including small description of classes and member functions
* Headers should only contain prototypes
* Headers must include preprocessor guards
* List to be updated

### Git
##### Checking out branches
Checking out a branch means to update all the files in the project to the versions tied to it and tell Git to track only its history. This makes the checked out branch your current working branch.

`git checkout <branch name>` - accomplishes just that

Sometimes this will not succeed if you have changes which have not been committed to Git, but you are trying to check out a branch which will overwrite them. This is to prevent destroying your current progress. To correct this stash your un-tracked changes for later like so:

`git stash`

When you want to re-apply your changes to the source tree run:

`git stash apply`

##### Committing

`git add <name of updated file>`

or

`git add .` to add all changed files in current folder

then

`git commit -m "Commit message"` to commit the added changes

##### Pushing
**Before pushing to the repository, please make sure that you have run the following command:**

`git config --global push.default simple`

**This is to ensure that your changes will be propagated only to the relevant branch that you have selected.**
To push simply type:

`git push`

This will update the remote repository with the changes of your current branch.
Sometimes you might want to change the remote destination of you current branch, which can be done like so:

`git push --set-upstream origin <name of remote branch>`

##### Making your own branch

`git branch <name of branch>` - to create it from the current branch

`git checkout <name of branch>` - to move and track it as the current branch

You can accomplish the above two with one command like so:

`git checkout -b <name of branch>`

##### Merging branches
**Normally do not this directly! Make sure your merge will be a [fast-forward](#rebasing)**

`git checkout <name of branch to merge into>`

`git merge <name of branch whose changes to merge into current one>`

##### Rebasing
Once you create your branch in the repository and start working, it might become outdated and fall out of sync with the rest of the main branches. This happens when another developer pushes/merges a new feature while you are working on something else. That is expected and normal practice. To ensure that individual contributions can be easily added on top of each other and distinguished in the project, developers are urged to synchronize their branches before merging their personal changes into `dev`. This is required to achieve so called **fast-forward merges** - merging of one branch into the other will not require user intervention and nit-picking small changes that could break things. **This procedure is essential to the workflow.**

To update your personal branch with the latest version of the `dev` branch, checkout your branch as your current and run:

`git checkout <my branch>`

`git rebase dev`

Now your branch will include all updates since the last time you merged with `dev` and your changed on top of this. It is possible, although rarely, that there will be a conflict while you are rebasing. Simply run:

`git status`

to see which files are affected, then edit/fix manually, add and commit them. Or run:

`git rebase --interactive` same as `git rebase -i`

to have Git guide you though this process if you are unsure of how to proceed.

### Giving Credit

**Giving credit where credit is due**

Please make sure to mark all your original files and contributions with a comment at the top like so:
* C++ - `// Copyright <year> <name> <institution>`
* Python - `# Copyright <year> <name> <institution>`
* etc.

If you contribute to a file/project add your name below the last named contributor as needed:
* C++
```
// Copyright <year> <name> <institution>
// <year> <your name> <your institution>
```
* Python
```
// Copyright <year> <name> <institution>
// <year> <your name> <your institution>
```
* etc.

Follow appropriate syntax for other languages/source files and try to take as little space as possible.