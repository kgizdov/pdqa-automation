// Copyright 2016 Konstantin Gizdov University of Edinburgh

#ifndef __COMMON_PREFS_HPP__
#define __COMMON_PREFS_HPP__

#include <string>

struct pbwStruct {
    bool enable;
    std::string fifo;
};

struct trigStruct {
    bool enable;
    std::string fifo;
};

struct proxyStruct {
    std::string path;
    pbwStruct pbw;
    trigStruct trig;
};

struct runtimeStruct {
    std::string feb_type;
    int latency;
    int maxEvents;
    int pulseCount;
    int pulseDelay;
    bool recording;
    unsigned runNumber;
    std::string saveFileDestination;
    bool saveRunPreferences;
    int strobeLength;
    int tag;
    int triggerDelay;
    bool triggerPanel;
};

struct scanStruct {
    int begin;
    int end;
    int maxEvents;
    int step;
};

struct commonPrefsStruct{
    proxyStruct proxy;
    runtimeStruct runtime;
    scanStruct scan;
};

extern commonPrefsStruct commonPrefs;

#endif  // __COMMON_PREFS_HPP__
