// Copyright 2016 Konstantin Gizdov University of Edinburgh

#ifndef __DNA_HPP__
#define __DNA_HPP__

#include <string>
#include <utility>
#include <vector>

#include "../include/tinyxml2.h"
#include "../include/custom.hpp"

class Dna {
 public:
    Dna();
    Dna(const Dna &);
    Dna &operator=(const Dna &);
    ~Dna();
    // explicit Dna(std::string devName);
    void init();

    dna_t getDNA(int i);
    dna_t getDNA(std::string name);
    std::vector<dna_t> getDNAs();
    std::vector<dna_t> getDNAs() const;

    unsigned dnaNum();

    int addDNA(std::string name);
    int addDNA(std::string name, std::string ip, std::string mac);
    int addDNA(std::string name, std::string ip, std::string mac, int on);
    int addDNA(std::string name, std::string ip, std::string mac, int trig_channel, int on);
    int addDNAs(std::vector<std::string> names, std::vector<std::string> ips, std::vector<std::string> macs);
    int addDNAs(std::vector<std::string> names, std::vector<std::string> ips, std::vector<std::string> macs,
                std::vector<int> ons);
    int addDNAs(std::vector<std::string> names, std::vector<std::string> ips, std::vector<std::string> macs,
                std::vector<int> trig_channels, std::vector<int> ons);

    int setName(std::string name, int which);
    int setName(std::string name, std::string which);
    std::string getName(int which);
    std::vector<std::string> getNames();

    int setCustomFileName(std::string file, int which);
    std::string getFileName(int which);
    std::vector<std::string> getFileNames();

    int setCustomFolderName(std::string folder, int which);
    int setCustomFolderNames(std::string folder);
    std::string getFolderName(int which);
    std::vector<std::string> getFolderNames();

    int getDB(int which);
    std::vector<int> getDBs();

    int getON(int which);
    std::vector<int> getONs();
    std::vector<bool> getBoolONs();
    std::vector<bool> getTrigMap();
    bool checkBoolONs();

    int setGain(int which, int pmt, int gain);
    int setGain(std::string name, int pmt, int gain);
    int setGainAll(int gain);
    int getGain(int which, int pmt);
    std::vector<int> getGainAll();

    int setIP(std::string ip, int which);
    int setIP(std::string ip, std::string name);
    std::string getIP(int which);
    std::vector<std::string> getIPs();

    int setMAC(std::string mac, int which);
    int setMAC(std::string mac, std::string name);
    std::string getMAC(int which);
    std::vector<std::string> getMACs();

    int setTriggerType(int trig);
    int setTriggerType(int trig, int which);
    int getTriggerType(int which);
    std::vector<int> getTriggerTypes();
    std::vector<bool> getBoolTriggerTypes();

    int setTestPulse(bool on);
    int setTestPulse(bool on, int which);
    bool checkTestPulse();
    int checkTestPulse(int which);
    std::vector<bool> getTestPulse();

    int setDACval(int DACval);
    int setDACval(int DACval, int which);
    int getDACval(int which);
    std::vector<int> getDACvals();

    int setHoldDelay(int hold_delay);
    int setHoldDelay(int hold_delay, int which);
    int getHoldDelay(int which);
    std::vector<int> getHoldDelays();

    int setTriggerChannel(int trig_channel, int which);
    int setTriggerChannels(std::vector<int> trig_channels);
    int getTriggerChannel(int which);
    std::vector<int> getTriggerChannels();

    int LoadSettings(const std::string &settings_file_name = "./settings/settings_template.xml");
    tinyxml2::XMLDocument *getXMLDoc(int which);
    tinyxml2::XMLDocument *getXMLDoc(int which) const;
    std::vector<tinyxml2::XMLDocument *> getXMLDocs();
    std::vector<tinyxml2::XMLDocument *> getXMLDocs() const;
    int UpdateXMLGain(int which);
    int UpdateXMLGains();
    int UpdateXMLCTest(int which);
    int UpdateXMLCTests();

 private:
    int setFileNames();
    int setFileName(int which);

    int setFolderNames();
    int setFolderName(int which);

    int setDB(int which);
    int setON(int on, int which);
    int setON(bool on, int which);

    std::vector<std::pair<dna_t, tinyxml2::XMLDocument *> > dnas;
};

#endif  // __DNA_HPP__
