/* Copyright
RICH EC Control Panel
2016 Konstantin Gizdov University of Edinburgh
Steve Wotton, 2000-2015
Jackson Smith
Cavendish Lab (HEP)
*/

#ifndef INCLUDE_MAROC3CSR_H_
#define INCLUDE_MAROC3CSR_H_

union maroc3SumGain_u_t {
    unsigned char uc[2];
    struct {
        unsigned short cmd_sum :1;
        unsigned short gain :8;
    } ctl;
};

// Bitfield for MAROC3 configuration
// Excludes mask_OR, sum/gain and ctest - they cannot be aligned because the overlap word boundaries.

union maroc3Control_u_t {
    unsigned char uc[104];

    struct {
        unsigned en_otabg :1;   // Not used on MAROC3
        unsigned en_dac :1;     // Not used on MAROC3
        unsigned small_dac :1;
        unsigned DAC1 :10;
        unsigned DAC0 :10;
        unsigned enb_outADC :1;
        unsigned inv_startCmptGray :1;
        unsigned ramp_8bit :1;
        unsigned ramp_10bit :1;
        unsigned mask_or_e :5;
        unsigned mask_or_d :32;
        unsigned mask_or_c :32;
        unsigned mask_or_b :32;
        unsigned mask_or_a :27;
        unsigned cmd_CK_mux :1;
        unsigned d1_d2 :1;
        unsigned inv_discriADC :1;
        unsigned polar_discri :1;
        unsigned enb_tristate :1;
        unsigned valid_dc_fsb2 :1;
        unsigned sw_fsb2_50f :1;
        unsigned sw_fsb2_100f :1;
        unsigned sw_fsb2_100k :1;
        unsigned sw_fsb2_50k :1;
        unsigned valid_dc_fs :1;
        unsigned cmd_fsb_fsu :1;
        unsigned sw_fsb1_50f :1;
        unsigned sw_fsb1_100f :1;
        unsigned sw_fsb1_100k :1;
        unsigned sw_fsb1_50k :1;
        unsigned sw_fsu_100k :1;
        unsigned sw_fsu_50k :1;
        unsigned sw_fsu_25k :1;
        unsigned sw_fsu_40f :1;
        unsigned sw_fsu_20f :1;
        unsigned h1h2_choice :1;
        unsigned en_adc :1;
        unsigned sw_ss_1200f :1;
        unsigned sw_ss_600f :1;
        unsigned sw_ss_300f :1;
        unsigned onoff_ss :1;
        unsigned swb_buf_2p :1;
        unsigned swb_buf_1p :1;
        unsigned swb_buf_500f :1;
        unsigned swb_buf_250f :1;
        unsigned cmd_fsb :1;
        unsigned cmd_ss :1;
        unsigned cmd_fsu :1;
    } ctl;
};


#endif  // INCLUDE_MAROC3CSR_H_
