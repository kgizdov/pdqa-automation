/* Copyright
RICH EC Control Panel
2016 Konstantin Gizdov University of Edinburgh
Steve Wotton, 2000-2015
Jackson Smith
Cavendish Lab (HEP)
Konstantin Gizdov  2016 (Uni-Edinburgh)
*/

#ifndef __ADEPT_DEPP_HPP__
#define __ADEPT_DEPP_HPP__

#include <string>

#include "./custom.hpp"

// Digilent Adept headers
// #undef WIN32
#include "./dpcdecl.h"
#include "./depp.h"
#include "./dmgr.h"
// #define WIN32 1

class AdeptDepp {
 public:
    AdeptDepp();
    ~AdeptDepp();
    explicit AdeptDepp(std::string devName);
    int Enumerate();
    std::string getEnumeratedDevice(int idx);
    int deppOpen(std::string deviceString);
    int deppClose();
    int deppGetSet(byte addrArray[], size_t addr_size, byte dataArray[], size_t data_size);
    int deppPutSet(byte buf[], size_t buf_size);
    int deppPutRepeat(byte addr, byte buf[], size_t buf_size);
    int deppGetRepeat(byte addr, byte buf[], size_t buf_size);
    bool OK();
    void connect();
    void disconnect();
    void open();
    void close();
    void get(byte addr, byte buf[], size_t buf_size);
    void get(byte addr[], size_t addr_size, byte data[], size_t data_size);
    void put(byte addr, byte data);
    void put(byte buf[], size_t buf_size);
    void push(byte addr, byte buf[], size_t buf_size);
    void pop(byte addr, byte buf[], size_t buf_size);
    void putByte(int base, int addr, int val);
    void putBytes(int base, int addr, byte regs[], size_t buf_size);

 private:
    HIF hif;
    bool isOK;
    std::string deviceString;
};

#endif  // __ADEPT_DEPP_HPP__
