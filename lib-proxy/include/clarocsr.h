/* Copyright
RICH EC Control Panel
Steve Wotton, 2000-2015
Jackson Smith
Cavendish Lab (HEP)
Konstantin Gizdov (Uni-Edinburgh)
*/

#ifndef INCLUDE_CLAROCSR_H_
#define INCLUDE_CLAROCSR_H_

struct channelpair_t {
  unsigned thresh1 : 6;
  unsigned atten1  : 2;
  unsigned inEnb1  : 1;
  unsigned HysDis1 : 1;
  unsigned TPDis1  : 1;
  unsigned thresholdOffset1  : 1;
  unsigned thresh2 : 6;
  unsigned atten2  : 2;
  unsigned inEnb2  : 1;
  unsigned HysDis2 : 1;
  unsigned TPDis2  : 1;
  unsigned thresholdOffset2  : 1;
};

struct clarogencfg {
  unsigned seu_count_reset  : 1;
  unsigned testpulse_enable : 1;
  unsigned aux_testpulse    : 1;
  unsigned internal_correct : 1;
  unsigned seu_gen_enable   : 1;
  unsigned mux_address      : 3;
  unsigned SEU_count        : 16;
  unsigned CLARO_comp_latch : 8;
};


struct claroControl_t {
  unsigned char chanconfig[12];  // number of BYTES taken up by the contained struct
  clarogencfg cfg;
};

union claroControl_u_t {
  unsigned char uc[16];
  claroControl_t ctl;
};


#endif  // INCLUDE_CLAROCSR_H_
