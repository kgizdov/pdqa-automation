/* Copyright
RICH EC Control Panel
2016 Konstantin Gizdov University of Edinburgh
Steve Wotton, 2000-2015
Jackson Smith
Cavendish Lab (HEP)
*/

#ifndef __MAPMTFEB_HPP__
#define __MAPMTFEB_HPP__

#include <stdint.h>
#include <string>
#include "./custom.hpp"
#include "./mapmtfeb.h"
#include "./common_prefs.hpp"
#include "./tinyxml2.h"

union fe_ctl_u {
  maroc_ctl_t maroc;
  claro_ctl_t claro;
};

struct mapmtfebControl_t {
    febmap_t feb;
    fe_ctl_u fe;
    bb_ctl_t bbctl;
};


// Helper functions to access the MAPMTFEB register map

void getIPFromString(std::string ip, int ipArray[]);

void getMACFromString(std::string mac, int macArray[]);

class MAPMTFEB {
 public:
    MAPMTFEB();
    ~MAPMTFEB();
    void changeTrigger(int trigtype);
    // void setStateFromPreferences(Preferences prefs, Preferences prefsCommon, Preferences prefsRuntime); //reimplement in C++
    void setStateFromPreferences(const commonPrefsStruct& commonPrefs, dna_t dna);

    void getRegisterArray(int regs[]);
    void getRegisterArray(byte regs[], int nreg, int offset);

    int setPC_MAC(std::string mac);
    std::string getPC_MAC();

    int setPC_IP(std::string ip);
    std::string getPC_IP();

    void setECVersion(int number);
    int getECVersion();

    void setClaroBaseAddr(int number);
    void setDACBaseAddr(int number);
    void setDACval(int number);
    bool getSkipSmb();
    void setSkipSmb(bool flag);
    bool getTestPulseEnable();
    void setTestPulseEnable(bool flag);

    bool getMarCmuxrpt(int mar);
    void setMarCmuxrpt(bool flag, int mar);
    bool getMarAdcEnable(int mar);
    void setMarAdcEnable(int mar, bool flag);
    int getMarCmuxStop();
    void setMarCmuxStop(int ch);
    int getMarHoldDelay();
    void setMarHoldDelay(int delay);
    bool getMarSkipCfg(int mar);
    void setMarSkipCfg(bool flag, int mar);
    void setStrobeLength(int strobe_length);
    int getStrobeLength();
    void setInvertFE(bool flag);
    bool getInvertFE();
    void setEnableMIIM(bool flag);
    bool getEnableMIIM();
    void setSourcesComplement(int sources);
    int getSourcesComplement();
    void setSourcesEnable(int sources);
    int getSourcesEnable();
    void setSourcesTrigger(int sources);
    int getSourcesTrigger();

    void setSourceMacAddress(int mac[]);
    void getSourceMacAddress(int mac[]);
    void setDestinationMacAddress(int mac[]);
    void getDestinationMacAddress(int mac[]);
    void setSourceIpAddress(int ip[]);
    void getSourceIpAddress(int ip[]);
    void setDestinationIpAddress(int ip[]);
    void getDestinationIpAddress(int ip[]);
    void setIpProtocol(int ip_proto);
    int  getIpProtocol();
    void setIpTtl(int ip_ttl);
    int  getIpTtl();

    void setMepFixedDestination(bool flag);
    bool  getMepFixedDestination();
    void setMepTfcControl(bool flag);
    bool  getMepTfcControl();
    void setMepEventCount(int mep_evtcnt);
    int  getMepEventCount();

    void setThrottleAEnable(bool flag);
    bool  getThrottleAEnable();
    void setThrottleBEnable(bool flag);
    bool  getThrottleBEnable();
    void setInvertedThrottleAPolarity(bool flag);
    bool  getInvertedThrottleAPolarity();
    void setInvertedThrottleBPolarity(bool flag);
    bool  getInvertedThrottleBPolarity();

    void setTfcDecoderEnable(bool flag);
    bool  getTfcDecoderEnable();

    void setGbePollEnable(bool flag);
    bool  getGbePollEnable();

    void setGbePort(int gbe_port);
    int  getGbePort();
    void setGbeFixedPort(bool flag);
    bool  getGbeFixedPort();

    void setPartitionId(int partition);
    int  getPartitionId();

    void setTag(int tag);
    int getTag();

    void setLatencyCompensation(int latency);
    int  getLatencyCompensation();

    void setL1Id(int l1id);
    int  getL1Id();

    void setIngressInhibit(int inhb);
    int  getIngressInhibit();

    void setDummyIngressSize(int size);
    int  getDummyIngressSize();

    void setTruncationHwm(int hwm);
    int  getTruncationHwm();

    void setAliceMode(bool flag);
    bool getAliceMode();

    void setPulseCount(int count);
    int getPulseCount();
    void setPulseDelay(int delay);
    int getPulseDelay();
    void setLatency(int latency);
    int getLatency();
    void setTriggerDelay(int delay);
    int getTriggerDelay();

    void setDaqReset(bool flag);
    bool getDaqReset();
    void setDummyIngressReset(bool flag);
    bool getDummyIngressReset();
    void setGate(bool flag);
    bool getGate();

    // Status info

    bool isReset();
    bool isReady();
    bool isUpDCMLocked();
    bool isDownDCMLocked();
    bool isDCMsLocked();

    bool isTfcFifoReady();
    bool isTfcFifoFull();
    bool isRxSignalDetect();

    int getL0MuxWriteAddress();
    int getL0MuxReadAddress();
    int getL0MuxTfcFifoWriteAddress();
    int getL0MuxTfcFifoReadAddress();
    int getL0MuxOccupancy();
    int getL0MuxTfcFifoOccupancy();

    bool isTfcFifoDataRequestAsserted();
    bool isTtcReady();

    bool isSpi3TxBusy();
    bool isMepFifoFull();
    bool isThrottleAsserted();

    bool isTfcL0ResetAsserted();
    bool isTfcL1ResetAsserted();

    int getTfcBShort();
    int getTpa();

    bool isFragmentReady();
    bool isMepReady();
    bool isFragmentRequestAsserted();
    bool isEventQValidAsserted();
    bool isEthernetQValidAsserted();
    bool isTfcSequenceErrorAsserted();

    int getTfcBLong();
    int getMepFifoOccupancy();
    int getTfcFifoOccupancy();
    int getTfcFifoInCount();
    int getTfcFifoOutCount();
    int getE100RxInCount();
    int getE100RxOutCount();
    int getE100TxInCount();
    int getE100TxOutCount();

    // Per-channel status info

    int getParityErrorCount(int ingressId, int channelId);
    int getIngressCount(int ingressId, int channelId);
    int getCosynchroCount(int ingressId, int channelId);
    int getClockCorrectionCount(int ingressId, int channelId);
    int getBufferErrorCount(int ingressId, int channelId);
    int getZSCount(int ingressId, int channelId);
    int getZSFifoReadAddress(int ingressId, int channelId);
    int getZSFifoWriteAddress(int ingressId, int channelId);
    int getZSA(int ingressId, int channelId);
    int getZSB(int ingressId, int channelId);
    int getL0TimeOfArrival(int ingressId, int channelId);
    int getTfcTimeOfArrival(int ingressId, int channelId);

    int getRxLos(int ingressId, int channelId);
    bool getRxBufferHalfFull(int ingressId, int channelId);
    bool getRxBufferOverflow(int ingressId, int channelId);
    bool getGtInhibit(int ingressId, int channelId);
    bool getCosynchroError(int ingressId, int channelId);
    bool getIngressBufferReady(int ingressId, int channelId);
    bool getIngressBufferFull(int ingressId, int channelId);

 private:
    mapmtfebControl_t mapmtControl;
    pc_t pc;
};

#endif  // __MAPMTFEB_HPP__
