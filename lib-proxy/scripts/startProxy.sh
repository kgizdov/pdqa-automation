#!/bin/bash

# The following line redirects error messages. Can be changed.
# Do not redirect stdin or stdout. They are used for communication with the GUI
exec 2>feb-proxy.log

sudo ./feb-proxy $@

echo "feb-proxy done." >&2
