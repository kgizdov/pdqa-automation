/* Copyright
MAROC parameter panel

Steve Wotton, 2015-
Cavendish Lab (HEP)
Konstantin Gizdov 2016
Uni Edinburgh
*/

package cbsw.lhcb;

import java.awt.*;
import java.util.prefs.Preferences;
import java.text.DecimalFormat;
import cbsw.gui.*;
import cbsw.lhcb.*;

public class MAROCControl extends GuiJPanel
{

public MAROCControl(Preferences pf )
{
  super(pf,"maroc.");

// Set up layout style

  setLayout( new GridBagLayout() );
  GridBagConstraints gbC = new GridBagConstraints();
  gbC.anchor = GridBagConstraints.CENTER;
  gbC.insets = new Insets(2,2,2,2);
  gbC.weightx = 1.0f;
  gbC.weighty = 1.0f;
  gbC.gridwidth = GridBagConstraints.REMAINDER;

// Start adding the components

// DAC and gain settings are done through JRichEcConfigurator
//   add( makeTF(new DecimalFormat("#"),"dac0", "128", 4, "DAC0", "DAC0 value [1,1023]",true).getParent(),gbC );
//   add( makeTF(new DecimalFormat("#"),"dac1", "128", 4, "DAC1", "DAC1 value [1,1023]",true).getParent(),gbC );
//  add( makeTF(new DecimalFormat("#"),"gain", "128", 3, "Gain", "Gain [0,255]",true).getParent(),gbC );

  add( makeTF(new DecimalFormat("#"),"cmuxstop", "127", 3, "Cmux stop", "Cmux stop at channel [0,127]",true).getParent(),gbC );
  add( makeCheckBox("cmuxrpt","CMux repeat",false,true), gbC );
  add( makeCheckBox("skipcfg","Skip config",false,true), gbC );
  add( makeCheckBox("adc-enable.1","MAROC1 ADC",false,true), gbC );
  add( makeCheckBox("adc-enable.2","MAROC2 ADC",false,true), gbC );

  return;

}

}
