/*
 * ==================================================
 * JRichTBTrigger
 * ==================================================
 */
package cbsw.lhcb;
import java.util.Date;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
//import java.applet.Applet;
//import java.applet.AudioClip;
//import java.net.URL;
import java.io.*;
import java.util.prefs.Preferences;
import cbsw.lhcb.*;
import cbsw.instrument.adept.*;
import cbsw.instrument.adept.JniAdeptDepp.*;
import cbsw.gui.GuiJPanel;
import java.text.Format;
import java.text.DecimalFormat;

public class JRichTBTrigger extends javax.swing.JFrame implements ActionListener
{
  Preferences rootPrefs;
  Preferences prefs;
  Preferences runprefs = Preferences.userRoot().node("RICHTB");

  private Timer  timer;
  private int ticks;
  private int sleepyTime = 200;
  private static JniAdeptDepp usb;
  //    private static JniAdeptDeppDummy usb;
  private static boolean isUsbOpen = false;
  static final int nCounters = 8;
  private int switches;
  private int gateMask;

  private CounterPanel [] counterPanel;

  private JRadioButton [] dbRB;
  private JRadioButton trgNoneRB, trgPulserRB, trgBeamRB, trgAuxRB, ledRB;

ActionListener usbOpenAL = new ActionListener () { public void actionPerformed(ActionEvent e)
{
// Open the device if not already open

  try
  {
      if ( !usb.OK() ) usb.close();
      if ( !usb.OK() ) usb.open();
      initTrigger();
  }
  catch ( DeppException de ){System.out.println("usbOpenAL(): Exception opening device");}

  //  if ( !isOK ) System.out.println("Waiting for "+deviceString);

  return;
}
};

  private class CounterPanel extends JPanel
  {
    private int id;
    private int count = 0;
    private int lastCount = 0;
    private double rate = 0.0;
    private int lastRateCount = 0;
    private long lastRateTime = 0;
    private JTextField countTF;
    private JTextField rateTF;

    public CounterPanel( int ctr, String bT, int fS, Color fC )
    {
      java.util.Random rd = new java.util.Random();
      int ri = rd.nextInt(1000000);

      id = ctr;

      setLayout(new java.awt.GridLayout(2, 1));
      setBorder( BorderFactory.createTitledBorder(bT));

      countTF = new JTextField(String.format("%1$010d",ri));
      countTF.setColumns(10);
      countTF.setBackground(Color.darkGray);
      countTF.setFont(new java.awt.Font("Monospaced", 1, fS));
      countTF.setForeground(fC);
      countTF.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
      countTF.setEditable(false);
      add(countTF);

      rateTF = new JTextField();
      rateTF.setColumns(10);
      rateTF.setBackground(Color.darkGray);
      rateTF.setFont(new java.awt.Font("Monospaced", 1, fS));
      rateTF.setForeground(fC);
      rateTF.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
      rateTF.setEditable(false);
      add(rateTF);

    }

    public void updateCount()
    {
      byte [] buf = new byte[4];
      byte [] addr = {(byte)0x0,(byte)(id*4),(byte)0x1,(byte)0x0};
      try
      {
        usb.put(addr);
      	usb.pop((byte)0x2,buf);
      	// To combine the 4 signed bytes into an int, convert via a hex string
      	String countString = String.format("%1$02x%2$02x%3$02x%4$02x",buf[3],buf[2],buf[1],buf[0]);
        if ((buf[3] & 0x80) == 0) {
          count = Integer.parseInt(countString,16);
      	  countTF.setText(String.format("%1$010d",count));
      	}
      }
      catch ( JniAdeptDepp.DeppException de )
      {
	  //	System.out.println("updateCount() exception");
      }
      return;
    }

    public void updateRate( Date tod )
    {
      long now = tod.getTime();
      rate = ((now - lastRateTime) == 0 ) ? 0.0 : 1000.0*(double)(count-lastRateCount)/(double)(now-lastRateTime);
      rateTF.setText(String.format("%1$7.2g Hz",rate)) ;
      lastRateCount = count;
      lastRateTime = now;
    }

    public int getCount() { return count; }

    public double getRate() { return rate; }

  }

  public void initTrigger()
  {
    if (prefs.getBoolean("trg_none",true)) setSwitchBits(0x0<<4,0x3<<4);
    if (prefs.getBoolean("trg_pulser",true)) setSwitchBits(0x1<<4,0x3<<4);
    if (prefs.getBoolean("trg_beam",true)) setSwitchBits(0x2<<4,0x3<<4);
    if (prefs.getBoolean("trg_aux",true)) setSwitchBits(0x3<<4,0x3<<4);
    if (prefs.getBoolean("led_en",true)) setSwitchBits(0x40,0x40);

    for (int db=0;db<8;db++)
    {
      int dbMask = 1<<(db+8);
      if (prefs.getBoolean("db"+db,false))
        { setSwitchBits(0x0,dbMask); }
      else
        { setSwitchBits(dbMask,dbMask); }
    }

    setDeadtime(prefs.getInt("deadtime",255));
    setPulseDelay(prefs.getInt("pulse_delay",100));

    return;
  }

  // Creates new top level form

  public JRichTBTrigger()
  {
    super("JRichTBTrigger");

    rootPrefs = Preferences.userNodeForPackage(JRichTBTrigger.class).node("JRichTBTrigger");
    prefs = Preferences.userNodeForPackage(JRichTBTrigger.class).node("JRichTBTrigger");

    Container cP = getContentPane();
    cP.setLayout(new BorderLayout());

    JMenuBar mB = new JMenuBar();
    setJMenuBar( mB );

    initComponents();

    pack();

    usb = new JniAdeptDepp(prefs.get("connectionName","Chimaera2"),usbOpenAL);
    usb.connect();

    timer = new Timer(sleepyTime, this );
    timer.setInitialDelay(0);
    timer.setCoalesce( false );

    timer.start();

  }

  public void updateStatus()
  {
    return;
  }

  // Send an entry to the logbook
  public void elog( String elogSubject, String elogText, Boolean htmlFlag )
  {
    String server = runprefs.get("logging.server","lblogbook.cern.ch");
    String user = runprefs.get("logging.user","guest");
    String pw = runprefs.get("logging.password","");
    String book = runprefs.get("logging.logbook-name","RICH+upgrade+testbeam");
    String sys = runprefs.get("logging.system-name","Rich");
    String author = runprefs.get("logging.author","RICH TB robot");
    String encoding = htmlFlag?"2":"1";
    String [] cmdArray = {"elog","-h",server,"-l",book,"-u",user,pw,"-x","-a","Subject="+elogSubject,"-a","System="+sys,"-a","Author="+author,"-n",encoding,elogText};
    try
    {
      if (runprefs.getBoolean("autolog",false))
      {
        Process pr = Runtime.getRuntime().exec(cmdArray);
	//      ostream = pr.getOutputStream();
	//      istream = pr.getInputStream();
      }
    }
    catch (IOException ioe){ ioe.printStackTrace(); }
  }

  public void actionPerformed( ActionEvent e )
  {
    //-- This deals with the periodic tasks

    String cmd = e.getActionCommand();
    Object ob = e.getSource();

    if ( cmd == null )
    {
      ticks++;
      Date tod = new Date();

      // Update count on every tick
      for (int CtrIdx=0;CtrIdx<JRichTBTrigger.nCounters;CtrIdx++)
      {
      	counterPanel[CtrIdx].updateCount();
      }
      updateStatus();

      // Update rate from time to time
      if ( (ticks%100) == 0 )
      {
      	for (int CtrIdx=0;CtrIdx<JRichTBTrigger.nCounters;CtrIdx++)
      	{
      	  counterPanel[CtrIdx].updateRate( tod );
      	}
      }
    }
    else if (cmd.equals("trgNone"))
    {
      prefs.putBoolean("trg_none",true);
      prefs.putBoolean("trg_pulser",false);
      prefs.putBoolean("trg_beam",false);
      prefs.putBoolean("trg_aux",false);
      setSwitchBits(0x0<<4,0x3<<4);
      //elog("Trigger source changed","Trigger source set to NONE",false);
      runprefs.put("RunControl.trigsource","None");
    }
    else if (cmd.equals("trgPulser"))
    {
      prefs.putBoolean("trg_none",false);
      prefs.putBoolean("trg_pulser",true);
      prefs.putBoolean("trg_beam",false);
      prefs.putBoolean("trg_aux",false);
      setSwitchBits(0x1<<4,0x3<<4);
      //elog("Trigger source changed","Trigger source set to PULSER",false);
      runprefs.put("RunControl.trigsource","Pulser");
    }
    else if (cmd.equals("trgBeam"))
    {
      prefs.putBoolean("trg_none",false);
      prefs.putBoolean("trg_pulser",false);
      prefs.putBoolean("trg_beam",true);
      prefs.putBoolean("trg_aux",false);
      setSwitchBits(0x2<<4,0x3<<4);
      //elog("Trigger source changed","Trigger source set to BEAM",false);
      runprefs.put("RunControl.trigsource","Beam");
    }
    else if (cmd.equals("trgAux"))
    {
      prefs.putBoolean("trg_none",false);
      prefs.putBoolean("trg_pulser",false);
      prefs.putBoolean("trg_beam",false);
      prefs.putBoolean("trg_aux",true);
      setSwitchBits(0x3<<4,0x3<<4);
      //elog("Trigger source changed","Trigger source set to Aux",false);
      runprefs.put("RunControl.trigsource","Aux");
    }
    else if (cmd.equals("ledEn"))
    {
      prefs.putBoolean("led_en",ledRB.isSelected());
      if ( ledRB.isSelected() )
      {
      	setSwitchBits(0x40,0x40);
      }
      else
      {
      	setSwitchBits(0x00,0x40);
      }
    }
    else if (cmd.startsWith("enDB"))
    {
      int db = Integer.parseInt(cmd.substring(4));
      if (db>=0 && db<8)
      {
        prefs.putBoolean("db"+db,dbRB[db].isSelected());
        int dbMask = 1<<(db+8);
        if ( dbRB[db].isSelected() )
        {
    	    setSwitchBits(0x0,dbMask);
        }
        else
        {
      	  setSwitchBits(dbMask,dbMask);
        }
      }
    }
    else if (cmd.equals("resetCounterAll"))
    {
      toggleButtonBits(0x1);
    }
    else if (cmd.equals("resetCounterPulser"))
    {
      toggleButtonBits(0x2);
    }
    else if (cmd.equals("resetCounterBeam"))
    {
      toggleButtonBits(0x4);
    }
    else if (cmd.equals("logCounters"))
    {
      StringBuffer html = new StringBuffer("<table><tr><th>Counter</th><th>Count</th></tr>");
      for ( int i=0; i<JRichTBTrigger.nCounters; i++ )
      {
      	int count = counterPanel[i].getCount();
      	String counterLabel = prefs.get("counter"+i+"_label","Counter A");
      	html.append("<tr><td>"+counterLabel+"</td><td>"+count+"</td></tr>");
      }
      html.append("</table>");
      elog("Trigger counters",html.toString(),true);
    }
    else if (cmd.equals("triggerDeadtime"))
    {
      setDeadtime(Integer.parseInt(((JTextField)ob).getText(),10));
    }
    else if (cmd.equals("pulseDelay"))
    {
      setPulseDelay(Integer.parseInt(((JTextField)ob).getText(),10));
    }
   return;
  }

  public int getCount( int i )
  {
    return counterPanel[i].getCount();
  }

  private void setDeadtime(int deadtime)
  {
    if (deadtime<1) deadtime = 1;
    if (deadtime>0x1ff) deadtime = 0x1ff;
    prefs.putInt("deadtime",deadtime);
    byte [] addr = {(byte)0x0,(byte)0x6,(byte)0x1,(byte)0x0};
    byte [] buf = new byte [2];
    buf[0] = (byte)(deadtime & 0xff);
    buf[1] = (byte)((deadtime>>8) & 0xff);
    try
    {
      usb.put(addr);
      usb.push((byte)0x3,buf);
    }
    catch ( JniAdeptDepp.DeppException de )
    { System.out.println("deadtime exception: deadtime="+deadtime); }
    return;
  }

  private void setPulseDelay(int pulseDelay)
  {
    if (pulseDelay<10) pulseDelay = 10;
    if (pulseDelay>0xffff) pulseDelay = 0xffff;
    prefs.putInt("pulse_delay",pulseDelay);
    byte [] addr = {(byte)0x0,(byte)0x8,(byte)0x1,(byte)0x0};
    byte [] buf = new byte [2];
    buf[0] = (byte)(pulseDelay & 0xff);
    buf[1] = (byte)((pulseDelay>>8) & 0xff);
    try
    {
      usb.put(addr);
      usb.push((byte)0x3,buf);
    }
    catch ( JniAdeptDepp.DeppException de )
    { System.out.println("pulseDelay exception: delay="+pulseDelay); }
    return;
  }

  private void setSwitchBits(int bits, int mask)
  {
    switches &= ~mask; // Force bits off
    switches |= bits; // Update the bits
    byte [] addr = {(byte)0x0,(byte)0x0,(byte)0x1,(byte)0x0};
    byte [] buf = new byte [2];
    buf[0] = (byte)(switches & 0xff);
    buf[1] = (byte)((switches>>8) & 0xff);
    try
    {
      usb.put(addr);
      usb.push((byte)0x3,buf);
    }
    catch ( JniAdeptDepp.DeppException de )
    { System.out.println("setSwitchBits() exception: bits="+bits+" mask="+mask); }
    //catch ( java.lang.NullPointerException npe){}
  }

  public void resetCounts()
  {
    toggleButtonBits(0x1);
  }

  private void toggleButtonBits(int bits)
  {
    try
    {
      byte [] addr = {(byte)0x0,(byte)0x2,(byte)0x1,(byte)0x0};
      byte [] buf = new byte [2];
      buf[0] = (byte)(bits & 0xff);
      buf[1] = (byte)((bits>>8) & 0xff);
      usb.put(addr);
      usb.push((byte)0x3,buf);
      buf[0] = 0; buf[1] = 0; // Unset the bits
      usb.put(addr);
      usb.push((byte)0x3,buf);
    }
    catch ( JniAdeptDepp.DeppException de )
    { System.out.println("toggleButtonBits() exception"); }
  }

  // This method is called from within the constructor to initialize the form.

  private void initComponents()
  {
    counterPanel = new CounterPanel[JRichTBTrigger.nCounters];

    counterPanel[0] = new CounterPanel(0, prefs.get("counter0_label","Counter A"),32,Color.red);
    counterPanel[1] = new CounterPanel(1, prefs.get("counter1_label","Counter B"),32,Color.red);
    counterPanel[2] = new CounterPanel(2, prefs.get("counter2_label","Counter C"),32,Color.red);
    counterPanel[3] = new CounterPanel(3, prefs.get("counter3_label","Counter D"),32,Color.red);
    counterPanel[4] = new CounterPanel(4, prefs.get("counter4_label","Counter E"),32,Color.red);
    counterPanel[5] = new CounterPanel(5, prefs.get("counter5_label","Counter F"),32,Color.red);
    counterPanel[6] = new CounterPanel(6, prefs.get("counter6_label","Counter G"),32,Color.red);
    counterPanel[7] = new CounterPanel(7, prefs.get("counter7_label","Counter H"),32,Color.red);

    Container cP = getContentPane();

    JPanel mainPanel = new JPanel();

    GridBagLayout gB = new GridBagLayout();
    mainPanel.setLayout( gB );
    GridBagConstraints gbC = new GridBagConstraints();

    gbC.fill = GridBagConstraints.NONE;
    gbC.anchor = GridBagConstraints.CENTER;
    gbC.insets = new Insets(0,0,0,0);
    gbC.weightx = 1.0;
    gbC.weighty = 1.0;

    addWindowListener(new java.awt.event.WindowAdapter() {
    	public void windowClosing(java.awt.event.WindowEvent evt) {
    	  exitForm(evt);
    	}
    });

    gbC.gridwidth = 1;
    mainPanel.add(counterPanel[0], gbC);

    gbC.gridwidth = GridBagConstraints.REMAINDER;
    mainPanel.add(counterPanel[1], gbC);

    gbC.gridwidth = 1;
    mainPanel.add(counterPanel[2], gbC);

    gbC.gridwidth = GridBagConstraints.REMAINDER;
    mainPanel.add(counterPanel[3], gbC);

    gbC.gridwidth = 1;
    mainPanel.add(counterPanel[4],gbC);

    gbC.gridwidth = GridBagConstraints.REMAINDER;
    mainPanel.add(counterPanel[5],gbC);

    gbC.gridwidth = 1;
    mainPanel.add(counterPanel[6],gbC);

    gbC.gridwidth = GridBagConstraints.REMAINDER;
    mainPanel.add(counterPanel[7],gbC);

    JButton logButton = new JButton("Log counters");
    logButton.setActionCommand("logCounters");
    logButton.addActionListener(this);
    gbC.gridwidth = GridBagConstraints.REMAINDER;
    mainPanel.add(logButton, gbC);

    JPanel counterResetsJP = new JPanel();
    counterResetsJP.setBorder( BorderFactory.createTitledBorder("Counter resets") );
    JButton button = new JButton("All");
    button.setActionCommand("resetCounterAll");
    button.addActionListener(this);
    counterResetsJP.add(button);
    button = new JButton("Pulser");
    button.setActionCommand("resetCounterPulser");
    button.addActionListener(this);
    counterResetsJP.add(button);
    button = new JButton("Beam");
    button.setActionCommand("resetCounterBeam");
    button.addActionListener(this);
    counterResetsJP.add(button);

    gbC.gridwidth = GridBagConstraints.REMAINDER;
    mainPanel.add(counterResetsJP,gbC);

    JPanel triggerSourceJP = new JPanel();
    triggerSourceJP.setBorder( BorderFactory.createTitledBorder("Trigger source") );
    ButtonGroup triggerBG = new ButtonGroup();
    trgNoneRB = new JRadioButton("None");
    trgNoneRB.setSelected(prefs.getBoolean("trg_none",true));
    trgNoneRB.setActionCommand("trgNone");
    trgNoneRB.addActionListener(this);
    triggerBG.add(trgNoneRB);
    triggerSourceJP.add(trgNoneRB);
    trgPulserRB = new JRadioButton("Pulser");
    trgPulserRB.setSelected(prefs.getBoolean("trg_pulser",false));
    trgPulserRB.setActionCommand("trgPulser");
    trgPulserRB.addActionListener(this);
    triggerBG.add(trgPulserRB);
    triggerSourceJP.add(trgPulserRB);
    trgBeamRB = new JRadioButton("Beam");
    trgBeamRB.setSelected(prefs.getBoolean("trg_beam",false));
    trgBeamRB.setActionCommand("trgBeam");
    trgBeamRB.addActionListener(this);
    triggerBG.add(trgBeamRB);
    triggerSourceJP.add(trgBeamRB);
    trgAuxRB = new JRadioButton("LEMO top");
    trgAuxRB.setSelected(prefs.getBoolean("trg_aux",false));
    trgAuxRB.setActionCommand("trgAux");
    trgAuxRB.addActionListener(this);
    triggerBG.add(trgAuxRB);
    triggerSourceJP.add(trgAuxRB);

    ledRB = new JRadioButton("LED");
    ledRB.setSelected(prefs.getBoolean("led_en",false));
    ledRB.setActionCommand("ledEn");
    ledRB.addActionListener(this);
    triggerSourceJP.add(ledRB);

    gbC.gridwidth = GridBagConstraints.REMAINDER;
    mainPanel.add(triggerSourceJP,gbC);

    JPanel gateMaskJP = new JPanel();
    gateMaskJP.setBorder( BorderFactory.createTitledBorder(prefs.get("connected-boards-border-label","Connected DBs")) );

    dbRB = new JRadioButton[8];
    for (int i=0;i<8;i++)
    {
      dbRB[i] = new JRadioButton(prefs.get("connected-board-label-"+i,"DB "+i));
      dbRB[i].setSelected(prefs.getBoolean("db"+i,true));
      dbRB[i].setActionCommand(String.format("enDB%1$d",i));
      dbRB[i].addActionListener(this);
      gateMaskJP.add(dbRB[i]);
    }

    gbC.gridwidth = GridBagConstraints.REMAINDER;
    mainPanel.add(gateMaskJP,gbC);

// Deadtime
    gbC.gridwidth = 1;
    JTextField tf = makeTF(new DecimalFormat("#"),"deadtime", "255", 10, "Deadtime", "Trigger deadtime",false);
    tf.setActionCommand("triggerDeadtime");
    tf.addActionListener(this);
    mainPanel.add( tf.getParent(),gbC );

    gbC.gridwidth = GridBagConstraints.REMAINDER;
    tf = makeTF(new DecimalFormat("#"),"pulse_delay", "100", 10, "Pulse delay", "Delay between pulses",false);
    tf.setActionCommand("pulseDelay");
    tf.addActionListener(this);
    mainPanel.add( tf.getParent(),gbC );

    cP.add( mainPanel, BorderLayout.NORTH );

  }

  public JTextField makeTF(Format fmt, String name, String prefsDefault, int width, String borderLabel, String toolTip, boolean registerPrefsAction)
  {
    JFormattedTextField tf = new JFormattedTextField(fmt);
    tf.setColumns(width);
    ((JTextField)tf).setText( prefs.get( name,prefsDefault ) );
    tf.setName(name);
    if (registerPrefsAction)
    {
      tf.addActionListener( new ActionListener () { public void actionPerformed(ActionEvent e) { JTextField tf = (JTextField)e.getSource(); prefs.put(tf.getName(),tf.getText()); return; } } );
    }
    tf.setToolTipText( toolTip );
    if ( borderLabel != null )
    {
      JPanel jp = new JPanel();
      jp.setBorder( BorderFactory.createTitledBorder(borderLabel) );
      jp.add( tf );
    }

    return tf;
  }

  private void exitForm(java.awt.event.WindowEvent evt)
  {
      usb.close();
      System.exit(0);
  }

  public static void main(String args[])
  {
    //Schedule a job for the event-dispatching thread:
    //creating and showing this application's GUI.
    javax.swing.SwingUtilities.invokeLater(new Runnable() {
    	public void run() {
          JRichTBTrigger MainFrame = new JRichTBTrigger() ;
          MainFrame.setVisible(true);
    	}
    });
  }
 }

