/*
RICH EC Control Panel
Steve Wotton, 2000-2015
Jackson Smith
Cavendish Lab (HEP)
*/

package cbsw.lhcb;

import java.io.*;
import java.nio.*;
import java.nio.channels.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.util.prefs.*;
import cbsw.lhcb.*;
import java.util.*;
import java.lang.Math;
import java.lang.Object;
import cbsw.gui.GuiJPanel;
import cbsw.gui.GuiDaqControl;
import javax.swing.filechooser.*;
import java.text.DecimalFormat;

public class JRichEcControl extends GuiJPanel implements ActionListener {

//=============================================================
//             D A T A   M E M B E R S
//=============================================================
static final int bufSendSize = 388;
static final int proxyMsgSize = 1024;
ByteBuffer proxyMsg;
int proxyMsgSeq=0;
boolean logEnd=false;

OutputStream ostream;
WritableByteChannel ochannel;
ReadableByteChannel ichannel;

ScanThread dth;
boolean isThresholdScan = true;

JTextField tagTF;
JTextField runNumberTF;

JniMAPMTFEB map;
JniMAROC3 mar; // still partly owned by this class
JniCLARO cla; // ditto

Map< String, Preferences > prefsDBMap;
Map< String, Integer > dbSideMap; // Which side of the EC
Map< String, Preferences > prefsPCMap;
Map< String, Preferences > prefsFEMap;

// Trigger panel
JRichTBTrigger trig;

JFrame scanFrame;
JFrame dbPulserFrame;
JFrame daqFrame;
GuiDaqControl daqCP;

JFrame configuratorFrame;

Preferences countprefs = Preferences.userNodeForPackage(JRichTBTrigger.class).node("JRichTBTrigger");

Preferences runprefs = Preferences.userRoot().node("RICHTB");

ActionListener exitAL = new ActionListener () { public void actionPerformed(ActionEvent e)
{
   System.exit(0);
   return;
}
};

ActionListener timestampAL = new ActionListener () { public void actionPerformed(ActionEvent e)
{
  try { Preferences.userRoot().node("RICH").sync(); } catch (BackingStoreException ex) {ex.printStackTrace();}
   System.out.println(Preferences.userRoot().node("RICH").get("timestamp","-"));
   return;
}
};

ActionListener configuratorAL = new ActionListener () { public void actionPerformed(ActionEvent e)
{
   configuratorFrame.setVisible(true);
   return;
}
};

ActionListener febTypeAL = new ActionListener () { public void actionPerformed(ActionEvent e)
{
  JRadioButtonMenuItem rb = (JRadioButtonMenuItem)e.getSource();
  if ( rb.getText().equals("CLARO") )
  {
    prefs.put("runtime.feb-type","CLARO");
  }
  else if ( rb.getText().equals("MAROC3") )
  {
    prefs.put("runtime.feb-type","MAROC3");
  }
  return;
}
};

ActionListener abstractButtonAL = new ActionListener () { public void actionPerformed(ActionEvent e)
{
  AbstractButton ab = (AbstractButton)e.getSource();
  prefs.putBoolean(ab.getName(),ab.isSelected());

  return;
}
};

ActionListener pbwFifoPathAL = new ActionListener () { public void actionPerformed(ActionEvent e)
{
    String path = JOptionPane.showInputDialog("Please enter FIFO path",prefs.get("proxy.pbw.fifo","/dev/shm/pbwrly"));
  try
  {
// new File doesn't create FIFO, just allows to check for a valid path.
    File fi = new File(path);
    if ( !fi.isAbsolute() ) return;
    prefs.put("proxy.pbw.fifo",path);
  }
  catch (NullPointerException npx) {} // Fail silently (path can be null)
  return;
}
};

ActionListener importAL = new ActionListener () { public void actionPerformed(ActionEvent e)
{

  JFileChooser chooser = new JFileChooser();
  FileNameExtensionFilter filter = new FileNameExtensionFilter( "Preferences (XML)", "xml" );
  chooser.setFileFilter(filter);
  int returnVal = chooser.showOpenDialog(null);
  if(returnVal == JFileChooser.APPROVE_OPTION)
  {
// Import preferences in XML file
    try
    {
      FileInputStream prefsStream = new FileInputStream(chooser.getSelectedFile());
      Preferences.userRoot().importPreferences(prefsStream);
      prefsStream.close();
    }
    catch (IOException ex) { ex.printStackTrace(); }
    catch (InvalidPreferencesFormatException ex) { ex.printStackTrace(); }
  }

   return;
}
};

ActionListener expertDAQAL = new ActionListener () { public void actionPerformed(ActionEvent e)
{
   daqFrame.setVisible(true);
   return;
}
};

ActionListener dbPulserAL = new ActionListener () { public void actionPerformed(ActionEvent e)
{
   dbPulserFrame.setVisible(true);
   return;
}
};

ActionListener scanAL = new ActionListener () { public void actionPerformed(ActionEvent e)
{
   scanFrame.setVisible(true);
   return;
}
};

public class ScanControl extends GuiJPanel implements ActionListener
{

  public ScanControl()
  {
    super(Preferences.userNodeForPackage(JRichEcControl.class).node("JRichEcControl"),"scan.");

    setLayout( new GridBagLayout() );
    GridBagConstraints gbC = new GridBagConstraints();

    gbC.insets = new Insets(2,2,2,2);
    gbC.weightx = 1.0f;
    gbC.weighty = 1.0f;

    gbC.fill = GridBagConstraints.NONE;
    gbC.anchor = GridBagConstraints.CENTER;
    gbC.gridwidth = 1;

    gbC.gridwidth = GridBagConstraints.REMAINDER;
    add( makeTF(new DecimalFormat("#"),"maxEvents", "100", 20, "Max events per step", "Maximum number of events to store per step",true).getParent(),gbC );
    gbC.gridwidth = 1;
    add( makeTF(new DecimalFormat("#"),"step", "2", 10, "Increment", "Threshold (DAC) increment size [1,63] ([1,1023])",true).getParent(),gbC );
    add( makeTF(new DecimalFormat("#"),"begin", "0", 10, "Begin value", "Threshold (DAC) lower limit [0,63] ([0,1023])",true).getParent(),gbC );
    gbC.gridwidth = GridBagConstraints.REMAINDER;
    add( makeTF(new DecimalFormat("#"),"end", "63", 10, "End value", "Threshold (DAC) upper limit [0,63] ([0,1023])",true).getParent(),gbC );

    gbC.gridwidth = 1;
    JButton button;
    button = new JButton( "Threshold scan" );
    button.addActionListener( this );
    button.setActionCommand( "threshScan" );
    add( button, gbC );

    button = new JButton( "Test pulse scan" );
    button.addActionListener( this );
    button.setActionCommand( "DACscan" );
    add( button, gbC );

    button = new JButton( "Interrupt" );
    button.setToolTipText("Use this button to interrupt a stuck or running scan");
    button.addActionListener( this );
    button.setActionCommand( "stopScan" );
    gbC.gridwidth = GridBagConstraints.REMAINDER;
    add( button, gbC );
  }

// Handle panel changes
// Text fields handled by listener in superclass.

  public void actionPerformed( ActionEvent e )
  {

    String cmd = e.getActionCommand();

// Bail out if new scan requested and scan is still active
    if(dth.isAlive() && !cmd.equals("stopScan"))
    {
      System.out.println("Request for new scan ignored while scan is active");
      return;
    }

    if ( cmd.equals( "threshScan" ) )
    {
      makeMaps();
      savePreferences(prefs,this);
      isThresholdScan = true;
      dth = new ScanThread();
      dth.start();
    }
    else if ( cmd.equals( "DACscan" ) )
    {
      makeMaps();
      savePreferences(prefs,this);
      isThresholdScan = false;
      dth = new ScanThread();
      dth.start();
    }
    else if ( cmd.equals( "stopScan" ) )
    {
      dth.interrupt();
      int waitCount=3;
      System.out.println("Waiting 6s (max) for scan to stop...");
      while(dth.isAlive()&&(waitCount--)>0)
      {
        try { Thread.sleep(2000); } catch(InterruptedException iex){}
      }
      System.out.println("Scan is finished");
    }
    return;
  }

}

public class ScanThread extends Thread
{
  public void run()
  {
    int step = prefs.getInt( "scan.step" , 2 );
    int lowerLimit = prefs.getInt( "scan.begin" , 0 );
    int upperLimit = prefs.getInt( "scan.end" , 63 );
    int maxEvents = prefs.getInt( "scan.maxEvents" , 100 );
    String source = runprefs.get("RunControl.trigsource","unknown");

    setupProxy();
    int runNumber = newRun();

    // add entry to logbook
    String logsubject = (isThresholdScan?"Threshold":"DAC")+" scan: Run "+runNumber;
    String logentry = "increment = "+step+"; start value = "+lowerLimit+"; end value = "+upperLimit+"; trigger = "+source+"; max events = "+maxEvents;
    logStart(logsubject,logentry);

    if (prefs.getBoolean("runtime.triggerPanel",false)) trig.resetCounts();

    for( int currentVal=lowerLimit; Math.abs(currentVal-lowerLimit) <= Math.abs(upperLimit-lowerLimit) && !isInterrupted(); currentVal+=step ) {
    // Loop over the boards to start the run
      Set<String> keys = prefsDBMap.keySet();
      Iterator<String> it = keys.iterator();
      while (it.hasNext()) {
        String key = it.next();
        map.setStateFromPreferences(prefsDBMap.get(key),prefsPCMap.get(key),prefs);
        String febType = prefs.get("runtime.feb-type","CLARO");
        if ( febType.equals("CLARO") ) {
          cla.setStateFromPreferences(prefsFEMap.get(key),dbSideMap.get(key).intValue());
        } else if ( febType.equals("MAROC3") ) {
          mar.setStateFromPreferences(prefsFEMap.get(key),dbSideMap.get(key).intValue());
        }

        map.setDaqReset(false);
        map.setDummyIngressReset(true);
        map.setGate(true);
        map.setMepTfcControl( false );
        map.setMepFixedDestination( true );
        map.setTfcDecoderEnable( true );
        map.setGbePollEnable( true );

        if ( isThresholdScan ) {
          // set maroc registers
          for(int j=0; j<2; j++) {
            mar.set_dac( 0, currentVal, j );
          }
          // set claro registers
          for(int j=0; j<cla.getNregisters(); j++) {
            for(int ch=0; ch<8; ch++) {
              cla.set_threshold( ch, currentVal, j );
            }
          }
        } else {
          map.setDACval(currentVal);
        }

        map.setTag( currentVal );
      	// Only one board (the last) should wait for maxEvents
        if (it.hasNext()) {
          sendConfiguration(key,0,false);
        } else {
          sendConfiguration(key,maxEvents,true);
        }
      }

      tagTF.setText(String.format("%1$d",currentVal));
    }


    endRun();

    System.out.println("Scan is stopping");

    if (prefs.getBoolean("runtime.triggerPanel",false)) {
      StringBuffer html = makeCounterTable();
      logStop( html );
    }

  }
}

JPanel makeRunJP()
{
// Run controls

  JPanel runJP = new JPanel();
  runJP.setBorder( BorderFactory.createTitledBorder("Run control") );
  runJP.setLayout( new GridBagLayout() );
  GridBagConstraints gbC = new GridBagConstraints();

// Start adding the components

  gbC.fill = GridBagConstraints.VERTICAL;
  gbC.anchor = GridBagConstraints.CENTER;
  gbC.insets = new Insets(2,2,2,2);
  gbC.weightx = 1.0f;
  gbC.weighty = 1.0f;

  tagTF = makeTF(new DecimalFormat("#"),"tag","0",10,"Tag","Current tag value",false);
  tagTF.setEditable(false);
  runJP.add(tagTF.getParent(),gbC);

  runJP.add( makeTF("saveFileDestination", "/data/", 25, "Folder", "Folder for saved data and preferences",false).getParent(),gbC );

  runNumberTF = makeTF(new DecimalFormat("#"),"runNumber","1",6,"Run","Run number (auto-increment)",false);
  gbC.gridwidth = GridBagConstraints.REMAINDER;

  runJP.add(runNumberTF.getParent(),gbC);

  JButton button = new JButton( "Start" );
  button.addActionListener( this );
  button.setActionCommand( "start" );
  gbC.gridwidth = 1;
  runJP.add( button, gbC );

  button = new JButton( "Stop" );
  button.addActionListener( this );
  button.setActionCommand( "stop" );
  gbC.gridwidth = 1;
  runJP.add( button, gbC );

  gbC.gridwidth = 1;
  runJP.add( makeCheckBox("recording","Recording",false,false), gbC );

  return (runJP);
}

// Send an entry to the logbook
public void elog( String elogSubject, String elogText, Boolean htmlFlag )
{
  Preferences pfs = Preferences.userRoot().node("RICHTB");
  String server = pfs.get("logging.server","lblogbook.cern.ch");
  String user = pfs.get("logging.user","guest");
  String pw = pfs.get("logging.password","");
  String book = pfs.get("logging.logbook-name","RICH+upgrade+testbeam");
  String sys = pfs.get("logging.system-name","Rich");
  String author = pfs.get("logging.author","TBTrigger");
  String encoding = htmlFlag?"2":"1";
  String [] cmdArray = {"elog","-h",server,"-l",book,"-u",user,pw,"-x","-a","Subject="+elogSubject,"-a","System="+sys,"-a","Author="+author,"-n",encoding,elogText};
  try
  {
    if (pfs.getBoolean("autolog",false))
    {
      Process pr = Runtime.getRuntime().exec(cmdArray);
    }
  }
  catch (IOException ioe){ ioe.printStackTrace(); }
}

public JFrame makeConfiguratorFrame()
{
  JFrame mf = new JFrame("JRichEcConfigurator");

  Container cP = mf.getContentPane();
  cP.setLayout(new BorderLayout());

  JMenuBar mB = new JMenuBar();
  mf.setJMenuBar( mB );

  JRichEcConfigurator ec = new JRichEcConfigurator( mB );
  cP.add( ec, BorderLayout.NORTH );

  mf.pack();

  return mf;
}

// Make some Maps containing the Preferences branches
// Should be called before run start to reflect changes made by JRichEcConfigurator
// Readout is DB-centric so Maps use DB DNA as keys.

public void makeMaps()
{
  Preferences ecPrefs = Preferences.userRoot().node("RICH/EC");
  prefsDBMap = new HashMap<String,Preferences>();
  prefsPCMap = new HashMap<String,Preferences>();
  prefsFEMap = new HashMap<String,Preferences>();
  dbSideMap = new HashMap<String,Integer>();
  try
  {
    for ( String ecName : ecPrefs.childrenNames() )
    {
      String fePrefsName = ecPrefs.node(ecName).get("preferences","default");
      String dna = ecPrefs.node(ecName).get("db0.dna","---- ----");
      if (!dna.equals("---- ----"))
      {
	prefsDBMap.put(dna, Preferences.userRoot().node("RICH/DB").node(dna));
	prefsPCMap.put(dna, Preferences.userRoot().node("RICH/PC"));
	prefsFEMap.put(dna, Preferences.userRoot().node("RICH/preferences").node(fePrefsName));
        dbSideMap.put(dna, 0);
      }
      dna = ecPrefs.node(ecName).get("db1.dna","---- ----");
      if (!dna.equals("---- ----"))
      {
        prefsDBMap.put(dna, Preferences.userRoot().node("RICH/DB").node(dna));
	prefsPCMap.put(dna, Preferences.userRoot().node("RICH/PC"));
	prefsFEMap.put(dna, Preferences.userRoot().node("RICH/preferences").node(fePrefsName));
        dbSideMap.put(dna, 1);
      }

    }
  } catch ( Exception e ) {}

  return;

}

public JRichEcControl( JMenuBar menuBar )
{

  super(Preferences.userNodeForPackage(JRichEcControl.class).node("JRichEcControl"),"runtime.");
// The JNI interface

  map = new JniMAPMTFEB();
  mar = new JniMAROC3();
  cla = new JniCLARO();

  // Proxy message buffer

  proxyMsg = ByteBuffer.allocate(proxyMsgSize);
  proxyMsg.order(ByteOrder.LITTLE_ENDIAN);

  makeMaps();

// Launch the proxy

  launchProxy();

  // Create scan thread
  dth = new ScanThread();

// The trigger panel
  if (prefs.getBoolean("runtime.triggerPanel",false))
  {
    trig = new JRichTBTrigger();
    trig.setVisible(true);
  }

  configuratorFrame = makeConfiguratorFrame();

  JMenu menuBarMenu = new JMenu("Main");

  JMenuItem menuItem = new JMenuItem("Import...");
  menuItem.addActionListener( importAL );
  menuBarMenu.add( menuItem );

  menuBarMenu.addSeparator();

//   menuItem = new JMenuItem("Show timestamp");
//   menuItem.addActionListener( timestampAL );
//   menuBarMenu.add(menuItem);

  menuItem = new JMenuItem("Configurator");
  menuItem.addActionListener( configuratorAL );
  menuBarMenu.add(menuItem);

  menuItem = new JMenuItem("Exit");
  menuItem.addActionListener( exitAL );
  menuBarMenu.add(menuItem);

  menuBar.add(menuBarMenu);

  daqFrame = new JFrame( "DAQ expert settings" );
  daqCP = new GuiDaqControl(Preferences.userRoot().node("RICH/PC"), daqFrame);
  daqFrame.getContentPane().add(daqCP);
  daqFrame.pack();

  menuBarMenu = new JMenu("Settings");

  menuItem = new JMenuItem("Expert DAQ...");
  menuItem.addActionListener( expertDAQAL );
  menuBarMenu.add( menuItem );

  dbPulserFrame = new JFrame( "DB pulser settings" );
  Container dbPulserCP = dbPulserFrame.getContentPane();
// Pulse Parameters
  JPanel dbPulserJP = new JPanel();
  dbPulserJP.add( makeTF(new DecimalFormat("#"),"pulseCount", "100", 10, "Pulse count", "Number of pulses, stored as 4 bytes(!)",true).getParent());
  dbPulserJP.add( makeTF(new DecimalFormat("#"),"pulseDelay", "100", 10, "Pulse delay", "Delay between pulses",true).getParent());
  dbPulserCP.add(dbPulserJP);
  dbPulserFrame.pack();

  menuItem = new JMenuItem("DB pulser...");
  menuItem.addActionListener( dbPulserAL );
  menuBarMenu.add( menuItem );

// Threshold scan
  scanFrame = new JFrame( "Threshold/DAC scans" );
  Container scanCP = scanFrame.getContentPane();
  JPanel jp = new JPanel();
  jp.add(new ScanControl());
  scanCP.add(jp);
  scanFrame.pack();

  menuItem = new JMenuItem("Threshold/DAC scan...");
  menuItem.addActionListener( scanAL );
  menuBarMenu.add( menuItem );

  JMenu menu = new JMenu("FEB type");
  boolean selected = prefs.get("runtime.feb-type","CLARO").equals("CLARO");
  JRadioButtonMenuItem rbMenuItem = new JRadioButtonMenuItem("CLARO",selected);
  rbMenuItem.addActionListener( febTypeAL );
  menu.add(rbMenuItem);
  selected = prefs.get("runtime.feb-type","CLARO").equals("MAROC3");
  rbMenuItem = new JRadioButtonMenuItem("MAROC3",selected);
  rbMenuItem.addActionListener( febTypeAL );
  menu.add(rbMenuItem);
  menuBarMenu.add(menu);

  menu = new JMenu("PBW relay");
  rbMenuItem = new JRadioButtonMenuItem("Enable",prefs.getBoolean("proxy.pbw.enable",false));
  rbMenuItem.setName("proxy.pbw.enable");
  rbMenuItem.addActionListener( abstractButtonAL );
  menu.add(rbMenuItem);

  menuItem = new JMenuItem("FIFO path...");
  menuItem.addActionListener( pbwFifoPathAL );
  menu.add( menuItem );

  menuBarMenu.add(menu);

  menu = new JMenu("Trigger panel");
  rbMenuItem = new JRadioButtonMenuItem("Enable",prefs.getBoolean("runtime.triggerPanel",false));
  rbMenuItem.setName("runtime.triggerPanel");
  rbMenuItem.addActionListener( abstractButtonAL );
  menu.add(rbMenuItem);

  menuBarMenu.add(menu);

  menuBar.add(menuBarMenu);

// Set layout style

  setLayout( new GridBagLayout() );
  GridBagConstraints gbC = new GridBagConstraints();
  gbC.fill = GridBagConstraints.VERTICAL;
  gbC.anchor = GridBagConstraints.CENTER;
  gbC.insets = new Insets(2,2,2,2);
  gbC.weightx = 1.0f;
  gbC.weighty = 1.0f;
  gbC.fill = GridBagConstraints.BOTH;

// Start adding the components

  add( makeTF(new DecimalFormat("#"),"latency", "16", 10, "Latency", "Latency, 1=25ns",false).getParent(),gbC );
  add( makeTF(new DecimalFormat("#"),"triggerDelay", "16", 10, "Trigger delay", "Trigger delay, 1=25ns",false).getParent(),gbC );
  gbC.gridwidth = GridBagConstraints.REMAINDER;
  add( makeTF(new DecimalFormat("#"),"strobeLength", "7", 10, "Strobe length", "Strobe length, 1 to 7",false).getParent(),gbC );
  gbC.gridwidth = 1;

  // new row for run control
  gbC.gridwidth = GridBagConstraints.REMAINDER;
  add(makeRunJP(), gbC);

}

//===========================================================================
//                      M E M B E R   F U N C T I O N S
//===========================================================================

// Handle changes to panel
// Most text fields are handled by a catch-all clause here.

public void actionPerformed( ActionEvent e )
{
   String cmd = e.getActionCommand();

   // If a scan thread is still active, bail out.
   if (dth.isAlive())
   {
     System.out.println("A scan still appears to be active. No action taken.");
     return;
   }

   if ( cmd.equals( "start" ) )
   {
     makeMaps();
     savePreferences(prefs,this);
     setupProxy();
     if (prefs.getBoolean("runtime.recording",false))
     {
       String msg = JOptionPane.showInputDialog("Please enter a comment");
       if (msg != null)
       {
         if ( !msg.equals("") ) StartRun(msg);
       }
     }
     else
     {
       StartRun(null);
     }
   }
   else if ( cmd.equals( "stop" ) )
   {
     makeMaps();
     savePreferences(prefs,this);
     setupProxy();
     endRun();
     if (prefs.getBoolean("runtime.triggerPanel",false))
     {
       StringBuffer html = makeCounterTable();
       logStop( html );
     }
   }

   return;
}

// Proxy always sends a response after a message.  An additional
// synchronisation response can be sent by the data reader but the
// read of this could be interrupted so we need to handle the
// possibility that an old response is still waiting to be read before
// the intended one. Therefore check the message sequence and re-read
// the response if needed.

void readProxyResponse()
{
  int seq=0;
  try
  {
    do
    {
      proxyMsg.clear();
      while (proxyMsg.position() != 4 ) { ichannel.read(proxyMsg); }
      proxyMsg.flip(); seq = proxyMsg.getInt();
    }
    while (seq < proxyMsgSeq );
  }
  catch (ClosedByInterruptException iex) { return; }
  catch (IOException ioe) { System.out.println("readProxyResponse(): IOException at seq="+proxyMsgSeq); System.out.println(ioe.getMessage()); }

  return;
}

// Send message header. Dataless messages should set waitResponse=true
void sendHeader(short len, short type, int info, boolean waitResponse)
{
  try
  {
    proxyMsgSeq++;
    // Build the header
    proxyMsg.clear();
    proxyMsg.putShort(len);
    proxyMsg.putShort(type);
    proxyMsg.putInt(info);
    proxyMsg.putInt(proxyMsgSeq);
    int size = proxyMsg.position();
    // Write it through a Channel
    proxyMsg.flip();
    ochannel.write(proxyMsg);
    ostream.flush();

    // Get (and check) the response if requested
    if (waitResponse) readProxyResponse();
  }
  catch (IOException ioe) { System.out.println("sendHeader(): IOException at seq="+proxyMsgSeq); System.out.println(ioe.getMessage()); }

  return;
}

// Send configuration packet
public void sendConfiguration( String dna, int maxEvents, boolean wait )
{
  byte [] bufSend = new byte [bufSendSize];

// Header
  sendHeader((short)bufSend.length,(short)1,maxEvents,false);

// DNA
  bufSend[3] = (byte)Integer.parseInt(dna.substring(0,2),16);
  bufSend[2] = (byte)Integer.parseInt(dna.substring(2,4),16);
  bufSend[1] = (byte)Integer.parseInt(dna.substring(5,7),16);
  bufSend[0] = (byte)Integer.parseInt(dna.substring(7,9),16);

// Fill with DB/BB part. 68 bytes db + 4 bytes bb, offset 4 bytes for dna.
  map.getRegisterArray(bufSend,72,4);

// Either MAROC3 or CLARO may be used
  String febType = prefs.get("runtime.feb-type","CLARO");
  if (febType.equals("CLARO"))
  {
    for(int i=0; i<16; i++) cla.getRegisterArray(i,bufSend,16,128+4+16*i);
  }
  else if (febType.equals("MAROC3"))
  {
    for(int i=0; i<2; i++) mar.getRegisterArray(i,bufSend,104,128+4+128*i);
  }

  try
  {
    proxyMsg.clear();
    proxyMsg.put(bufSend);
    proxyMsg.flip();
    ochannel.write(proxyMsg);
    ostream.flush();
    readProxyResponse();
  }
  catch (IOException ioe) { System.out.println("sendConfiguration(): IOException at seq="+proxyMsgSeq); System.out.println(ioe.getMessage()); }

// Wait until proxy has seen maxEvents.

  if (wait) readProxyResponse();

  return;
}

public void StartRun( String comment )
{
  int runNumber = newRun();
  // make log entry
  String logsubject = "New run started: Run "+runNumber;
  logStart( logsubject,comment );
  // reset counters (might be best to send stop first).
  if (prefs.getBoolean("runtime.triggerPanel",false)) trig.resetCounts();

  // Loop over the boards to start the run
  Set<String> keys = prefsDBMap.keySet();
  Iterator<String> it = keys.iterator();
  while (it.hasNext())
  {
    String key = it.next();
    map.setStateFromPreferences(prefsDBMap.get(key),prefsPCMap.get(key),prefs);
    map.setDaqReset(false);
    map.setDummyIngressReset(prefs.getBoolean("runtime.dummy-ingress-reset",true));
    map.setGate(true);
    map.setTag(0);
    map.setMepTfcControl(false);
    map.setMepFixedDestination(true);
    map.setTfcDecoderEnable(true);
    map.setGbePollEnable(true);
    String febType = prefs.get("runtime.feb-type","CLARO");
    if ( febType.equals("CLARO") )
    {
      cla.setStateFromPreferences(prefsFEMap.get(key),dbSideMap.get(key).intValue());
    }
    else if ( febType.equals("MAROC3") )
    {
      mar.setStateFromPreferences(prefsFEMap.get(key),dbSideMap.get(key).intValue());
    }
    sendConfiguration(key,0,false);
  }

  tagTF.setText("0");

  return;
}

public void logStart( String logsubject, String comment )
{
  if ( !prefs.getBoolean("runtime.recording",false) ) {return;}

  String prefix = "runtime.";
  int latency = prefs.getInt( prefix+"latency" , 16 );
  int triggerD = prefs.getInt( prefix+"triggerDelay" , 16 );
  int strobeL = prefs.getInt( prefix+"strobeLength", 7 );
  String source = runprefs.get("RunControl.trigsource","unknown");

  String logentry;

  if ( comment == null )
  {
    logentry = String.format("New run%ntrigger=%1$s; latency=%2$d; triggerDelay=%3$d; strobeLength=%4$d",source,latency,triggerD,strobeL);
  }
  else
  {
    logentry = String.format("%1$s%ntrigger=%2$s; latency=%3$d; triggerDelay=%4$d; strobeLength=%5$d",comment,source,latency,triggerD,strobeL);
  }

  elog( logsubject, logentry, false );

  logEnd = true;
}

public StringBuffer makeCounterTable()
{
  StringBuffer html = new StringBuffer("<table><tr><th>Counter</th><th>Count</th></tr>");
  for ( int i=0; i<6; i++ )
  {
    int count = trig.getCount(i);
    String counterLabel = countprefs.get("counter"+i+"_label","Counter A");
    html.append("<tr><td>"+counterLabel+"</td><td>"+count+"</td></tr>");
  }
  html.append("</table>");
  return html;
}

public void logStop( StringBuffer html )
{
  if ( !logEnd ) return;

  logEnd = false;

  //  if ( !prefs.getBoolean("runtime.recording",false) ) {return;}

  elog("End of run counters",html.toString(),true);

  return;
}

public void setupProxy()
{
  Vector< String > filenames = new Vector<String>();
  Vector< String > macAddresses = new Vector<String>();
  Vector< String > ipAddresses = new Vector<String>();

  String folder = prefs.get("runtime.saveFileDestination","/data/");

  // Loop over the boards
  Set<String> keys = prefsDBMap.keySet();
  Iterator<String> it = keys.iterator();
  while (it.hasNext())
  {
    String dnaString = it.next();
    Preferences prefsDB = prefsDBMap.get(dnaString);
    Preferences prefsPC = prefsPCMap.get(dnaString);
    dnaString = dnaString.replaceAll("\\s+",""); // don't want whitespace in filenames
    filenames.add(folder+"feb-"+dnaString+"-%06d.mdf");

// Get mac address from configurator prefs
    String macString = "";
    for(int j=5; j>=0; j--)
    {
      macString += prefsDB.get("net.mac."+j,"00");
      if(j != 0){macString += ":";}
    }
    macAddresses.add(macString);

    String ipString = "";
    for(int j=3; j>=0; j--)
    {
      ipString += prefsDB.get("net.ip."+j,"0");
      if(j != 0){ipString += ".";}
    }
    ipAddresses.add(ipString);
  }

  if(macAddresses.size() != filenames.size() || ipAddresses.size() != filenames.size())
  {
    System.out.println( "Don't have equal number of filenames, source ip's and source mac addresses" );
  }

  StringBuilder sb = new StringBuilder();

  for(int i=0; i<filenames.size(); i++)
  {
    sb.append(macAddresses.get(i)); sb.append((char)0);
    sb.append(ipAddresses.get(i)); sb.append((char)0);
    sb.append(filenames.get(i)); sb.append((char)0);
  }
  sb.append("--"); sb.append((char)0);

  if (prefs.getBoolean("proxy.pbw.enable",false))
  {
    sb.append(prefs.get("proxy.pbw.fifo","/dev/shm/pbwrly"));
    sb.append((char)0);
  }
  sb.append("--"); sb.append((char)0);

  sendHeader((short)sb.length(),(short)4,0,false);
  try
  {
    proxyMsg.clear();
    proxyMsg.put(sb.toString().getBytes());
    proxyMsg.flip();
    ochannel.write(proxyMsg);
    ostream.flush();
    readProxyResponse();
  }
  catch (IOException ioe) { System.out.println("setupProxy(): IOException at seq="+proxyMsgSeq); System.out.println(ioe.getMessage()); }

  return;
}

public void launchProxy()
{

  String [] cmdArray = {"startProxy.sh","--protocol","0xf4"};
  try
  {
    Process pr = Runtime.getRuntime().exec(cmdArray);
    ostream = pr.getOutputStream();
    ochannel = Channels.newChannel(pr.getOutputStream());
    ichannel = Channels.newChannel(pr.getInputStream());
  }
  catch (IOException ioe){ ioe.printStackTrace(); }

}

// save preferences in a txt file, stored in the same place where the mdf file will be written
public void exportPreferences()
{
  if ( !prefs.getBoolean("runtime.recording",true) ) return;

// Save prefences in XML file
  String folder = prefs.get("runtime.saveFileDestination","/data/");
  int run = prefs.getInt("runtime.runNumber",1);
  try
  {
    File prefsOutputFile = new File(String.format("%1$sprefs_%2$06d.xml",folder,run));
    FileOutputStream prefsOutputStream = new FileOutputStream(prefsOutputFile);
    prefs.exportSubtree(prefsOutputStream);
  }
  catch (IOException e) { e.printStackTrace(); }
  catch (BackingStoreException e) { e.printStackTrace(); }

  try
  {
    File prefsOutputFile = new File(String.format("%1$sFEprefs_%2$06d.xml",folder,run));
    FileOutputStream prefsOutputStream = new FileOutputStream(prefsOutputFile);
// Export of FE settings from configurator preferences
    Set<String> keys = prefsDBMap.keySet();
    Iterator<String> it = keys.iterator();
    while (it.hasNext())
    {
      String key = it.next();
      prefsFEMap.get(key).exportSubtree(prefsOutputStream);
    }
  }
  catch (IOException e) { e.printStackTrace(); }
  catch (BackingStoreException e) { e.printStackTrace(); }

  return;
}

public int newRun()
{

// Increment the run number
  int runNumber = prefs.getInt("runtime.runNumber",1);
  if ( !prefs.getBoolean("runtime.recording",false) ) return runNumber;

  if (runNumber > 0) runNumber += 1;
  prefs.putInt("runtime.runNumber",runNumber);
  runNumberTF.setText(String.format("%1$d",runNumber));

  tagTF.setBackground(Color.green);

// Export preferences
  exportPreferences();

// Send the new run message
  sendHeader((short)0,(short)2,runNumber,true); // Start new run

  return runNumber;
}

public void endRun()
{
  // Loop over the boards to stop the run
  Set<String> keys = prefsDBMap.keySet();
  Iterator<String> it = keys.iterator();
  while (it.hasNext())
  {
    String key = it.next();
    map.setStateFromPreferences(prefsDBMap.get(key),prefsPCMap.get(key),prefs);
    map.setDaqReset(true);
    map.setGate(false);
    map.setTag(0);
    map.setMepTfcControl( false );
    map.setMepFixedDestination( true );
    map.setTfcDecoderEnable( false );
    map.setGbePollEnable( false );
    String febType = prefs.get("runtime.feb-type","CLARO");
    if ( febType.equals("CLARO") )
    {
      cla.setStateFromPreferences(prefsFEMap.get(key),dbSideMap.get(key).intValue());
    }
    else if ( febType.equals("MAROC3") )
    {
      mar.setStateFromPreferences(prefsFEMap.get(key),dbSideMap.get(key).intValue());
    }
    sendConfiguration(key,0,false);
  }

// Send the end run message
  sendHeader((short)0,(short)3,0,true);

  tagTF.setBackground(Color.gray);

  return;
}

public static void main( String[] args )
{

// The pause here is for the splash screen which closes as soon as the first swing window is made visible.

  try
  {
    Thread.sleep(1000);
  }
  catch ( InterruptedException inte )
  {
    System.err.println("Warning: " + inte.getMessage() );
  }

// Force SwingApplet to come up in the System L&F

  String laf = UIManager.getSystemLookAndFeelClassName();
  try
  {
    UIManager.setLookAndFeel(laf);
  }
  catch (UnsupportedLookAndFeelException exc)
  {
    System.err.println("Warning: UnsupportedLookAndFeel: " + laf);
  }
  catch (Exception exc)
  {
    System.err.println("Error loading " + laf + ": " + exc);
  }

  JFrame frame = new JFrame( "JRichEcControl" );
  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

// get handle for the content pane
  Container cP = frame.getContentPane();
  cP.setLayout(new BorderLayout());

  JMenuBar mB = new JMenuBar();
  frame.setJMenuBar( mB );

  JRichEcControl ec = new JRichEcControl( mB );
  cP.add( ec, BorderLayout.NORTH );

  frame.setTitle("JRichEcControl - " + Preferences.userNodeForPackage(JRichEcControl.class).node("JRichEcControl").get("runtime.feb-type","CLARO"));

  frame.pack();
  frame.setVisible(true);

  return;
}

}
