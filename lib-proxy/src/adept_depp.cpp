/* Copyright
RICH EC Control Panel
2016 Konstantin Gizdov University of Edinburgh
Steve Wotton, 2000-2015
Jackson Smith
Cavendish Lab (HEP)
*/

#include <stdio.h>

#include <cstring>
#include <string>

#include <iostream>  // errors

#include "../include/custom.hpp"
#include "../include/adept_depp.hpp"

// Digilent Adept headers
// #undef WIN32
#include "./dpcdecl.h"
#include "./depp.h"
#include "./dmgr.h"
// #define WIN32 1

// HIF hif = hifInvalid;

AdeptDepp::AdeptDepp() : hif(hifInvalid), isOK(false), deviceString("Chimaera2") {}

AdeptDepp::~AdeptDepp() {}

AdeptDepp::AdeptDepp(std::string devName) : hif(hifInvalid), isOK(false), deviceString(devName) {}

int AdeptDepp::Enumerate() {
    int num = 0;
    BOOL rc = DmgrEnumDevices(&num);
    if (!rc) return 0;
    return num;
}

std::string AdeptDepp::getEnumeratedDevice(int idx) {
    DVC dvcInfo;
    BOOL rc = DmgrGetDvc(idx, &dvcInfo);
    if (!rc) std::cerr << "ERROR: Could not get enumerated USB device" << std::endl;
    // std::string dvcString = env->NewStringUTF(dvcInfo.szConn);
    std::string dvcString = dvcInfo.szConn;
    return dvcString;
}

int AdeptDepp::deppOpen(std::string deviceString) {
    // const char *name = env->GetStringUTFChars(deviceString, 0);
    if (deviceString.length() > 64) {
        std::cerr << "ERROR: USB device name (" << deviceString << ") too long. Aborting" << std::endl;
        return 0;
    }
    const char *name = deviceString.c_str();
    char dv[64];
    strcpy(dv, name);  // replace later with snprintf()

    hif = hifInvalid;
    BOOL rc = DmgrOpen(&hif, dv);
    if ( !rc ) return 0;
    rc = DeppEnable(hif);
    if ( !rc ) return 0;

    return 1;
}

int AdeptDepp::deppClose() {
    if ( hif != hifInvalid ) {
        DeppDisable(hif);
        DmgrClose(hif);
    }
    return 1;
}

// Multiple read of addr,data pairs

int AdeptDepp::deppGetSet(byte addrArray[], size_t addr_size, byte dataArray[], size_t data_size) {
    // jsize addrArrayLength = env->GetArrayLength(addrArray);
    // jsize dataArrayLength = env->GetArrayLength(dataArray);

    byte *addr = addrArray;
    byte *data = dataArray;

    DWORD nget = (data_size < addr_size) ? data_size : addr_size;

    BOOL rc = DeppGetRegSet(hif, reinterpret_cast<BYTE *>(addr), reinterpret_cast<BYTE *>(data), nget, fFalse);

    // env->ReleaseByteArrayElements(addrArray, addr, 0);
    // env->ReleaseByteArrayElements(dataArray, data, 0);

    return rc?1:0;
}

// Multiple write of addr,data pairs

int AdeptDepp::deppPutSet(byte buf[], size_t buf_size) {
    // jsize len = env->GetArrayLength(buf);
    byte *array = buf;

    DWORD npairs = 0; npairs |= (buf_size>>1);  // Buffer length/2

    BOOL rc = DeppPutRegSet(hif, reinterpret_cast<BYTE *>(array), npairs, fFalse);

    // env->ReleaseByteArrayElements(buf, array, 0);

    return rc?1:0;
}

// Multiple write to same address

int AdeptDepp::deppPutRepeat(byte addr, byte buf[], size_t buf_size) {
    // jsize len = env->GetArrayLength(buf);
    // jbyte *array = env->GetByteArrayElements(buf, 0);
    byte *array = buf;

    // Byte is signed in Java so can't simply cast the address
    // Byte is unsigned here, try directly

    // BYTE uaddr = 0; uaddr |= addr;
    // DWORD ulen = 0; ulen |= buf_size;
    BYTE uaddr = 0; uaddr = addr;
    DWORD ulen = 0; ulen = buf_size;

    BOOL rc = DeppPutRegRepeat(hif, uaddr, reinterpret_cast<BYTE *>(array), ulen, fFalse);

    // env->ReleaseByteArrayElements(buf, array, 0);

    return rc?1:0;
}
// Multiple write to same address

int AdeptDepp::deppGetRepeat(byte addr, byte buf[], size_t buf_size) {
    // jsize len = env->GetArrayLength(buf);
    // jbyte *array = env->GetByteArrayElements(buf, 0);
    byte *array = buf;

    // Byte is signed in Java so can't simply cast the address
    // Byte is unsigned here, try directly

    // BYTE uaddr = 0; uaddr |= addr;
    // DWORD ulen = 0; ulen |= buf_size;
    BYTE uaddr = 0; uaddr = addr;
    DWORD ulen = 0; ulen = buf_size;

    BOOL rc = DeppGetRegRepeat(hif, uaddr, reinterpret_cast<BYTE *>(array), ulen, fFalse);

    // env->ReleaseByteArrayElements(buf, array, 0);

    return rc?1:0;
}

bool AdeptDepp::OK() {
    return isOK;
}

void AdeptDepp::connect() {
    // timer.start();
    return;
}

void AdeptDepp::disconnect() {
    // timer.stop();
    close();
    return;
}

// void AdeptDepp::open() throws DeppException {
void AdeptDepp::open() {
    isOK = false;
    //        System.out.println("Open "+deviceString);
    // try {
    //     lock.lock();
    //     int rc = deppOpen( deviceString );
    //     isOK = (rc == 1);
    // }
    // finally {
    //     lock.unlock();
    //     if ( !isOK ) throw new DeppException();
    // }
    int rc = deppOpen(deviceString);
    isOK = (rc == 1);
    // if (!isOK) printf("ERROR: Could not open USB device...\n");
    if (!isOK) std::cerr << "ERROR: Could not open USB device..." << std::endl;

    return;
}

void AdeptDepp::close() {
    isOK = false;
    // try {
    //     lock.lock();
    //     int rc = deppClose();
    // }
    // finally {
    //     lock.unlock();
    // }
    int rc = deppClose();

    if (rc != 1) std::cerr << "ERROR: Could not close USB device...(busy?)" << std::endl;

    return;
}

// Get single byte

// void AdeptDepp::get(byte addr, byte buf[], size_t buf_size ) throws DeppException {
void AdeptDepp::get(byte addr, byte buf[], size_t buf_size) {
    // if ( !isOK ) throw new DeppException();
    if (!isOK) std::cerr << "ERROR: Could not get info from USB device...(not ready?)" << std::endl;

    // try {
    //     lock.lock();
    //     byte [] addrArray = new byte[1]; addrArray[0] = addr;
    //     int rc = deppGetSet(addrArray, 1, buf, buf_size);
    //     isOK = (rc == 1);
    // }
    // finally {
    //     lock.unlock();
    //     if ( !isOK ) throw new DeppException();
    // }
    byte addrArray[1];
    addrArray[0] = addr;
    int rc = deppGetSet(addrArray, 1, buf, buf_size);
    isOK = (rc == 1);

    if (!isOK) std::cerr << "ERROR: Could not get info from USB device...(busy?)" << std::endl;

    return;
}

// Get multiple data

// void AdeptDepp::get(byte addr[], size_t addr_size, byte data[], size_t data_size) throws DeppException {
void AdeptDepp::get(byte addr[], size_t addr_size, byte data[], size_t data_size) {
    // if ( !isOK ) throw new DeppException();
    if (!isOK) std::cerr << "ERROR: Could not get info from USB device...(not ready?)" << std::endl;

    // try {
    //     lock.lock();
    //     int rc = deppGetSet(addr, addr_size, data, data_size);
    //     isOK = (rc == 1);
    // }
    // finally {
    //     lock.unlock();
    //     if ( !isOK ) throw new DeppException();
    // }
    int rc = deppGetSet(addr, addr_size, data, data_size);

    isOK = (rc == 1);
    if (!isOK) std::cerr << "ERROR: Could not get info from USB device...(busy?)" << std::endl;

    return;
}

// Put single addr,byte

// void AdeptDepp::put( byte addr, byte data ) throws DeppException {
void AdeptDepp::put(byte addr, byte data) {
    // if ( !isOK ) throw new DeppException();
    if (!isOK) std::cerr << "ERROR: Could not get info from USB device...(not ready?)" << std::endl;

    // try {
    //     lock.lock();
    //     byte [] buf = new byte[2]; buf[0] = addr; buf[1] = data;
    //     int rc = deppPutSet( buf );
    //     isOK = (rc == 1);
    // }
    // finally {
    //     lock.unlock();
    //     if ( !isOK ) throw new DeppException();
    // }
    byte buf[2];
    buf[0] = addr;
    buf[1] = data;
    int rc = deppPutSet(buf, 2);

    isOK = (rc == 1);
    if (!isOK) std::cerr << "ERROR: Could not put info to USB device...(busy?)" << std::endl;

    return;
}

// Buffer contains addr,data pairs

// void AdeptDepp::put( byte [] buf ) throws DeppException {
void AdeptDepp::put(byte buf[], size_t buf_size) {
    // if ( !isOK ) throw new DeppException();
    if (!isOK) std::cerr << "ERROR: Could not get info from USB device...(not ready?)" << std::endl;

    // try {
    //     lock.lock();
    //     int rc = deppPutSet( buf );
    //     isOK = (rc == 1);
    // }
    // finally {
    //     lock.unlock();
    //     if ( !isOK ) throw new DeppException();
    // }
    int rc = deppPutSet(buf, buf_size);

    isOK = (rc == 1);
    if (!isOK) std::cerr << "ERROR: Could not put info to USB device...(busy?)" << std::endl;
    return;
}

// Push buf[0],buf[1],...,buf[buf.legth-1] to addr

// void AdeptDepp::push( byte addr, byte [] buf ) throws DeppException {
void AdeptDepp::push(byte addr, byte buf[], size_t buf_size) {
    // if ( !isOK ) throw new DeppException();
    if (!isOK) std::cerr << "ERROR: Could not get info from USB device...(not ready?)" << std::endl;

    // try {
    //     lock.lock();
    //     int rc = deppPutRepeat( addr, buf );
    //     isOK = (rc == 1);
    // }
    // finally {
    //     lock.unlock();
    //     if ( !isOK ) throw new DeppException();
    // }
    int rc = deppPutRepeat(addr, buf, buf_size);

    isOK = (rc == 1);
    if (!isOK) std::cerr << "ERROR: Could not push to USB device...(busy?)" << std::endl;
    return;
}

// Pop buf[0],buf[1],...,buf[buf.legth-1] from addr

// void AdeptDepp::pop( byte addr, byte [] buf ) throws DeppException {
void AdeptDepp::pop(byte addr, byte buf[], size_t buf_size) {
    // if ( !isOK ) throw new DeppException();
    if (!isOK) std::cerr << "ERROR: Could not get info from USB device...(not ready?)" << std::endl;

    // try {
    //     lock.lock();
    //     int rc = deppGetRepeat( addr, buf );
    //     isOK = (rc == 1);
    // }
    // finally {
    //     lock.unlock();
    //     if ( !isOK ) throw new DeppException();
    // }
    int rc = deppGetRepeat(addr, buf, buf_size);

    isOK = (rc == 1);
    if (!isOK) std::cerr << "ERROR: Could not pop USB device...(busy?)" << std::endl;
    return;
}

void AdeptDepp::putByte(int base, int addr, int val) {
    byte regs[] = {(byte)(base&0xff), (byte)(addr&0xff), (byte)((base+1)&0xff), (byte)((addr>>8)&0xff), (byte)((base+2)&0xff), (byte)(val&0xff)};
    put(regs, 6);
    return;
}

void AdeptDepp::putBytes(int base, int addr, byte regs[], size_t buf_size) {
    byte hdr[] = {(byte)(base&0xff), (byte)(addr&0xff), (byte)((base+1)&0xff), (byte)((addr>>8)&0xff)};
    put(hdr, 4);
    push((byte)((base+2)&0xff), regs, buf_size);
    return;
}
