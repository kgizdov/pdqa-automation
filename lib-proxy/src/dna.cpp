// Copyright 2016 Konstantin Gizdov University of Edinburgh

#include <stdio.h>

#include <algorithm>
#include <cstring>
#include <iostream>  // errors
#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include "../include/custom.hpp"
#include "../include/dna.hpp"
#include "../include/tinyxml2.h"
#include "../include/xml_helpers.hpp"

Dna::Dna() : dnas() {}

Dna::Dna(const Dna &other) {
    std::vector<dna_t> dnas_ = other.getDNAs();
    std::vector<tinyxml2::XMLDocument *> docs = (&other)->getXMLDocs();
    for (unsigned i = 0; i < dnas_.size(); ++i) {
        dnas[i].first = dnas_[i];
        dnas[i].second = docs[i];
    }
}

Dna &Dna::operator=(const Dna & other) {
    std::vector<dna_t> dnas_ = other.getDNAs();
    std::vector<tinyxml2::XMLDocument *> docs = (&other)->getXMLDocs();
    for (unsigned i = 0; i < dnas_.size(); ++i) {
        dnas[i].first = dnas_[i];
        dnas[i].second = docs[i];
    }
    return *this;
}

Dna::~Dna() {
    for (unsigned i = 0; i < dnas.size(); ++i) {
        delete dnas[i].second;
    }
}

void Dna::init() {
    while (dnas.size() > 0) {  // more safe
        delete dnas[dnas.size()-1].second;
        dnas.pop_back();
    }
    return;
}

dna_t Dna::getDNA(int which) {
    dna_t dummy = {};
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return dummy;
    }
    return dnas[which].first;
}

dna_t Dna::getDNA(std::string name) {
    for (unsigned i = 0; i < dnas.size(); ++i) {
        if (dnas[i].first.name == name || dnas[i].first.file == name) {
            return dnas[i].first;
        }
    }
    dna_t dummy = {};
    std::cerr << "ERROR: no DNA by the name: " << name << std::endl;
    return dummy;
}

std::vector<dna_t> Dna::getDNAs() {
    std::vector<dna_t> dnas_;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        dnas_.push_back(dnas[i].first);
    }
    return dnas_;
}

std::vector<dna_t> Dna::getDNAs() const {
    std::vector<dna_t> dnas_;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        dnas_.push_back(dnas[i].first);
    }
    return dnas_;
}

unsigned Dna::dnaNum() {
    return dnas.size();
}

int Dna::addDNA(std::string name) {
    unsigned check = dnas.size();
    if (dnas.size() == 8) {
        std::cerr << "ERROR: Cannot add more DNAs" << std::endl;
        return -1;
    }
    dna_t temp = {};
    dnas.push_back(std::make_pair(temp, new tinyxml2::XMLDocument()));
    if (check == dnas.size()) {
        std::cerr << "ERROR: Error adding DNA by name" << std::endl;
        return -2;
    }
    setName(name, dnas.size()-1);
    setDB(dnas.size()-1);
    setGain(dnas.size()-1, 0, 128);
    setGain(dnas.size()-1, 1, 128);
    setIP("192.168.0.1", dnas.size()-1);
    setMAC("02:00:00:00:00:11", dnas.size()-1);
    setTriggerType(1, dnas.size()-1);
    setTestPulse(false, dnas.size()-1);
    setDACval(100, dnas.size()-1);
    setHoldDelay(9, dnas.size()-1);
    setTriggerChannel(dnas.size()-1, dnas.size()-1);
    setON(true, dnas.size()-1);
    return 0;
}

int Dna::addDNA(std::string name, std::string ip, std::string mac) {
    unsigned check = dnas.size();
    if (dnas.size() == 8) {
        std::cerr << "ERROR: Cannot add more DNAs" << std::endl;
        return -1;
    }
    dna_t temp = {};
    dnas.push_back(std::make_pair(temp, new tinyxml2::XMLDocument()));
    if (check == dnas.size()) {
        std::cerr << "ERROR: Error adding DNA by name, ip and mac" << std::endl;
        return -2;
    }
    setName(name, dnas.size()-1);
    setDB(dnas.size()-1);
    setGain(dnas.size()-1, 0, 128);
    setGain(dnas.size()-1, 1, 128);
    setIP(ip, dnas.size()-1);
    setMAC(mac, dnas.size()-1);
    setTriggerType(1, dnas.size()-1);
    setTestPulse(false, dnas.size()-1);
    setDACval(100, dnas.size()-1);
    setHoldDelay(9, dnas.size()-1);
    setTriggerChannel(dnas.size()-1, dnas.size()-1);
    setON(true, dnas.size()-1);
    return 0;
}

int Dna::addDNA(std::string name, std::string ip, std::string mac, int on) {
    unsigned check = dnas.size();
    if (dnas.size() == 8) {
        std::cerr << "ERROR: Cannot add more DNAs" << std::endl;
        return -1;
    }
    dna_t temp = {};
    dnas.push_back(std::make_pair(temp, new tinyxml2::XMLDocument()));
    if (check == dnas.size()) {
        std::cerr << "ERROR: Error adding DNA by name, ip, mac and on flag" << std::endl;
        return -2;
    }
    setName(name, dnas.size()-1);
    setDB(dnas.size()-1);
    setGain(dnas.size()-1, 0, 128);
    setGain(dnas.size()-1, 1, 128);
    setIP(ip, dnas.size()-1);
    setMAC(mac, dnas.size()-1);
    setTriggerType(((on == 1) ? 1 : 2), dnas.size()-1);  // set trig on/off state
    setTestPulse(false, dnas.size()-1);
    setDACval(100, dnas.size()-1);
    setHoldDelay(9, dnas.size()-1);
    setTriggerChannel(dnas.size()-1, dnas.size()-1);
    setON(on, dnas.size()-1);
    return 0;
}

int Dna::addDNA(std::string name, std::string ip, std::string mac, int trig_channel, int on) {
    unsigned check = dnas.size();
    if (dnas.size() == 8) {
        std::cerr << "ERROR: Cannot add more DNAs" << std::endl;
        return -1;
    }
    dna_t temp = {};
    dnas.push_back(std::make_pair(temp, new tinyxml2::XMLDocument()));
    if (check == dnas.size()) {
        std::cerr << "ERROR: Error adding DNA by name, ip, mac and on flag" << std::endl;
        return -2;
    }
    setName(name, dnas.size()-1);
    setDB(dnas.size()-1);
    setGain(dnas.size()-1, 0, 128);
    setGain(dnas.size()-1, 1, 128);
    setIP(ip, dnas.size()-1);
    setMAC(mac, dnas.size()-1);
    setTriggerType(((on == 1) ? 1 : 2), dnas.size()-1);  // set trig on/off state
    setTestPulse(false, dnas.size()-1);
    setDACval(100, dnas.size()-1);
    setHoldDelay(9, dnas.size()-1);
    setTriggerChannel(trig_channel, dnas.size()-1);
    setON(on, dnas.size()-1);
    return 0;
}

int Dna::addDNAs(std::vector<std::string> names, std::vector<std::string> ips, std::vector<std::string> macs) {
    if (names.size() != ips.size() || macs.size() != ips.size()) {
        std::cerr << "ERROR: Error adding DNAs, details' sizes do not agree" << std::endl;
        return -1;
    }
    unsigned check = dnas.size();
    if (dnas.size() + names.size() > 8) {
        std::cerr << "ERROR: Cannot add so many DNAs" << std::endl;
        return -2;
    }
    for (unsigned i = 0; i < names.size(); ++i) {
        addDNA(names[i], ips[i], macs[i]);
    }
    if ((check + names.size()) != dnas.size()) {
        std::cerr << "ERROR: Adding DNAs failed" << std::endl;
        return -3;
    }
    return 0;
}

int Dna::addDNAs(std::vector<std::string> names, std::vector<std::string> ips, std::vector<std::string> macs,
    std::vector<int> ons) {
    if (names.size() != ips.size() || macs.size() != ips.size() || macs.size() != ons.size()) {
        std::cerr << "ERROR: Error adding DNAs, details' sizes do not agree" << std::endl;
        return -1;
    }
    unsigned check = dnas.size();
    if (dnas.size() + names.size() > 8) {
        std::cerr << "ERROR: Cannot add so many DNAs" << std::endl;
        return -2;
    }
    for (unsigned i = 0; i < names.size(); ++i) {
        addDNA(names[i], ips[i], macs[i], ons[i]);
    }
    if ((check + names.size()) != dnas.size()) {
        std::cerr << "ERROR: Adding DNAs failed" << std::endl;
        return -3;
    }
    return 0;
}

int Dna::addDNAs(std::vector<std::string> names, std::vector<std::string> ips, std::vector<std::string> macs,
    std::vector<int> trig_channels, std::vector<int> ons) {
    if (names.size() != ips.size() || macs.size() != ips.size() || macs.size() != ons.size() || macs.size() != trig_channels.size()) {
        std::cerr << "ERROR: Error adding DNAs, details' sizes do not agree" << std::endl;
        return -1;
    }
    unsigned check = dnas.size();
    if (dnas.size() + names.size() > 8) {
        std::cerr << "ERROR: Cannot add so many DNAs" << std::endl;
        return -2;
    }
    for (unsigned i = 0; i < names.size(); ++i) {
        addDNA(names[i], ips[i], macs[i], trig_channels[i], ons[i]);
    }
    if ((check + names.size()) != dnas.size()) {
        std::cerr << "ERROR: Adding DNAs failed" << std::endl;
        return -3;
    }
    return 0;
}

int Dna::setName(std::string name, int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }
    dnas[which].first.name = name;
    setFileName(which);
    setFolderName(which);
    return 0;
}

int Dna::setName(std::string name, std::string which) {
    for (unsigned i = 0; i < dnas.size(); ++i) {
        if (dnas[i].first.name == which || dnas[i].first.file == which) {
            return setName(name, i);
        }
    }
    return -1;
}


std::string Dna::getName(int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return std::string("");
    }
    return dnas[which].first.name;
}

std::vector<std::string> Dna::getNames() {
    std::vector<std::string> names;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        names.push_back(dnas[i].first.name);
    }
    return names;
}

int Dna::setCustomFileName(std::string file, int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }
    std::string str;
    str = file;
    str.erase(std::remove_if(str.begin(), str.end(), ::isspace), str.end());
    dnas[which].first.file = str;
    return 0;
}

int Dna::setFileNames() {
    std::string str;
    unsigned i = 0;
    while (i < dnas.size()) {
        // dnas[i].db = i % 2;
        str = dnas[i].first.name;
        str.erase(std::remove_if(str.begin(), str.end(), ::isspace), str.end());
        dnas[i].first.file = str;
        // std::cerr << dnas[i].file << std::endl;
        str = "";
        // for (unsigned j = 0; j < 2; ++j) {
        //     dnas[i].pmt[j].gain = 128;  // default gain is 128
        //     dnas[i].pmt[j].pos = 2*i + j % 2;  // 0(0,1); 1(2,3); 2(4,5)...
        // }
        i++;
    }
    return 0;
}

int Dna::setFileName(int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }
    std::string str;
    str = dnas[which].first.name;
    str.erase(std::remove_if(str.begin(), str.end(), ::isspace), str.end());
    dnas[which].first.file = str;
    return 0;
}

std::string Dna::getFileName(int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return std::string("");
    }

    return dnas[which].first.file;
}

std::vector<std::string> Dna::getFileNames() {
    std::vector<std::string> files;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        files.push_back(dnas[i].first.file);
    }
    return files;
}

int Dna::setCustomFolderName(std::string folder, int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }
    std::string str;
    str = folder;
    str.erase(std::remove_if(str.begin(), str.end(), ::isspace), str.end());
    dnas[which].first.folder = str;
    return 0;
}

int Dna::setCustomFolderNames(std::string folder) {
    for (unsigned i = 0; i < dnas.size(); ++i) {
        setCustomFolderName(folder, i);
    }
    return 0;
}

int Dna::setFolderNames() {  // init or clear folder names
    std::string str;
    str = "";
    for (unsigned i = 0; i < dnas.size(); ++i) {
        dnas[i].first.folder = str;
    }
    return 0;
}

int Dna::setFolderName(int which) {  // auto method to init folder names
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }
    std::string str;
    str = "";
    dnas[which].first.folder = str;
    return 0;
}

std::string Dna::getFolderName(int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return "";
    }
    return dnas[which].first.folder;
}

std::vector<std::string> Dna::getFolderNames() {
    std::vector<std::string> folders;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        folders.push_back(dnas[i].first.folder);
    }
    return folders;
}

int Dna::getDB(int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }
    return dnas[which].first.db;
}

std::vector<int> Dna::getDBs() {
    std::vector<int> dbs;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        dbs.push_back(dnas[i].first.db);
    }
    return dbs;
}

int Dna::getON(int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }
    return dnas[which].first.on;
}

std::vector<int> Dna::getONs() {
    std::vector<int> ons;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        ons.push_back(dnas[i].first.on);
    }
    return ons;
}

std::vector<bool> Dna::getBoolONs() {
    std::vector<bool> ons;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        if (dnas[i].first.on == 0) {
            ons.push_back(false);
        } else {
            ons.push_back(true);
        }
    }
    return ons;
}

std::vector<bool> Dna::getTrigMap() {
    std::vector<bool> trig_map;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        trig_map.push_back(false);
    }
    for (unsigned i = 0; i < dnas.size(); ++i) {
        if (dnas[i].first.on == 1) {
            trig_map[dnas[i].first.trig_ch] = true;
        }
    }
    return trig_map;
}


bool Dna::checkBoolONs() {
    for (unsigned i = 0; i < dnas.size(); ++i) {
        if (dnas[i].first.on == 0) return false;
    }
    return true;
}


int Dna::setGain(int which, int pmt, int gain) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }
    if (gain < 0 || gain > 255) {
        std::cerr << "ERROR: Invalid Gain value" << gain << std::endl;
        return -2;
    }
    if (pmt != 0 && pmt != 1) {
        std::cerr << "ERROR: Invalid PMT value" << pmt << std::endl;
        return -3;
    }
    dnas[which].first.gain[pmt] = gain;
    return 0;
}

int Dna::setGain(std::string name, int pmt, int gain) {
    int which = -1;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        if (dnas[i].first.name == name || dnas[i].first.file == name) {
            which = i;
            break;
        }
    }
    if (which == -1) {
        std::cerr << "ERROR: no DNA by the name: " << name << std::endl;
        return -1;
    }
    if (setGain(which, pmt, gain) < 0) {
        return -2;
    }
    return 0;
}

int Dna::setGainAll(int gain) {
    for (unsigned i = 0; i < dnas.size(); ++i) {
        for (unsigned j = 0; j < 2; ++j) {
            if (setGain(i, j, gain) < 0) return -1;
        }
    }
    return 0;
}

int Dna::getGain(int which, int pmt) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }
    if (pmt != 0 && pmt != 1) {
        std::cerr << "ERROR: Invalid PMT value" << pmt << std::endl;
        return -3;
    }
    return dnas[which].first.gain[pmt];
}

std::vector<int> Dna::getGainAll() {
    std::vector<int> gains;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        for (unsigned j = 0; j < 2; ++j) {
            gains.push_back(getGain(i, j));
        }
    }
    return gains;
}

int Dna::setIP(std::string ip, int which) {
    if (!checkIPAddress(ip)) {
        std::cerr << "ERROR: invalid IPs cannot be added in DNA" << std::endl;
        return -1;
    }
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }
    dnas[which].first.ip = ip;
    return 0;
}

int Dna::setIP(std::string ip, std::string name) {
    if (!checkIPAddress(ip)) {
        std::cerr << "ERROR: invalid IPs cannot be added in DNA" << std::endl;
        return -1;
    }
    for (unsigned i = 0; i < dnas.size(); ++i) {
        if (dnas[i].first.name == name || dnas[i].first.file == name) {
            dnas[i].first.ip = ip;
            return 0;
        }
    }
    std::cerr << "ERROR: IP not updated, no DNA by that name..." << std:: endl;
    return -1;
}
std::string Dna::getIP(int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return std::string("");
    }
    return dnas[which].first.ip;
}
std::vector<std::string> Dna::getIPs() {
    std::vector<std::string> ips;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        ips.push_back(dnas[i].first.ip);
    }
    return ips;
}

int Dna::setMAC(std::string mac, int which) {
    if (!checkMACAddress(mac.c_str())) {
        std::cerr << "ERROR: invalid MACs cannot be added in DNA" << std::endl;
        return -1;
    }
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }
    dnas[which].first.mac = mac;
    return 0;
}

int Dna::setMAC(std::string mac, std::string name) {
    if (!checkMACAddress(mac.c_str())) {
        std::cerr << "ERROR: invalid MACs cannot be added in DNA" << std::endl;
        return -1;
    }
    for (unsigned i = 0; i < dnas.size(); ++i) {
        if (dnas[i].first.name == name || dnas[i].first.file == name) {
            dnas[i].first.mac = mac;
            return 0;
        }
    }
    std::cerr << "ERROR: MAC not updated, no DNA by that name..." << std:: endl;
    return -1;
}
std::string Dna::getMAC(int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return std::string("");
    }
    return dnas[which].first.mac;
}

std::vector<std::string> Dna::getMACs() {
    std::vector<std::string> macs;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        macs.push_back(dnas[i].first.mac);
    }
    return macs;
}

int Dna::setTriggerType(int trig) {  // trig 1 is LVDS, 0 is Pulser, 2 is off
    if (trig < 0 && trig > 2) {
        std::cerr << "ERROR: Not a valid trigger type" << trig << std::endl;
        return -1;
    }
    for (unsigned i = 0; i < dnas.size(); ++i) {
        dnas[i].first.trig = trig;
    }
    return 0;
}

int Dna::setTriggerType(int trig, int which) {  // trig 1 is LVDS, 0 is Pulser, 2 is off
    if (trig < 0 || trig > 2) {
        std::cerr << "ERROR: Not a valid trigger type" << trig << std::endl;
        return -1;
    }
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -2;
    }
    dnas[which].first.trig = trig;
    return 0;
}

int Dna::getTriggerType(int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }
    return dnas[which].first.trig;
}

std::vector<int> Dna::getTriggerTypes() {
    std::vector<int> trigs;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        trigs.push_back(dnas[i].first.trig);
    }
    return trigs;
}

std::vector<bool> Dna::getBoolTriggerTypes() {
    std::vector<bool> trigs;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        if (dnas[i].first.trig == 1) {
            trigs.push_back(true);
        } else {
            trigs.push_back(false);
        }
    }
    return trigs;
}

int Dna::setTestPulse(bool on) {
    for (unsigned i = 0; i < dnas.size(); ++i) {
        dnas[i].first.testpulse = on;
    }
    return 0;
}

int Dna::setTestPulse(bool on, int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }
    dnas[which].first.testpulse = on;
    return 0;
}

bool Dna::checkTestPulse() {
    for (unsigned i = 0; i < dnas.size(); ++i) {
        if (dnas[i].first.testpulse == false) return false;
    }
    return true;
}

int Dna::checkTestPulse(int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }

    return dnas[which].first.testpulse;
}

std::vector<bool> Dna::getTestPulse() {
    std::vector<bool> test;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        test.push_back(dnas[i].first.testpulse);
    }
    return test;
}

int Dna::setDACval(int DACval) {
    if (DACval < 0 || DACval > 255) {
        std::cerr << "ERROR: Invalid DAC value " << DACval << std::endl;
        return -1;
    }
    for (unsigned i = 0; i < dnas.size(); ++i) {
        dnas[i].first.dac = DACval;
    }
    return 0;
}

int Dna::setDACval(int DACval, int which) {
    if (DACval < 0 || DACval > 255) {
        std::cerr << "ERROR: Invalid DAC value " << DACval << std::endl;
        return -1;
    }
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -2;
    }
    dnas[which].first.dac = DACval;
    return 0;
}

int Dna::getDACval(int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }

    return dnas[which].first.dac;
}

std::vector<int> Dna::getDACvals() {
    std::vector<int> dacs;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        dacs.push_back(dnas[i].first.dac);
    }
    return dacs;
}

int Dna::setHoldDelay(int hold_delay) {
    if (hold_delay < 0) {
        std::cerr << "ERROR: Invalid Hold Delay value " << hold_delay << std::endl;
        return -1;
    }
    for (unsigned i = 0; i < dnas.size(); ++i) {
        dnas[i].first.hold = hold_delay;
    }
    return 0;
}

int Dna::setHoldDelay(int hold_delay, int which) {
    if (hold_delay < 0) {
        std::cerr << "ERROR: Invalid Hold Delay value " << hold_delay << std::endl;
        return -1;
    }
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -2;
    }
    dnas[which].first.hold = hold_delay;
    return 0;
}
int Dna::getHoldDelay(int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -2;
    }

    return dnas[which].first.hold;
}

std::vector<int> Dna::getHoldDelays() {
    std::vector<int> holds;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        holds.push_back(dnas[i].first.hold);
    }
    return holds;
}

int Dna::setTriggerChannel(int trig_channel, int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -2;
    }
    if (trig_channel >= 8 || trig_channel < 0) {
        std::cerr << "ERROR: Channel out of bounds (0..7)" << which << std::endl;
        return -3;
    }
    dnas[which].first.trig_ch = trig_channel;
    return 0;
}

int Dna::setTriggerChannels(std::vector<int> trig_channels) {
    if (trig_channels.size() != dnas.size()) {
        std::cerr << "ERROR: Trigger channel map does not match number of DNAs" << std::endl;
        return -1;
    }
    for (unsigned i = 0; i < dnas.size(); ++i) {
        setTriggerChannel(trig_channels[i], i);
    }
    return 0;
}

int Dna::getTriggerChannel(int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -2;
    }
    return dnas[which].first.trig_ch;
}

std::vector<int> Dna::getTriggerChannels() {
    std::vector<int> trig_channels;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        trig_channels.push_back(dnas[i].first.trig_ch);
    }
    return trig_channels;
}

int Dna::LoadSettings(const std::string &settings_file_name) {
    int result_ = 0;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        tinyxml2::XMLError errCheck = dnas[i].second->LoadFile(settings_file_name.c_str());  // load settings file
        if (errCheck != 0 && settings_file_name.substr(0, 2) == "./") {
            errCheck = dnas[i].second->LoadFile(std::string("." + settings_file_name).c_str());  // try in a different folder if needed
        }
        if (errCheck != 0) {  // check if file is loaded
            result_--;
            std::cerr << "ERROR: DNA " << i << " could not load settings from XMLs..." << std::endl;
        }
        XMLCheckSuccess(errCheck);
        std::cerr << "INFO: DNA " << i << " loaded settings from XML." << std::endl;
    }
    return result_;  // success
}

int Dna::setDB(int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }
    dnas[which].first.db = which % 2;
    return 0;
}

int Dna::setON(int on, int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }
    dnas[which].first.on = on == 1 ? true : false;
    return 0;
}

int Dna::setON(bool on, int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }
    dnas[which].first.on = on;
    return 0;
}

tinyxml2::XMLDocument *Dna::getXMLDoc(int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return NULL;
    }
    return dnas[which].second;
}

tinyxml2::XMLDocument *Dna::getXMLDoc(int which) const {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return NULL;
    }
    return dnas[which].second;
}

std::vector<tinyxml2::XMLDocument *> Dna::getXMLDocs() {
    std::vector<tinyxml2::XMLDocument *> docs;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        docs.push_back(getXMLDoc(i));
    }
    return docs;
}

std::vector<tinyxml2::XMLDocument *> Dna::getXMLDocs() const {
    std::vector<tinyxml2::XMLDocument *> docs;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        docs.push_back(getXMLDoc(i));
    }
    return docs;
}

int Dna::UpdateXMLGain(int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }

    tinyxml2::XMLNode *node = getNodeByName(dnas[which].second, "default");  // go to section in XML for settings

    if (node == NULL) {
        std::cerr << "ERROR: Failed to read XML doc for MAROC for settings" << std::endl;
        return -1;
    }

    std::string section;
    for (int k = 0; k < 4; k++) {  // four sectors A(0),B(0),C(1),D(1)
        // Preserve orientation
        if (k == 0) {
            if (dnas[which].first.db == 1) continue;
            section = "A";
        } else if (k == 1) {
            if (dnas[which].first.db == 1) continue;
            section = "B";
        } else if (k == 2) {
            if (dnas[which].first.db == 0) continue;
            section = "C";
        } else {
            if (dnas[which].first.db == 0) continue;
            section = "D";
        }

        node = moveToNode2(node, section);
        if (node == NULL) {
            std::cerr << "ERROR: Moving node to " << section
            << " in general maroc settings failed. Aborting..." << std::endl;
            return -1;
        }

        tinyxml2::XMLNode *tempnode = node;  // temp node for traversing

        for (int j = 0; j < 64; j++) {  // 64 channels per MAROC
            tempnode = node;

            std::stringstream anode_name;
            if (j+1 < 10) {
                anode_name << 0 << j+1;
            } else {
                anode_name << j+1;
            }

            tempnode = moveToInnerNode2(tempnode, anode_name.str());
            if (tempnode == NULL) {
                std::cerr << "ERROR: Getting maroc settings for PD Anode " << anode_name.str()
                << " failed. Aborting..." << std::endl;
                return -1;
            }

            // set gain
            int32_t gain = dnas[which].first.gain[(k == 0 || k == 3) ? 0 : 1];
            setIntPropertyIntoXMLNode(tempnode, "maroc.gain", gain);

            anode_name.str(std::string());
        }
    }
    return 0;
}

int Dna::UpdateXMLGains() {
    for (unsigned i = 0; i < dnas.size(); ++i) {
        UpdateXMLGain(i);
    }
    return 0;
}

int Dna::UpdateXMLCTest(int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }

    tinyxml2::XMLNode *node = getNodeByName(dnas[which].second, "default");  // go to section in XML for settings

    if (node == NULL) {
        std::cerr << "ERROR: Failed to read XML doc for MAROC for settings" << std::endl;
        return -1;
    }

    std::string section;
    for (int k = 0; k < 4; k++) {  // four sectors A(0),B(0),C(1),D(1)
        // Preserve orientation
        if (k == 0) {
            if (dnas[which].first.db == 1) continue;
            section = "A";
        } else if (k == 1) {
            if (dnas[which].first.db == 1) continue;
            section = "B";
        } else if (k == 2) {
            if (dnas[which].first.db == 0) continue;
            section = "C";
        } else {
            if (dnas[which].first.db == 0) continue;
            section = "D";
        }

        node = moveToNode2(node, section);
        if (node == NULL) {
            std::cerr << "ERROR: Moving node to " << section
            << " in general maroc settings failed. Aborting..." << std::endl;
            return -1;
        }

        tinyxml2::XMLNode *tempnode = node;  // temp node for traversing

        for (int j = 0; j < 64; j++) {  // 64 channels per MAROC
            tempnode = node;

            std::stringstream anode_name;
            if (j+1 < 10) {
                anode_name << 0 << j+1;
            } else {
                anode_name << j+1;
            }

            tempnode = moveToInnerNode2(tempnode, anode_name.str());
            if (tempnode == NULL) {
                std::cerr << "ERROR: Getting maroc settings for PD Anode " << anode_name.str()
                << " failed. Aborting..." << std::endl;
                return -1;
            }

            // set ctest
            bool ctest = dnas[which].first.testpulse;
            setBoolPropertyIntoXMLNode(tempnode, "maroc.ctest", ctest);

            anode_name.str(std::string());
        }
    }
    return 0;
}

int Dna::UpdateXMLCTests() {
    for (unsigned i = 0; i < dnas.size(); ++i) {
        UpdateXMLCTest(i);
    }
    return 0;
}
