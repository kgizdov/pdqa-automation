#!/bin/bash

# Change dir manually to the current folder
# cd /home/lhcbuser/lib-proxy
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# echo $DIR
cd $DIR
# The following line redirects error messages. Can be changed.
# Do not redirect stdin or stdout. They are used for communication with the GUI
exec 2>temp_status.log

# echo $@

./bin/temp_status $@

echo "temp_status done." >&2

# echo "ex"
