// Copyright 2016 Konstantin Gizdov Univ of Edinburgh

#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include <stdint.h>
#include <pthread.h>

#include <cstdlib>
#include <string>
#include <sstream>
#include <iostream>
#include <algorithm>

#include "../include/easywsclient.hpp"

typedef uint8_t byte;       // define 8 bit byte
typedef uint16_t byte16_t;  // define 16 bit byte

using easywsclient::WebSocket;
static WebSocket::pointer ws = NULL;

int sequence = 0;

pthread_mutex_t wait_ = PTHREAD_MUTEX_INITIALIZER;

struct msg_t {
    uint16_t length;  // length of following message
    uint16_t comm;    // current command type
    uint32_t info;    // accompanying info
    uint32_t seq;     // exchange sequence number
};

union msg_u {
    msg_t inf;
    byte b[sizeof(msg_t)];
};

msg_u msg;

bool IsOneOfThese(char c) {
    switch (c) {
        // case '\260':  // degree sign
        case '\057':  // forward slash
        case '\134':  // back slash
        case '\045':  // percent sign
        case 'A':
        case 'C':
        case 'm':
        case 's':
        case 'V':
            return true;
        default:
            if (c >= '\177' || c<= '\37') {  // ignore everything outside ANSI
                return true;
            }
            return false;
    }
}

void writeStringToBytes(const std::string &input, byte byteArray[], int offset) {
    for (unsigned i = 0; i < input.length(); ++i) {
        byteArray[i+offset] = input[i];
    }
    return;
}

void writeToParent(const std::string &message, unsigned is_err = 0) {
    bool debug = true;
    if (message.compare("ok") == 0 || message.compare("er") == 0) {
        msg.inf.length = 0;
    } else {
        msg.inf.length = message.size();
    }
    msg.inf.comm = 0;
    msg.inf.info = is_err;
    // msg.inf.seq = sequence;  // already read from parent

    if (write(1, &msg.b, sizeof(msg.b)) != sizeof(msg.b)) {
        std::cerr << "ERROR: Could not respond to parent..." << std::endl;
        msg.inf.length = 0;  // nulify command
    }
    fflush(stdout);
    if (debug) {
        std::cerr << "DEBUG: command to PARENT [length, command, info, sequence] -> "
        << msg.inf.length << " " << msg.inf.comm << " " << msg.inf.info << " " << msg.inf.seq << std::endl;
    }
    if (msg.inf.length > 0) {
        write(1, message.c_str(), message.size());
        fflush(stdout);
        std::cerr << "[SENT TO PARENT] -> " << message.c_str() << std::endl;
    }
    return;
}

void handle_message(const std::string &message) {
    std::cerr << message.length() << std::endl;
    if (!message.substr(0, 5).compare("ERROR")) {
        std::cerr << "[" << sequence << "] ";
        std::cerr << "ERROR: Received -> " << message << std::endl;  // record error in log
        writeToParent("er", 1);
        return;
    }

    std::string str = message;
    str.erase(std::remove_if(str.begin(), str.end(), &IsOneOfThese), str.end());
    writeToParent(str);
    return;
}

void return_temps(const std::string &message) {
    std::cerr << message.length() << std::endl;
    if (!message.substr(0, 5).compare("ERROR")) {
        std::cerr << "[" << sequence << "] ";
        std::cerr << "ERROR: Received -> " << message << std::endl;  // record error in log
        writeToParent("er", 1);
        return;
    }

    std::string str = message;
    str.erase(std::remove_if(str.begin(), str.end(), &IsOneOfThese), str.end());
    std::cerr << "[" << sequence << "] ";
    std::cerr << "READ -> " << str << std::endl;
    std::stringstream str1(str);
    // str1 << str;
    std::string buf, temps;
    for (unsigned i = 0; i < 4; ++i) {
        std::getline(str1, buf, ',');
        if (i % 2 == 0) {
            continue;
        }
        temps += buf + ", ";
    }
    std::cerr << "Parsed to -> " << temps << std::endl;
    writeToParent(temps);
    return;
}

void return_humidity(const std::string &message) {
    std::cerr << message.length() << std::endl;
    if (!message.substr(0, 5).compare("ERROR")) {
        std::cerr << "[" << sequence << "] ";
        std::cerr << "ERROR: Received -> " << message << std::endl;  // record error in log
        writeToParent("er", 1);
        return;
    }

    std::string str = message;
    str.erase(std::remove_if(str.begin(), str.end(), &IsOneOfThese), str.end());
    std::cerr << "[" << sequence << "] ";
    std::cerr << "READ -> " << str << std::endl;
    std::stringstream str1(str);
    std::string buf, humidity;
    for (unsigned i = 0; i < 2; ++i) {
        std::getline(str1, buf, ',');
        if (i == 0) {
            continue;
        }
        humidity += buf;
    }
    std::cerr << "Parsed to -> " << humidity << std::endl;
    writeToParent(humidity);
    return;
}

void return_hum_temp(const std::string &message) {
    std::cerr << message.length() << std::endl;
    if (!message.substr(0, 5).compare("ERROR")) {
        std::cerr << "[" << sequence << "] ";
        std::cerr << "ERROR: Received -> " << message << std::endl;  // record error in log
        writeToParent("er", 1);
        return;
    }

    std::string str = message;
    str.erase(std::remove_if(str.begin(), str.end(), &IsOneOfThese), str.end());
    std::cerr << "[" << sequence << "] ";
    std::cerr << "READ -> " << str << std::endl;
    std::stringstream str1(str);
    std::string buf, hum_temp;
    std::getline(str1, buf, ',');
    hum_temp += buf;
    std::cerr << "Parsed to -> " << hum_temp << std::endl;
    writeToParent(hum_temp);
    return;
}

void handle_status(const std::string &message) {
    std::cerr << message.length() << std::endl;
    if (!message.substr(0, 5).compare("ERROR")) {
        std::cerr << "[" << sequence << "] ";
        std::cerr << "ERROR: Received -> " << message << std::endl;  // record error in log
        writeToParent("er", 1);
        return;
    }
    writeToParent("ok");
    return;
}

void handle_login(const std::string &message) {
    if (!message.substr(0, 5).compare("ERROR")) {
        std::cerr << "[" << sequence << "] ";
        std::cerr << "ERROR: Received -> " << message << std::endl;  // record error in log
        std::cerr << "ERROR: Could not login..." << std::endl;
        writeToParent("er", 1);
        return;
    } else if (!message.compare("OK")) {
        std::cerr << "[" << sequence << "] ";
        std::cerr << "SUCCESS: Log in successful." << std::endl;  // record login success
        writeToParent("ok");
        return;
    }
    return;
}

int main(int argc, char const *argv[]) {
    if (argc <= 2 || argc > 3) {
        std::cerr << "ERROR: Wrong number of arguments" << std::endl;
        std::cerr << "USAGE: temp_status <IP> <PORT>" << std::endl;
        writeToParent("er");
        exit(-1);
    }
    std::string prefix, ip, port;
    prefix = "ws://";
    ip = std::string(argv[1]);
    port = std::string(argv[2]);
    ws = WebSocket::from_url(prefix + ip +":" + port);
    assert(ws);
    pthread_mutex_lock(&wait_);
    ws->send("Status?");
    ws->poll();
    pthread_mutex_unlock(&wait_);
    if (ws->getReadyState() != WebSocket::CLOSED) {
        pthread_mutex_lock(&wait_);
        ws->poll(1000);
        pthread_mutex_unlock(&wait_);
        pthread_mutex_lock(&wait_);
        ws->dispatch(handle_status);
        pthread_mutex_unlock(&wait_);
    }
    pthread_mutex_lock(&wait_);
    ws->poll(1000);
    pthread_mutex_unlock(&wait_);

    bool comm_ok = true;
    bool debug = true;
    char * commBuf = new char[1024];
    while (ws->getReadyState() != WebSocket::CLOSED && ws->getReadyState() != WebSocket::CLOSING) {
        for (unsigned received = 0; received < sizeof(msg_t); ) {
            if (!comm_ok) break;  // quit on bad command
            size_t len = sizeof(msg_t) - received;
            len = read(0, msg.b+received, len);

            if (len == 0) {
                std::cerr << "ERROR: Encountered EOL, could not read command. Exiting..." << std::endl;
                comm_ok = false;
                break;
            }
            if (len > 0) received += len;
        }

        uint16_t length = 0, comm = 17;  // assume failed read, comm = 17 to quit gracefully
        uint32_t info, seq;
        if (comm_ok) {
            length = msg.inf.length;  // length of following message
            comm = msg.inf.comm;      // current command type
            info = msg.inf.info;      // accompanying info
            seq = msg.inf.seq;        // exchange sequence number
            sequence = seq;
        }

        if (length > 0) {
            for (unsigned received = 0; received < length;) {
                size_t len = length - received;
                len = read(0, commBuf + received, len);
                if (len == 0) {
                    std::cerr << "ERROR: Could not read full message length. Try again..." << std::endl;
                    comm_ok = false;
                    comm = 17;
                    break;
                }
                if (len > 0) received += len;
            }
        }

        if (debug) {
            std::cerr << "DEBUG: command type [length, command, info, sequence] -> "
            << length << " " << comm << " " << info << " " << seq << std::endl;
        }

        switch (comm) {
            case 1: {
                pthread_mutex_lock(&wait_);
                ws->send("Status?");
                ws->poll(1000);
                pthread_mutex_unlock(&wait_);
                pthread_mutex_lock(&wait_);
                ws->poll(1000);
                pthread_mutex_unlock(&wait_);
                pthread_mutex_lock(&wait_);
                ws->dispatch(handle_message);
                pthread_mutex_unlock(&wait_);
                break;
            }
            case 2: {
                pthread_mutex_lock(&wait_);
                ws->send("Sensors?");
                ws->poll(1000);
                pthread_mutex_unlock(&wait_);
                pthread_mutex_lock(&wait_);
                ws->poll(1000);
                pthread_mutex_unlock(&wait_);
                pthread_mutex_lock(&wait_);
                ws->dispatch(handle_message);
                pthread_mutex_unlock(&wait_);
                break;
            }
            case 3: {
                pthread_mutex_lock(&wait_);
                ws->send("Temperature?");
                ws->poll(1000);
                pthread_mutex_unlock(&wait_);
                pthread_mutex_lock(&wait_);
                ws->poll(5000);
                pthread_mutex_unlock(&wait_);
                pthread_mutex_lock(&wait_);
                ws->dispatch(return_temps);
                pthread_mutex_unlock(&wait_);
                break;
            }
            case 4: {
                pthread_mutex_lock(&wait_);
                ws->send("Humidity?");
                ws->poll(1000);
                pthread_mutex_unlock(&wait_);
                pthread_mutex_lock(&wait_);
                ws->poll(1000);
                pthread_mutex_unlock(&wait_);
                pthread_mutex_lock(&wait_);
                ws->dispatch(return_humidity);
                pthread_mutex_unlock(&wait_);
                break;
            }
            case 5: {
                pthread_mutex_lock(&wait_);
                ws->send("Password:setpdqa");
                ws->poll(1000);
                pthread_mutex_unlock(&wait_);
                pthread_mutex_lock(&wait_);
                ws->poll(5000);
                pthread_mutex_unlock(&wait_);
                pthread_mutex_lock(&wait_);
                ws->dispatch(handle_login);
                pthread_mutex_unlock(&wait_);
                break;
            }
            case 6: {
                pthread_mutex_lock(&wait_);
                ws->send("Humidity?");
                ws->poll(1000);
                pthread_mutex_unlock(&wait_);
                pthread_mutex_lock(&wait_);
                ws->poll(1000);
                pthread_mutex_unlock(&wait_);
                pthread_mutex_lock(&wait_);
                ws->dispatch(return_hum_temp);
                pthread_mutex_unlock(&wait_);
                break;
            }
            case 17: {
                pthread_mutex_lock(&wait_);
                ws->close();
                ws->poll(1000);
                pthread_mutex_unlock(&wait_);
                std::cerr << "[" << sequence << "] ";
                std::cerr << "Quiting..." << std::endl;  // log exit
                writeToParent("ok");
                break;
            }
            default: {
                std::cerr << "[" << sequence << "] ";
                std::cerr << "Not a valid command (" << comm << ")...Try again" << std::endl;  // record invalid command
                writeToParent("no", 1);
                break;
            }
        }
    }
    delete ws;
    delete[] commBuf;
    return 0;
}
