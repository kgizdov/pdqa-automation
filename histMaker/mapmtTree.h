//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Aug 21 17:46:48 2013 by ROOT version 5.34/05
// from TTree mdfTree/mdfTree
// found on file: mapmtfeb_000725.root
//////////////////////////////////////////////////////////

#ifndef mapmtTree_h
#define mapmtTree_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

// Fixed size dimensions of array or collections stored in the TTree if any.

class mapmtTree {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   UInt_t          timestamp;
   //UShort_t        frame;
   UInt_t        evid;
   UShort_t        totalHits;
   UInt_t          tag;
   UInt_t         PMT[2][64];

   // List of branches
   TBranch        *b_timestamp;   //!
   //TBranch        *b_frame;   //!
   TBranch        *b_evid;   //!
   TBranch        *b_totalHits;   //!
   TBranch        *b_tag;   //!
   TBranch        *b_PMT[2][64];   //!

   mapmtTree(TTree *tree=0);
   mapmtTree(const char *fileName);
   virtual ~mapmtTree();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef mapmtTree_cxx
mapmtTree::mapmtTree(TTree *tree) : fChain(0)
{
   if (tree != 0) Init(tree);
}

mapmtTree::mapmtTree(const char *fileName) : fChain(0)
{
// Connect the file
// used to generate this class and read the Tree.

  TTree *tree=0;
  TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(fileName);
  if (!f || !f->IsOpen()) f = new TFile(fileName);
  f->GetObject("mdfTree",tree);

  Init(tree);
}

mapmtTree::~mapmtTree()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t mapmtTree::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t mapmtTree::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void mapmtTree::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("timestamp", &timestamp, &b_timestamp);
   //fChain->SetBranchAddress("frame", &frame, &b_frame);
   fChain->SetBranchAddress("evid", &evid, &b_evid);
   fChain->SetBranchAddress("totalHits", &totalHits, &b_totalHits);
   fChain->SetBranchAddress("tag", &tag, &b_tag);
   for (unsigned ch=0;ch<64;ch++)
   {
     char pmtName[16];
     sprintf(pmtName,"PMT1_C%02d",ch+1);
     fChain->SetBranchAddress(pmtName, &PMT[0][ch], &b_PMT[0][ch]);
     sprintf(pmtName,"PMT2_C%02d",ch+1);
     fChain->SetBranchAddress(pmtName, &PMT[1][ch], &b_PMT[1][ch]);
   }
   Notify();
}

Bool_t mapmtTree::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void mapmtTree::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t mapmtTree::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef mapmtTree_cxx
