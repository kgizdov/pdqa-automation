#include "mdfHistos.h"

#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <vector>
#include <limits>
#include <sstream>
#include <iomanip>

#include <TTree.h>
using namespace std;

//________________________________________________________________________
// Initialize data members.
mdfHistos::mdfHistos( const bool debug )
  : mdfTreeSeg()
  , h_evid(0)
  , h_timestamp(0)
    //, h_frame(0)
  , h_totalHits(0)
  , h_pmt1Hits(0)
  , h_pmt2Hits(0)
  , h_pmt1(0)
  , h_pmt2(0)
  , tc_hits(0)
  , tc_pmt1(0)
  , tc_pmt2(0)
    //, tc_all(0)
  , leg_hits(0)
  , tc_pmt1SCurve(0)
  , tc_pmt2SCurve(0)
  , m_debug( debug )
{
  if( m_debug )
    std::cout << "mdfHistos::mdfHistos(void) : initializing to 0." << std::endl;

  for( unsigned ch = 0; ch < 64; ++ch )
  {
    h_pmt1SCurve[ch] = 0;
    h_pmt2SCurve[ch] = 0;
  }

  return;
}


//________________________________________________________________________
// Copy constructor; avoid default copy constructor
mdfHistos::mdfHistos( const mdfHistos &a )
  : mdfTreeSeg()        // Members are initialized in initialize method
  , h_evid(0)
  , h_timestamp(0)
    //, h_frame(0)
  , h_totalHits(0)
  , h_pmt1Hits(0)
  , h_pmt2Hits(0)
  , h_pmt1(0)
  , h_pmt2(0)
  , tc_hits(0)
  , tc_pmt1(0)
  , tc_pmt2(0)
    //, tc_all(0)
  , leg_hits(0)
  , tc_pmt1SCurve(0)
  , tc_pmt2SCurve(0)
  , m_debug( a.m_debug )
{
  if( m_debug )
    std::cout << "mdfHistos::mdfHistos(const mdfHistos &) : initializing to 0." << std::endl;

  for( unsigned ch = 0; ch < 64; ++ch )
  {
    h_pmt1SCurve[ch] = 0;
    h_pmt2SCurve[ch] = 0;
  }

  return;
}


//________________________________________________________________________
// Destructor
mdfHistos::~mdfHistos()
{
  if( m_debug )
    std::cout << "mdfHistos::~mdfHistos : not deleting histograms." << std::endl;

  // Delete the histograms
  // Don't delete.  They are owned by Root on creation.
  //if( h_evid )          delete h_evid;
  //if( h_timestamp )     delete h_timestamp;
  //if( h_frame )         delete h_frame;
  //if( h_totalHits )     delete h_totalHits;
  //if( h_pmt1Hits )      delete h_pmt1Hits;
  //if( h_pmt2Hits )      delete h_pmt2Hits;

  //for( unsigned ch = 0; ch < 64; ++ch)
  //{
  //  if( h_pmt1SCurve[ch] ) delete h_pmt1SCurve[ch];
  //  if( h_pmt2SCurve[ch] ) delete h_pmt2SCurve[ch];
  //}

  //if( tc_hits )         delete tc_hits;
  //if( leg_hits )        delete leg_hits;
  //if( tc_pmt1SCurve )   delete tc_pmt1SCurve;
  //if( tc_pmt2SCurve )   delete tc_pmt2SCurve;

  return;
}


//________________________________________________________________________
// The initialize method sets up the tree variables and histograms
bool mdfHistos::initialize( TTree *tt, std::string prefix )
{
  if( m_debug )
    std::cout << "mdfHistos::initialize  Initializing branches for " << prefix << std::endl;

  // Initialize the TTree
  bool fstatus = SetBranchAddresses( tt, prefix );


  if( m_debug )
    std::cout << "mdfHistos::initialize  Initializing histos for " << prefix << std::endl;

  // Assume that the prefix has a trailing '_'
  std::string title_pfx( getPrefix(), 0, getPrefix().size() - 1 );

  // Create the histograms
  std::string tit_evid = title_pfx + std::string( " Event ID" );
  h_evid = new TH1D(tit_evid.c_str(), "", 100, 0, 1200);
  h_evid->GetXaxis()->SetTitle("event id");
  h_evid->GetYaxis()->SetTitle("Number of events");

  std::string tit_timestamp = title_pfx + std::string( " Timestamp" );
  h_timestamp = new TH1D(tit_timestamp.c_str(), "", 55, 0, 4105000000);
  h_timestamp->GetXaxis()->SetTitle("t (ns)");
  h_timestamp->GetYaxis()->SetTitle("Number of events");


  //std::string tit_frame = title_pfx + std::string( " Frame" );
  //h_frame = new TH1D(tit_frame.c_str(), "", 55, -500, 9000);
  //h_frame->GetXaxis()->SetTitle("Frame");
  //h_frame->GetYaxis()->SetTitle("Number of events");

  std::string tit_totalHits = title_pfx + std::string( " Total Hits" );
  h_totalHits = new TH1D(tit_totalHits.c_str(), "", 129, 0, 128);
  h_totalHits->GetXaxis()->SetTitle("Count");
  h_totalHits->GetYaxis()->SetTitle("Number of events");

  std::string tit_pmt1Hits = title_pfx + std::string( " PMT1 Hits" );
  h_pmt1Hits = new TH1D(tit_pmt1Hits.c_str(), "", 100, 0, 100);
  h_pmt1Hits->GetXaxis()->SetTitle("Count");
  h_pmt1Hits->GetYaxis()->SetTitle("Number of events");

  std::string tit_pmt2Hits = title_pfx + std::string( " PMT2 Hits" );
  h_pmt2Hits = new TH1D(tit_pmt2Hits.c_str(), "", 100, 0, 100);
  h_pmt2Hits->GetXaxis()->SetTitle("Count");
  h_pmt2Hits->GetYaxis()->SetTitle("Number of events");


  std::string tit_pmt1 = title_pfx + std::string( " PMT1" );
  h_pmt1 = new TH2I(tit_pmt1.c_str(), "", 8, 0., 8., 8, 0., 8.);
  h_pmt1->GetXaxis()->SetTitle("Column");
  h_pmt1->GetYaxis()->SetTitle("Row");

  std::string tit_pmt2 = title_pfx + std::string( " PMT2" );
  h_pmt2 = new TH2I(tit_pmt2.c_str(), "", 8, 0, 8.0, 8, 0, 8.0);
  h_pmt2->GetXaxis()->SetTitle("Column");
  h_pmt2->GetYaxis()->SetTitle("Row");


  for( unsigned ch = 0; ch < 64; ++ch )
  {
    std::stringstream tit_pmt1Title;
    tit_pmt1Title << title_pfx << " S-Curve PMT1 Ch"
                 << std::setfill('0') << std::setw(2) << ch + 1;
    h_pmt1SCurve[ch] = new TProfile(tit_pmt1Title.str().c_str(), "", 1024, 0, 1024, 0, 32, " ");
    h_pmt1SCurve[ch]->GetXaxis()->SetTitle("Threshold /DAC-ref");
    h_pmt1SCurve[ch]->GetYaxis()->SetTitle("Fraction of events");


    std::stringstream tit_pmt2Title;
    tit_pmt2Title << title_pfx << " S-Curve PMT2 Ch"
                 << std::setfill('0') << std::setw(2) << ch + 1;
    h_pmt2SCurve[ch] = new TProfile(tit_pmt2Title.str().c_str(), "", 1024, 0, 1024, 0, 32, " ");
    h_pmt2SCurve[ch]->GetXaxis()->SetTitle("Threshold /DAC-ref");
    h_pmt2SCurve[ch]->GetYaxis()->SetTitle("Fraction of events");
  }

  return fstatus;
}


//________________________________________________________________________
// Read one event from the MDF file.
bool mdfHistos::fillOne( Int_t flag )
{
  if( !getTTree() )
  {
    std::cerr << "Attempt to fill histograms without first initializing. Flag is " << flag << std::endl;
    return false;
  }

  h_evid->Fill(evid);
  h_timestamp->Fill(timestamp);
  //h_frame->Fill(frame);

  // I do not understand why the channels are counted by halves.
  // I need to ask about this.
  unsigned pmt1_count1 = 0;  //, pmt1_count2 = 0;
  unsigned pmt2_count1 = 0;  //, pmt2_count2 = 0;

  for(unsigned ch = 0; ch < 64; ch++)
  {
    /*
    if(getPrefix()=="B01_"){
      PMT1[4]=0;
      PMT1[5]=0;
      PMT1[6]=0;
      PMT1[7]=0;
      PMT1[12]=0;
      PMT1[13]=0;
      PMT1[14]=0;
      PMT1[15]=0;

      PMT1[20]=0;
      PMT1[21]=0;
      PMT1[22]=0;
      PMT1[23]=0;
      PMT1[28]=0;
      PMT1[29]=0;
      PMT1[30]=0;
      PMT1[31]=0;
    }
    */

    /*
    if(getPrefix()=="B02_"){
      PMT1[35]=0;
      PMT1[36]=0;
      PMT1[43]=0;
      PMT1[44]=0;
      PMT1[51]=0;
      PMT1[52]=0;
      PMT1[59]=0;
      PMT1[60]=0;


    }
    */

    if(getPrefix()=="B01_"){
      //if(ch==36 || ch==37 || ch==38 || ch==39 || ch==44 || ch==45 || ch==46 || ch==47 || ch==52 || ch==53 || ch==54 || ch==55 || ch==60 || ch==61 || ch==62 || ch==63){
      //pmt1_count1 += 0;
      //}else{
      pmt1_count1 += PMT1[ch];
      //}
      //}
      //pmt1_count2 += PMT1[ch+32];
      //if(ch==1){
      //pmt2_count1 += 0;
      //}else{
      pmt2_count1 += PMT2[ch];
      //}
    } else if (getPrefix()=="B02_") {
      //right EC from the front
      if (ch==32 || ch==33 || ch==40 || ch==41 || ch==48 || ch==49 || ch==56 || ch==57) {
        pmt1_count1 += 0;
      } else {
        pmt1_count1 += PMT1[ch];
      }
      pmt2_count1 += PMT2[ch];
    }
    // pmt2_count2 += PMT2[ch+32];
    // h_pmt1->SetBinContent( (ch/8)+1, (ch%8)+1, PMT1[ch] );
    // h_pmt2->SetBinContent( (ch/8)+1, (ch%8)+1, PMT2[ch] );

    // cout<<"pixel "<<ch/8<<" "<<ch%8<<" "<<PMT1[ch]<<endl;
    // printf("%d\n", PMT1[ch]);

    if (getPrefix() == "B01_") {
      // left EC from the front
      // if(ch==36 || ch==37 || ch==38 || ch==39 || ch==44 || ch==45 || ch==46 || ch==47 || ch==52 || ch==53 || ch==54 || ch==55 || ch==60 || ch==61 || ch==62 || ch==63 ){
      // }else{
      h_pmt1->Fill((ch%8), 7-(ch/8), PMT1[ch]);
      // }
      h_pmt2->Fill((ch/8), (ch%8), PMT2[ch]);
    } else if (getPrefix() == "B02_") {
      //right EC from the front
      h_pmt2->Fill(7-(ch/8), 7-(ch%8), PMT2[ch]);
      if (ch==32 || ch==33 || ch==40 || ch==41 || ch==48 || ch==49 || ch==56 || ch==57) {
      } else {
        h_pmt1->Fill(7-(ch%8), (ch/8), PMT1[ch]);
      }
    }



  }

  // I really don't understand why half of the counts are ignored.
  //h_pmt1Hits->Fill( pmt1_count1 );
  /*
  if(getPrefix()=="B01_"){
    //left EC from the front
    h_pmt1->SetBinContent(5, 1, 0. );
    h_pmt1->SetBinContent(6, 1, 0. );
    h_pmt1->SetBinContent(7, 1, 0. );
    h_pmt1->SetBinContent(8, 1, 0. );

    h_pmt1->SetBinContent(5, 2, 0. );
    h_pmt1->SetBinContent(6, 2, 0. );
    h_pmt1->SetBinContent(7, 2, 0. );
    h_pmt1->SetBinContent(8, 2, 0. );

    h_pmt1->SetBinContent(5, 3, 0. );
    h_pmt1->SetBinContent(6, 3, 0. );
    h_pmt1->SetBinContent(7, 3, 0. );
    h_pmt1->SetBinContent(8, 3, 0. );

    h_pmt1->SetBinContent(5, 4, 0. );
    h_pmt1->SetBinContent(6, 4, 0. );
    h_pmt1->SetBinContent(7, 4, 0. );
    h_pmt1->SetBinContent(8, 4, 0. );

  }else

  if(getPrefix()=="B02_"){
    //right EC from the front
    h_pmt1->SetBinContent(7, 5, 0. );
    h_pmt1->SetBinContent(7, 6, 0. );
    h_pmt1->SetBinContent(7, 7, 0. );
    h_pmt1->SetBinContent(7, 8, 0. );

    h_pmt1->SetBinContent(8, 5, 0. );
    h_pmt1->SetBinContent(8, 6, 0. );
    h_pmt1->SetBinContent(8, 7, 0. );
    h_pmt1->SetBinContent(8, 8, 0. );

    //h_pmt2->SetBinContent(7, 1, 0. );
    //h_pmt2->SetBinContent(4, 2, 0. );

  }
  */
  h_pmt1Hits->Fill( pmt1_count1 );//+ pmt1_count2 );
  //h_pmt2Hits->Fill( pmt2_count1 );
  h_pmt2Hits->Fill( pmt2_count1 );// + pmt2_count2 );


  // And i don't understand why the total is only for PMT1
  //h_totalHits->Fill(pmt1_count1 + pmt1_count2);
  h_totalHits->Fill(totalHits);

  for (unsigned ch = 0; ch < 64; ch++) {
    h_pmt1SCurve[ch]->Fill(tag , PMT1[ch]);
    h_pmt2SCurve[ch]->Fill(tag , PMT2[ch]);
  }


  return true;
}


//________________________________________________________________________
// Finalize the histograms
bool mdfHistos::finalize( void )
{
  // Write all of the histograms
  h_evid->Write();
  h_timestamp->Write();
  //h_frame->Write();
  h_totalHits->Write();
  h_pmt1Hits->Write();
  h_pmt2Hits->Write();
  h_pmt1->Write();
  h_pmt2->Write();

  for( unsigned ch = 0; ch < 64; ++ch)
  {
    h_pmt1SCurve[ch]->Write();
    h_pmt2SCurve[ch]->Write();
  }

  // Create some specific plots.
  std::string title_pfx( getPrefix(), 0, getPrefix().size() - 1 );

  // Plot superposed hit totals
  std::stringstream tit_hits;
  tit_hits << title_pfx << " total count";
  tc_hits = new TCanvas(tit_hits.str().c_str(), tit_hits.str().c_str(), 69, 75, 300, 200);

  h_totalHits->SetLineColor(kBlack);
  h_totalHits->Draw();
  h_pmt1Hits->SetLineColor(kRed);
  h_pmt1Hits->Draw("SAME");
  h_pmt2Hits->SetLineColor(kBlue);
  h_pmt2Hits->Draw("SAME");



  leg_hits = new TLegend(0.75, 0.6, 0.99, 0.99);
  leg_hits->SetHeader("PMT counts");
  leg_hits->AddEntry(h_totalHits, "Total count", "l");
  leg_hits->AddEntry(h_pmt1Hits, "PMT 1 count", "l");
  leg_hits->AddEntry(h_pmt2Hits, "PMT 2 count", "l");
  leg_hits->AddEntry((TObject*)0, "Total=PMT1+PMT2", "");
  leg_hits->Draw();

  tc_hits->SetLogy(1);
  tc_hits->Write();

  std::stringstream tc_tit_pmt1;
  tc_tit_pmt1 << title_pfx << " PMT1 map";
  tc_pmt1 = new TCanvas(tc_tit_pmt1.str().c_str(), tc_tit_pmt1.str().c_str(), 69, 75, 300, 200);
  h_pmt1->Draw("COLZ");
  tc_pmt1->Write();

  std::stringstream tc_tit_pmt2;
  tc_tit_pmt2 << title_pfx << " PMT2 map";
  tc_pmt2 = new TCanvas(tc_tit_pmt2.str().c_str(), tc_tit_pmt2.str().c_str(), 69, 75, 300, 200);
  h_pmt2->Draw("COLZ");
  tc_pmt2->Write();

  /*
  std::stringstream tc_tit_all;
  tc_tit_all << "Total map";
  tc_all = new TCanvas(tc_tit_all.str().c_str(), tc_tit_all.str().c_str(), 69, 75, 300, 200);
  tc_all->Divide(2,2);

  if(getPrefix()=="B01_"){
  tc_all->cd(1);
  h_pmt2->Draw("COLZ");
  tc_all->cd(3);
  h_pmt1->Draw("COLZ");
  }else if(getPrefix()=="B02_"){
  tc_all->cd(2);
  h_pmt1->Draw("COLZ");
  tc_all->cd(4);
  h_pmt2->Draw("COLZ");
  }
  tc_all->Write();
  */

  std::stringstream tit_pmt1SCurve;
  tit_pmt1SCurve << title_pfx << " PMT1 S-Curves, all channels";
  tc_pmt1SCurve = new TCanvas(tit_pmt1SCurve.str().c_str(), tit_pmt1SCurve.str().c_str(), 69, 75, 300, 200);

  // Why are only the first 32 channels plotted in the original?
  h_pmt1SCurve[0]->Draw();
  for( unsigned ch = 1; ch < 64; ch++)
  {
    h_pmt1SCurve[ch]->Draw("SAME");
  }

  tc_pmt1SCurve->Write();


  std::stringstream tit_pmt2SCurve;
  tit_pmt2SCurve << title_pfx << " PMT2 S-Curves, all channels";

  tc_pmt2SCurve = new TCanvas(tit_pmt2SCurve.str().c_str(), tit_pmt2SCurve.str().c_str(), 69, 75, 300, 200);

  // Why are only the first 32 channels plotted in the original?
  h_pmt2SCurve[0]->Draw();
  for( unsigned ch = 1; ch < 32; ch++ )
  {
    h_pmt2SCurve[ch]->Draw("SAME");
  }

  tc_pmt2SCurve->Write();


  return true;
}


