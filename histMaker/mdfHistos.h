#ifndef MDFHISTOS_H
#define MDFHISTOS_H 1

#include <string>
#include <limits>
#include <fstream>

#include <TTree.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TProfile.h>
#include <TCanvas.h>
#include <TLegend.h>

#include "mdfTreeSeg.h"

/** @class mdfHistos mdfHistos.h
 *
 *  Handles the event-by-event reading of a RICH MDF file.
 *
 *  @date   2013-09-18
 */


class mdfHistos : public mdfTreeSeg {
public:
  //Constructor
  mdfHistos( const bool debug = false );

  virtual ~mdfHistos();

  virtual bool initialize( TTree *tt = 0, std::string prefix = "" );
  virtual bool fillOne( Int_t flag=0 );
  virtual bool finalize( void );


private:
  // Make copy constructor private until the implementation makes sense.
  mdfHistos( const mdfHistos &a );

  // Histograms and graphs
  TH1D *h_evid;
  TH1D *h_timestamp;
  //TH1D *h_frame;
  TH1D *h_totalHits;
  TH1D *h_pmt1Hits;
  TH1D *h_pmt2Hits;

  TH2I *h_pmt1;
  TH2I *h_pmt2;


  TProfile *h_pmt1SCurve[64];
  TProfile *h_pmt2SCurve[64];

  // Plotting canvases and primitives
  TCanvas *tc_hits;
  TCanvas *tc_pmt1;
  TCanvas *tc_pmt2;
  TLegend *leg_hits;
  TCanvas *tc_pmt1SCurve;
  TCanvas *tc_pmt2SCurve;

  //TCanvas *tc_all;

  bool m_debug;
};

#endif  // MDFHISTOS_H
