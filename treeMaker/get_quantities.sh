#!/bin/bash

if [[ $# != 4 ]]; then
    exit -1
fi

LEDON="$1"
RUN_NUMBER="$2"

PMT="$3"

INPUT_FOLDER="$4"
# INPUT_FILE="$6"

# The following line redirects stdout and error messages. Can be changed.
# Do not redirect stdin. It used for communication with the GUI
LOG_FILE=${INPUT_FOLDER}/log/root_to_png_${RUN_NUMBER}_${PMT}_Quantities.log
exec 1>>"${LOG_FILE}" 2>>"${LOG_FILE}"

# Change dir manually to the current folder
# cd /home/lhcbuser/lib-proxy
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# echo $DIR
cd "$DIR" || return 1

# echo $@

./getQuantities "${LEDON}" "${INPUT_FOLDER}/${PMT}_${RUN_NUMBER}.root" "${INPUT_FOLDER}/${PMT}_${RUN_NUMBER}_Quantities.root"
