//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Sep 24 09:25:04 2014 by ROOT version 6.00/02
// from TTree mdfTreeSeg/mdfTreeSeg
// found on file: mdfparsed.root
// And then substantially modified to serve a
//   different purpose.
//////////////////////////////////////////////////////////

#ifndef mdfTreeSeg_h
#define mdfTreeSeg_h

#include <string>

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>


class mdfTreeSeg {
public :
  // Constructor
  mdfTreeSeg( void );

  // Destructor
  virtual ~mdfTreeSeg() { }

  // Create branches on the tree
  bool Branches(TTree *tree = 0, std::string prefix = "");

  // Attach variables to existing branches
  bool SetBranchAddresses(TTree *tree = 0, std::string prefix = "");

  // Accessors
  TTree *getTTree( void ) const { return fChain; }
  std::string getPrefix( void ) const { return m_prefix; }

  // For simplicity, the variables attached to the tree are public
  UInt_t          timestamp;
  UInt_t          tag;
  UShort_t        totalHits;
  UShort_t        pmt1Hits;
  UShort_t        pmt2Hits;
  UInt_t          evid;
  UChar_t         PMT1[64];
  UChar_t         PMT2[64];
  UShort_t         MAR1_ADC[64];
  UShort_t         MAR2_ADC[64];
  UShort_t         PMT1_ADC[64];
  UShort_t         PMT2_ADC[64];


private :

   TTree          *fChain;      //!pointer to the analyzed TTree or TChain
   std::string    m_prefix;     // Prefix for branch names.


   // List of branches.  May not be necessary.
   TBranch        *b_timestamp;
   TBranch        *b_tag;
   TBranch        *b_totalHits;
   TBranch        *b_PMT1Hits;
   TBranch        *b_PMT2Hits;
   TBranch        *b_evid;
   TBranch        *b_PMT1;
   TBranch        *b_PMT2;
   TBranch        *b_MAR1_ADC;
   TBranch        *b_MAR2_ADC;
   TBranch        *b_PMT1_ADC;
   TBranch        *b_PMT2_ADC;
};

#endif

