#ifndef MDFREADER_H
#define MDFREADER_H 1

#include <string>
#include <limits>
#include <fstream>
#include <map>

#include <TTree.h>

#include "mdfTreeSeg.h"

/** @class mdfReader mdfReader.h
 *
 *  Handles the event-by-event reading of a RICH MDF file.
 *
 *  @date   2013-09-18
 */

typedef std::pair<unsigned int, unsigned int> ui_pair;
typedef std::pair<unsigned int, ui_pair> ui_ui_pair;

class mdfReader {
 public:
  //Constructor
  mdfReader(const std::string inputFileName, const bool obs_head = false, const bool debug = false, const std::string mapping = "AB");

  ~mdfReader() { finalize(); }

  bool initialize( TTree *tt = 0, std::string prefix = "", unsigned long ev_first = 0 );
  bool readOne( void );
  bool finalize( void );

  // Accessors for the data members read from the file.
  unsigned short sum( void ) const { return m_treeVar.totalHits; }
  unsigned short pmt1Hits( void ) const { return m_treeVar.pmt1Hits; }
  unsigned short pmt2Hits( void ) const { return m_treeVar.pmt2Hits; }
  unsigned timestamp(void) const { return m_treeVar.timestamp; }
  //unsigned short frame( void ) const { return m_treeVar.frame; }
  unsigned int evid( void ) const { return m_treeVar.evid; }
  unsigned int tag( void ) const { return m_treeVar.tag; }
  unsigned int PMT(unsigned pmt, unsigned pixel) const;

  std::string inputFileName( void ) const { return m_inputFileName; }
  unsigned long eventInFile( void ) const { return m_eventInFile; }

  // Random access to events
  // The fast access method assumes that each record is 104 bytes long.
  std::streampos evSeekFast( unsigned long ev_first ){ return 104 * ev_first; }
  std::streampos evSeekCareful( unsigned long ev_first );

 private:

  // method to calculate hits from mdf
  void calculateVariables(unsigned char* PMTone, unsigned char* PMTtwo);

  // Method to initialize branches in the input TTree
  bool initOutputTree( TTree *tt, std::string prefix );

  std::map <unsigned int, ui_pair> maroc1mapping;
  std::map <unsigned int, ui_pair> maroc2mapping;

  //Input file
  std::string m_inputFileName;  // File name of input MDF file
  std::fstream *m_infile;       // File handle
  std::streampos m_pos;         // Current position in file
  std::streampos m_pos_prev;    // Last position in file
  bool m_obs_head;              // Use obsolete L1 event header format.

  //Choice of mapping file
  std::string m_mapping;

  // Counters
  unsigned long m_eventStart;   // First event
  unsigned long m_eventInFile;  // Ordinal in file

  // Variables from the mdf file;  Can be attached to an output TTree
  mdfTreeSeg m_treeVar;

  //Diagnostics
  bool m_debug;

  // Output TTree
  TTree *m_outputTree;
  std::string m_br_prefix;
};

#endif  // MDFREADER_H
