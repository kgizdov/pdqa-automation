// Copyright 2017 Konstantin Gizdov University of Edinburgh

#include <cstring>
#include <iostream>
#include <memory>
#include <string>

#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TString.h"
#include "TH1D.h"

void plot_adc(char * num, const char *fname, const char *pmt1name, const char *pmt2name, const char *out_path) {
    TFile * f = TFile::Open(TString::Format(fname));

    auto mdfTree = std::unique_ptr<TTree>(static_cast<TTree *>(f->Get("mdfTree")));
    auto c = std::unique_ptr<TCanvas>(new TCanvas("c", "c", 2000, 2000));
    c->Divide(8, 8);
    for (int i = 0; i < 64; i++) {
        c->cd(i+1);
        gPad->SetLogy();
        mdfTree->Draw(TString::Format("B01_PMT1_ADC[%d]>>hist%d(256, 0, 256)", i, i));

        c->Update();
    }
    c->SaveAs(TString::Format("%s/PMT1_ADC_%s_%s.png", out_path, pmt1name, num));

    unsigned short PMT1_ADC_tree[64], PMT2_ADC_tree[64], PMT1_ADC_tree_[64], PMT2_ADC_tree_[64];

    auto file1 = std::unique_ptr<TFile>(TFile::Open(TString::Format("%s/%s_%s.root", out_path, pmt1name, num), "RECREATE"));
    TTree tree1("mdfTree", "ADC tree");
    tree1.Branch("B01_PMT1_ADC", &PMT1_ADC_tree_, "B01_PMT1_ADC[64]/s");
    mdfTree->SetBranchAddress("B01_PMT1_ADC", &PMT1_ADC_tree);
    for (unsigned i = 0; i < mdfTree->GetEntries(); ++i) {
        mdfTree->GetEntry(i);
        for (unsigned j = 0; j < 64; ++j) {
            PMT1_ADC_tree_[j] = PMT1_ADC_tree[j];
        }
        tree1.Fill();
    }
    file1->Write();

    if (std::strcmp(pmt2name, "none") == 0) {  // no need to draw if PMT is 2 inch
        return;
    }

    auto c1 = std::unique_ptr<TCanvas>(new TCanvas("c1", "c", 2000, 2000));
    c1->Divide(8, 8);
    for (int i = 0; i < 64; i++) {
        c1->cd(i+1);
        gPad->SetLogy();
        mdfTree->Draw(TString::Format("B01_PMT2_ADC[%d]>>hist1%d(256, 0, 256)", i, i));

        c1->Update();
    }
    c1->SaveAs(TString::Format("%s/PMT2_ADC_%s_%s.png", out_path, pmt2name, num));

    auto file2 = std::unique_ptr<TFile>(TFile::Open(TString::Format("%s/%s_%s.root", out_path, pmt2name, num), "RECREATE"));
    TTree tree2("mdfTree", "ADC tree");
    tree2.Branch("B01_PMT1_ADC", &PMT2_ADC_tree_, "B01_PMT1_ADC[64]/s");
    mdfTree->SetBranchAddress("B01_PMT2_ADC", &PMT2_ADC_tree);
    for (unsigned i = 0; i < mdfTree->GetEntries(); ++i) {
        mdfTree->GetEntry(i);
        for (unsigned j = 0; j < 64; ++j) {
            PMT2_ADC_tree_[j] = PMT2_ADC_tree[j];
        }
        tree2.Fill();
    }
    file2->Write();
}

int main(int argv, char * argc[]) {
    if (argv != 6) {
        std::cout << "Usage: " << argc[0] << " " << "<RUN NUMBER> <ROOT_FILE> <OUTPUT PATH> <PMT1 ID> <PMT2 ID>" << std::endl;
        return 1;
    }
    char * num = argc[1];
    char * fname = argc[2];
    char * out_path = argc[3];
    if (std::string(out_path).size() <= 1) {
        snprintf(out_path, sizeof("."), "%s", ".");
    }
    char * pmt1name = argc[4];
    char * pmt2name = argc[5];
    plot_adc(num, fname, pmt1name, pmt2name, out_path);
    return 0;
}
