#!/bin/bash

if [[ $# != 5 ]]; then
    exit -1
fi

RUN_NUMBER="$1"

DNA="$2"

PMT1="$3"
PMT2="$4"

INPUT_FOLDER="$5"
# INPUT_FILE="$6"

# The following line redirects stdout and error messages. Can be changed.
# Do not redirect stdin. It used for communication with the GUI
LOG_FILE=${INPUT_FOLDER}/log/root_to_png_${RUN_NUMBER}_${PMT1}_${PMT2}.log
exec 1>>"${LOG_FILE}" 2>>"${LOG_FILE}"

# Change dir manually to the current folder
# cd /home/lhcbuser/lib-proxy
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# echo $DIR
cd "$DIR" || return 1

# echo $@

./new_plot "${RUN_NUMBER}" "${INPUT_FOLDER}/${PMT1}_${PMT2}_${RUN_NUMBER}_${DNA}.root" "${INPUT_FOLDER}" "${PMT1}" "${PMT2}"
