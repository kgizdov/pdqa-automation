#include "mdfReader.h"

#include "mdfrich.h"

#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <vector>
#include <limits>
#include <sstream>
#include <iomanip>
#include <bitset>

#include <TTree.h>
using namespace std;

//________________________________________________________________________
// Initialize data members.
// File opening and closing will be done in the initialize and finalize
//   methods.
//________________________________________________________________________
    mdfReader::mdfReader( const std::string inFileName, const bool obs_head, const bool debug )
    : m_inputFileName(inFileName)
    , m_infile(0)
    , m_pos(0)
    , m_pos_prev(0)
    , m_obs_head(obs_head)
    , m_eventStart(0)
    , m_eventInFile(0)
    , m_treeVar()
    , m_debug(debug)
      , m_outputTree(0)
      , m_br_prefix("")
{
    for( unsigned j = 0; j < 64; ++j )
    {
        m_treeVar.PMT1[j] = 0;
        m_treeVar.PMT2[j] = 0;
        m_treeVar.MAR1_ADC[j] = 0;
        m_treeVar.MAR2_ADC[j] = 0;
    }

    cout << "Building MAROC->PMT ADC mapping" << endl;
//    ifstream myfile ("MAROC_PMT_MAPPING.txt");
    ifstream myfile ("mapping_AB_new.txt");
//    ifstream myfile ("mapping_CD_new.txt");
//    ifstream myfile ("mapping_2inch_fake.txt");
    string line;
    while ( getline (myfile,line) )
    {
            cout << line << endl;
            istringstream iss(line);
            unsigned int sub[5];
            unsigned int i(0);
            while ( iss >> sub[i] ) i++;
            if      (sub[1] == 0) maroc1mapping.insert( ui_ui_pair(sub[0], ui_pair(sub[3], sub[4]) ) );
            if      (sub[1] == 1) maroc2mapping.insert( ui_ui_pair(sub[0], ui_pair(sub[3], sub[4]) ) );
    }
    return;
}


//________________________________________________________________________
// The initialize method opens the MDF file for reading.
bool mdfReader::initialize( TTree *tt, std::string prefix, unsigned long ev_first )
{
    if( m_debug )
        std::cout << "initialize() : opening file " << m_inputFileName << std::endl;

    // Check that the file is not already opened (no previous call to initialize)
    if( !m_infile || !(m_infile->is_open()) )
    {
        // Attempt to open the file.
        m_infile = new std::fstream(m_inputFileName.c_str(), std::ios::in|std::ios::binary);

        // Check file status after opening
        if( !m_infile || !(m_infile->is_open()) )
        {
            std::cerr << "Unable to open file " << m_inputFileName << std::endl;
            if( m_infile )
            {
                delete m_infile;
                m_infile = 0;
            }

            return false;
        }
        else if( m_debug )
        {
            std::cout << "initialize() successfully opened file "
                << m_inputFileName << std::endl;
        }

        // Initialize stream positions and event counter
        m_pos = evSeekCareful(ev_first);
        //m_pos = evSeekFast(ev_first);
        m_pos_prev = m_pos;
        m_eventStart = ev_first;
        m_eventInFile = m_eventStart;
    }

    // Initialize the TTree
    bool fstatus = true;
    if( tt )
        fstatus = initOutputTree( tt, prefix );

    return fstatus;
}


//________________________________________________________________________
// Close the MDF file and turn out the lights.
bool mdfReader::finalize( void )
{
    if( m_infile && m_infile->is_open() )
    {
        m_infile->close();
        delete m_infile;
        m_infile = 0;

        std::cout << "In total, read " << m_eventInFile - m_eventStart
            << " events (" << m_eventStart << " to " << m_eventInFile - 1
            << ") from file " << m_inputFileName << std::endl;
    }

    if( m_debug )
        std::cout << "finalize() : closed file " << m_inputFileName << std::endl;

    return true;
}


//________________________________________________________________________
// Read one event from the MDF file.
bool mdfReader::readOne( void )
{
    // Make sure that we remembered to open the file
    // (A paranoid check)
    if( !m_infile || !(m_infile->is_open()) )
    {
        bool init = initialize();
        if( !init )
        {
            std::cerr << "Attempt to read from a file that cannot be opened:  "
                << m_inputFileName << std::endl;
            return false;
        }
    }

    if( m_infile->eof() )
    {
        if( m_debug )
        {
            std::cout << "  End of file reached:  "
                << m_inputFileName << std::endl;
        }
        return false;
    }

    if( m_debug )
    {
        std::cout << "  Attempting to read an event from "
            << m_inputFileName << std::endl;
    }

    // Structured blocks of header data
    mdfHeader_t mdfHeader;
    eventHeader_t eventHeader;
    rawHeader_t rawHeader;
    ingressHeader_t ingressHeader;
    l1Header_xc0_t l1Header_xc0;

    // Buffer for PMT data
    const unsigned dataLength = 32;
    unsigned char data[dataLength];
    unsigned short maroc1ADC[64];
    unsigned short maroc2ADC[64];
    //unsigned char maroc1ADC[128];
    //unsigned char maroc2ADC[128];
    unsigned short pad[64];

    // Move the file pointer to the next event in the file.
    m_pos_prev = m_pos;           // Update previous file position.
    m_infile->seekg( m_pos );

    // Read the event.
    m_infile->read( (char *)&mdfHeader, sizeof(mdfHeader_t) );
    m_infile->read( (char *)&eventHeader, sizeof(eventHeader_t) );
    m_infile->read( (char *)&rawHeader, sizeof(rawHeader_t) );
    m_infile->read( (char *)&ingressHeader, sizeof(ingressHeader_t) );
    m_infile->read( (char *)&l1Header_xc0, sizeof(l1Header_xc0_t) );
    m_infile->read( (char *)&data, dataLength);
    m_infile->read( (char *)&ingressHeader, sizeof(ingressHeader_t) );
    m_infile->read( (char *)&maroc1ADC, 32*4);
    //m_infile->read( (char *)&pad, 32*4);
    m_infile->read( (char *)&ingressHeader, sizeof(ingressHeader_t) );
    m_infile->read( (char *)&maroc2ADC, 32*4);
    //m_infile->read( (char *)&pad, 32*4);
    /*
       for(unsigned i = 0; i < 64; ++i)
       {
    //std::cout << "GC maroc1 "<< i << " " << static_cast<unsigned>(maroc1ADC[i]) << std::endl;
    //std::cout << "GC maroc2 "<< i << " " << static_cast<unsigned>(maroc2ADC[i]) << std::endl;
    //std::cout << std::hex;
    std::cout << "GC maroc1 "<< i << " " << maroc1ADC[i] << " " << (&l1Header_xc0)->evid << std::endl;
    std::cout << "GC maroc2 "<< i << " " << maroc2ADC[i] << " " << (&l1Header_xc0)->evid << std::endl;
    }
    */
    // Check that the read was successful
    if( !(m_infile->good()) )
    {
        if( m_debug )
        {
            std::cout << "  End of file surpassed:  "
                << m_inputFileName << std::endl;
        }
        return false;
    }

    if( m_debug || (m_eventInFile%100000 == 0) )
    {
        std::cout << "  Read event " << m_eventInFile << " from "
            << m_inputFileName << std::endl;
    }

    // Parse the PMT data
    unsigned char PMTone[8];
    unsigned char PMTtwo[8];

    for(unsigned i = 0; i < 8; ++i)
    {
        unsigned ctr0 = i * 4;
        unsigned ctr1 = i * 4 + 1;
        PMTone[i] = data[ctr0];
        PMTtwo[i] = data[ctr1];
    }

    if( m_debug )
    {
        std::cout << "    PMT1 (col1) and PMT2 (col2)" << std::endl;
        for(unsigned i = 0; i < 8; ++i)
        {
            std::cout << "        " << static_cast<unsigned>(PMTone[i])
                <<"                        "
                << static_cast<unsigned>(PMTtwo[i]) << std::endl;
        }
    }

    // Get the interesting variables from headers to store in tree
    calculateVariables(PMTone, PMTtwo);

    if( m_obs_head )
    {
        m_treeVar.timestamp = reinterpret_cast<l1Header_xc0_old_t *>(&l1Header_xc0)->timestamp;
        m_treeVar.evid      = reinterpret_cast<l1Header_xc0_old_t *>(&l1Header_xc0)->evid;
    }
    else
    {
        m_treeVar.timestamp = l1Header_xc0.timestamp;
        m_treeVar.evid      = l1Header_xc0.evid;
    }

    // Go from a 40MHz clock to ns
    m_treeVar.timestamp = m_treeVar.timestamp * 25;
    m_treeVar.tag       = eventHeader.bunch;

    for(unsigned i = 0; i < 64; i+=1)
    {
	/*
        if (m_debug) std::cout << std::bitset<16>( maroc1ADC[i] ) << std::endl;
        unsigned short mask = maroc1ADC[i] >> 1;
        if (m_debug) std::cout << std::bitset<16>( mask ) << std::endl;
        if (m_debug) std::cout << std::bitset<16>( mask >> 1 ) << std::endl;
        if (m_debug) std::cout << std::bitset<16>( maroc1ADC[i] ^ mask ) << std::endl;
        unsigned short gmask;
        for (gmask = maroc1ADC[i] >> 1; gmask != 0; gmask = gmask >> 1)
        {
            maroc1ADC[i] = maroc1ADC[i] ^ gmask;
        }
        for (gmask = maroc2ADC[i] >> 1; gmask != 0; gmask = gmask >> 1)
        {
            maroc2ADC[i] = maroc2ADC[i] ^ gmask;
        }
	*/
        m_treeVar.MAR1_ADC[i] = maroc1ADC[63-i];
        m_treeVar.MAR2_ADC[i] = maroc2ADC[63-i];
        //std::cout << std::bitset<16>(m_treeVar.PMT1_ADC[i]) << "\t" << m_treeVar.PMT1_ADC[i] << "\t" << std::bitset<16>(m_treeVar.PMT2_ADC[i]) << "\t" << m_treeVar.PMT2_ADC[i] << std::endl;

        ui_pair pair_maroc1 = maroc1mapping[i];
        unsigned int pmt_adc_1 = pair_maroc1.first;
        unsigned int pmt_num_1 = pair_maroc1.second;

        ui_pair pair_maroc2 = maroc2mapping[i];
        unsigned int pmt_adc_2 = pair_maroc2.first;
        unsigned int pmt_num_2 = pair_maroc2.second;

        // reverse ordering in the PMT array
	/*
	if (pmt_num_1 == 1) m_treeVar.PMT1_ADC[64-pmt_adc_1] = maroc1ADC[i];
	if (pmt_num_1 == 2) m_treeVar.PMT2_ADC[64-pmt_adc_1] = maroc1ADC[i];
	if (pmt_num_2 == 1) m_treeVar.PMT1_ADC[64-pmt_adc_2] = maroc2ADC[i];
	if (pmt_num_2 == 2) m_treeVar.PMT2_ADC[64-pmt_adc_2] = maroc2ADC[i];
	*/
	/*
	if (pmt_num_1 == 1) m_treeVar.PMT1_ADC[pmt_adc_1-1] = maroc1ADC[63-i];
	if (pmt_num_1 == 2) m_treeVar.PMT2_ADC[pmt_adc_1-1] = maroc1ADC[63-i];
	if (pmt_num_2 == 1) m_treeVar.PMT1_ADC[pmt_adc_2-1] = maroc2ADC[63-i];
	if (pmt_num_2 == 2) m_treeVar.PMT2_ADC[pmt_adc_2-1] = maroc2ADC[63-i];
	*/

	if (pmt_num_1 == 1) m_treeVar.PMT1_ADC[pmt_adc_1-1] =  m_treeVar.MAR1_ADC[i];
	if (pmt_num_1 == 2) m_treeVar.PMT2_ADC[pmt_adc_1-1] =  m_treeVar.MAR1_ADC[i];
	if (pmt_num_2 == 1) m_treeVar.PMT1_ADC[pmt_adc_2-1] =  m_treeVar.MAR2_ADC[i];
	if (pmt_num_2 == 2) m_treeVar.PMT2_ADC[pmt_adc_2-1] =  m_treeVar.MAR2_ADC[i];

    }

    if( m_debug )
    {
        std::cout << "    m_treeVar.timestamp:  " << m_treeVar.timestamp << std::endl;
        std::cout << "    m_treeVar.evid:  " << m_treeVar.evid << std::endl;
    }

    // Advance the file pointer to the next event.
    m_pos += mdfHeader.length;
    if( m_debug )
    {
        std::cout << "    previous file position in " << m_inputFileName << ":  "
            << m_pos << std::endl;
        std::cout << "    file position in " << m_inputFileName << ":  "
            << m_pos << std::endl;
        std::cout << "    mdfHeader.length in " << m_inputFileName << ":  "
	    << mdfHeader.length << std::endl;
    }
    ++m_eventInFile;

    return true;
}


//________________________________________________________________________
unsigned int mdfReader::PMT(unsigned pmt, unsigned pixel) const
{
    // Check array bounds (hard-coded array limits)
    // Use the maximum allowed value as an error code.
    if( (pmt > 1) || (pixel > 63) )
        return std::numeric_limits<unsigned char>::max();
    int tmp;

    switch( pmt )
    {
        case 0 :
            tmp = m_treeVar.PMT1[pixel];

        case 1 :
            tmp = m_treeVar.PMT2[pixel];

    };

    return tmp;

    //return std::numeric_limits<unsigned char>::max();
}


//________________________________________________________________________
void mdfReader::calculateVariables(unsigned char* PMTone, unsigned char* PMTtwo)
{
    m_treeVar.totalHits = 0;
    m_treeVar.pmt1Hits = 0;
    m_treeVar.pmt2Hits = 0;

    for( unsigned row = 0; row < 8; row++ )
    {
        for( unsigned col = 0; col < 8; col++ )
        {
            unsigned ch=8*row+col;
            m_treeVar.PMT1[8*row+col] = (PMTone[row] & (1<<col)) ? 1 : 0;
            m_treeVar.PMT2[8*row+col] = (PMTtwo[row] & (1<<col)) ? 1 : 0;
            // We need a more general way to do the channel masking.
            // Hard-coded magic numbers that depend on a specific ordering of
            //   the input files will break.
            if(m_br_prefix=="B01_"){
                if(ch==36 || ch==37 || ch==38 || ch==39 || ch==44 || ch==45 || ch==46 || ch==47 || ch==52 || ch==53 || ch==54 || ch==55 || ch==60 || ch == 61 || ch==62 || ch==63){
                    m_treeVar.pmt1Hits += 0;
                }else{
                    m_treeVar.pmt1Hits += (m_treeVar.PMT1[8*row+col]);
                }
                m_treeVar.pmt2Hits += (m_treeVar.PMT2[8*row+col]);
            }else if(m_br_prefix=="B02_"){
                if(ch==32 || ch==33 || ch==40 || ch==41 || ch==48 || ch==49 || ch==56 || ch==57){
                    m_treeVar.pmt1Hits += 0;
                }else{
                    m_treeVar.pmt1Hits += (m_treeVar.PMT1[8*row+col]);
                }
                m_treeVar.pmt2Hits += (m_treeVar.PMT2[8*row+col]);
            }

            m_treeVar.totalHits += (m_treeVar.PMT1[8*row+col] + m_treeVar.PMT2[8*row+col]);
        }
    }

    return;
}


//________________________________________________________________________
bool mdfReader::initOutputTree( TTree *tt, std::string prefix )
{
    m_outputTree = tt;

    m_br_prefix = std::string( "" );

    if( prefix != std::string("") )
        m_br_prefix = prefix + std::string("_");

    m_treeVar.Branches( tt, m_br_prefix );

    return true;
}

//________________________________________________________________________
std::streampos mdfReader::evSeekCareful( unsigned long ev_first )
{
    std::streampos tpos      = 0;

    // Make sure that we remembered to open the file
    // (A paranoid check)
    if( !m_infile || !(m_infile->is_open()) )
    {
        std::cerr << "Call to mdfReader::evSeekCareful before file is opened."
            << std::endl;
        return tpos;
    }

    mdfHeader_t mdfHeader;
    for( unsigned long i = 0; i < ev_first; ++i )
    {
        m_infile->seekg( tpos );
        m_infile->read( (char *)&mdfHeader, sizeof(mdfHeader_t) );

        if( m_infile->eof() )
        {
            std::cerr << "Read beyon end of file at event " << i
                << " prior to specified starting event " << ev_first << std::endl;
            break;
        }

        tpos += mdfHeader.length;
    }

    return tpos;
}
