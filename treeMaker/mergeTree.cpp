// Copyright 2017 Konstantin Gizdov University of Edinburgh
// 2017 Adam Morris University of Edinburgh

#include <algorithm>
#include <iostream>
#include <memory>

#include "TFile.h"
#include "TTree.h"

void mergeTree(char * file_left, char * file_right, char * outputfile) {
    /**
     * file_left  - Corresponds to data on left  side of 2-inch MaPMT
     * file_right - Corresponds to data on right side of 2-inch MaPMT
     */
    std::cerr << "Merging left file " << file_left << " and right file " << file_right << std::endl;
    auto file1 = std::unique_ptr<TFile>(TFile::Open(file_left));
    auto tree1 = std::unique_ptr<TTree>(static_cast<TTree *>(file1->Get("mdfTree")));

    auto file2 = std::unique_ptr<TFile>(TFile::Open(file_right));
    auto tree2 = std::unique_ptr<TTree>(static_cast<TTree *>(file2->Get("mdfTree")));

    unsigned short PMT1_ADC_tree1[64], PMT1_ADC_tree2[64], PMT1_ADC_tree[64];

    tree1->SetBranchAddress("B01_PMT1_ADC", &PMT1_ADC_tree1);
    tree2->SetBranchAddress("B01_PMT1_ADC", &PMT1_ADC_tree2);

    auto file3 = std::unique_ptr<TFile>(TFile::Open(outputfile, "recreate"));
    TTree tree3("mdfTree", "Merged ADC tree");
    tree3.Branch("B01_PMT1_ADC", &PMT1_ADC_tree, "B01_PMT1_ADC[64]/s");

    unsigned nentries = std::min(tree1->GetEntries(), tree2->GetEntries());
    for (unsigned i = 0; i < nentries; ++i ) {
        tree1->GetEntry(i);
        tree2->GetEntry(i);
        for (unsigned j = 0; j < 64; ++j) {
            PMT1_ADC_tree[j] = (j / 4) % 2 == 0 ? PMT1_ADC_tree1[j] : PMT1_ADC_tree2[j];
        }
        tree3.Fill();
    }
    tree3.Write();
}

int main(int argc, char * argv[]) {
    if (argc != 4) {
        std::cerr << argv[0] << " <file left> <file right> <output file>" << std::endl;
        return 1;
    }
    char * file_left  = argv[1];
    char * file_right = argv[2];
    char * outputfile = argv[3];
    mergeTree(file_left, file_right, outputfile);
    return 0;
}
