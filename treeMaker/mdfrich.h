#if !defined(_mdfrich_h)
#define _mdfrich_h

#include <cstdint>

struct mdfHeader_t
{
  std::uint32_t length;
  std::uint32_t length_1;
  std::uint32_t length_2;
  std::uint32_t checksum;
  unsigned compression :8;
  unsigned size :4;
  unsigned version :4;
  unsigned dataType :8;
  unsigned spare :8;
};

struct eventHeader_t
{
  std::uint32_t mask[4];
  std::uint32_t run;
  std::uint32_t orbit;
  std::uint32_t bunch;
};

struct rawHeader_t
{
  std::uint16_t magic;
  std::uint16_t length;
  std::uint8_t type;
  std::uint8_t version;
  std::uint16_t source;
};

struct ingressHeader_t
{
  unsigned event :8;
  unsigned bxid  :8;
  unsigned channelMask :12;
  unsigned id :2;
  unsigned truncated : 1;
  unsigned :1;
};

struct l1Header_xc0_t // Testbeam 2014
{
  unsigned hpd :11;
  unsigned event :5;
  unsigned nzcount :11;
  unsigned zs :1;
  unsigned mode :1;
  unsigned ext :1;
  unsigned gt :1;
  unsigned reserved :1;
  std::uint32_t timestamp;
  std::uint32_t evid;
};

struct l1Header_xc0_old_t // Textbeam 2012
{
  unsigned hpd :11;
  unsigned event :5;
  unsigned nzcount :11;
  unsigned zs :1;
  unsigned mode :1;
  unsigned ext :1;
  unsigned gt :1;
  unsigned reserved :1;
  std::uint32_t timestamp;
  std::uint16_t evid;
  std::uint16_t frame;
};

struct l1Header_x81_t
{
  unsigned hpd :11;
  unsigned event :5;
  unsigned nzcount :11;
  unsigned zs :1;
  unsigned mode :1;
  unsigned ext :1;
  unsigned gt :1;
  unsigned reserved :1;
};

struct l1Header_x80_t
{
  unsigned nzcount :11;
  unsigned zs :1;
  unsigned mode :1;
  unsigned pad_0 :3;
  unsigned event :15;
  unsigned reserved :1;
};

struct l1Trailer_t
{
  std::uint32_t parity;
};

struct l0Header_t
{
  unsigned event :8;
  unsigned pad_0 :8;
  unsigned bxid :12;
  unsigned pad_1 :3;
  unsigned parity_0 :1;
  unsigned hpd :11;
  unsigned fifoOccupancy :4;
  unsigned fifoEmpty :1;
  unsigned fifoFull :1;
  unsigned testPattern :1;
  unsigned calibration :1;
  unsigned mode :1;
  unsigned otherGolStatus :1;
  unsigned pad_2 :10;
  unsigned parity_1 :1;
};

struct odin_t
{
  std::uint32_t run;
  std::uint32_t eventType;
  std::uint32_t orbit;
  std::uint32_t eventId[2];
  std::uint32_t gpsTime[2];
  unsigned status :24;
  unsigned errors :8;
  unsigned bunchId :12;
  unsigned :4;
  unsigned triggerType :3;
  unsigned readoutType :2;
  unsigned forcedTrigger :1;
  unsigned bxType :2;
  unsigned bunchCurrent :8;
};

#endif
