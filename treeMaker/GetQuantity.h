// Copyright 2017 Jennifer Zonneveld University of Edinburgh
// Copyright 2017 Konstantin Gizdov University of Edinburgh

#ifndef GET_QUANTITY_H_
#define GET_QUANTITY_H_

// C++ inc
#include <string>
#include <iostream>
#include <stdio.h>

// ROOT inc
#include "TROOT.h"
#include "TCanvas.h"
#include "TSystem.h"
#include "TString.h"
#include "TFile.h"
#include "TTree.h"
#include "TObject.h"
#include "TSpectrum.h"
#include "TH1F.h"
#include "TH2D.h"
#include "TStyle.h"

// Useful constants
#define PMTDIM 8
#define NPX PMTDIM*PMTDIM


//==============
// Get quantity
//==============
class GetQuantity {
 public:
    // Constr/destr
    GetQuantity();
    ~GetQuantity();

    // Process data from HV Run
    void processIllumRun(std::string input_filename, std::string output_filename);

    // Process data from Dark Run
    void processDarkRun(std::string input_filename, std::string output_filename);

};
#endif // GET_QUANTITY_H_
