#!/bin/bash

if [[ $# != 11 ]]; then
    exit -1
fi

RUN_NUMBER="$1"

DNA1="$2"
PMT1_1="$3"
PMT1_2="$4"

DNA2="$5"
PMT2_1="$6"
PMT2_2="$7"

INPUT_FOLDER="$8"

MAPPING1="$9"
MAPPING2="${10}"
LEDON="${11}"

# The following line redirects stdout and error messages. Can be changed.
# Do not redirect stdin. It used for communication with the GUI
LOG_FILE=${INPUT_FOLDER}/log/mdf_to_root_${RUN_NUMBER}_${PMT1_1}_${PMT1_2}_${PMT2_1}_${PMT2_2}.log
if [[ ${MAPPING1} == "2i" ]]; then
    LOG_FILE=${INPUT_FOLDER}/log/mdf_to_root_${RUN_NUMBER}_${PMT1_1}.log
fi
exec 1>>"${LOG_FILE}" 2>>"${LOG_FILE}"

# Change dir manually to the current folder
# cd /home/lhcbuser/lib-proxy
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# echo $DIR
cd "$DIR" || return 1

# echo $@

# conert mdf to root
./makeTree -o "${INPUT_FOLDER}/${PMT1_1}_${PMT1_2}_${RUN_NUMBER}_${DNA1}.root" -i "${INPUT_FOLDER}/feb-${DNA1}-${RUN_NUMBER}.mdf" -m "${MAPPING1}"
./makeTree -o "${INPUT_FOLDER}/${PMT2_1}_${PMT2_2}_${RUN_NUMBER}_${DNA2}.root" -i "${INPUT_FOLDER}/feb-${DNA2}-${RUN_NUMBER}.mdf" -m "${MAPPING2}"

# process root files
## check if 2 inch PMT
if [[ ${MAPPING1} == "2i" ]]; then
    # merge root files and then produce plots
    ./merge_2in.sh "${RUN_NUMBER}" "${DNA1}" "${DNA2}" "${PMT1_1}" "${INPUT_FOLDER}"
    ./root_to_png_2in.sh "${RUN_NUMBER}" "${DNA1}" "${DNA2}" "${PMT1_1}" "${INPUT_FOLDER}"
    ./get_quantities.sh "${LEDON}" "${RUN_NUMBER}" "${PMT1_1}" "${INPUT_FOLDER}"
else
    # produce plots
    ./root_to_png.sh "${RUN_NUMBER}" "${DNA1}" "${PMT1_1}" "${PMT1_2}" "${INPUT_FOLDER}"
    ./root_to_png.sh "${RUN_NUMBER}" "${DNA2}" "${PMT2_1}" "${PMT2_2}" "${INPUT_FOLDER}"
    ./get_quantities.sh "${LEDON}" "${RUN_NUMBER}" "${PMT1_1}" "${INPUT_FOLDER}"
    ./get_quantities.sh "${LEDON}" "${RUN_NUMBER}" "${PMT1_2}" "${INPUT_FOLDER}"
    ./get_quantities.sh "${LEDON}" "${RUN_NUMBER}" "${PMT2_1}" "${INPUT_FOLDER}"
    ./get_quantities.sh "${LEDON}" "${RUN_NUMBER}" "${PMT2_2}" "${INPUT_FOLDER}"
fi
