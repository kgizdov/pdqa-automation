#include "mdfTreeSeg.h"

mdfTreeSeg::mdfTreeSeg( void ) :
  timestamp(0)
  , tag(0)
  , totalHits(0)
  , pmt1Hits(0)
  , pmt2Hits(0)
  , evid(0)
  , fChain(0)
  , b_timestamp(0)
  , b_tag(0)
  , b_totalHits(0)
  , b_PMT1Hits(0)
  , b_PMT2Hits(0)
  , b_evid(0)
  , b_PMT1(0)
  , b_PMT2(0)
  , b_MAR1_ADC(0)
  , b_MAR2_ADC(0)
  , b_PMT1_ADC(0)
  , b_PMT2_ADC(0)
{
  return;
}


bool mdfTreeSeg::Branches(TTree *tree, std::string prefix)
{
  fChain = tree;
  m_prefix = prefix;

  std::string s_timestamp      = m_prefix + std::string("timestamp");
  std::string s_timestamp_type = s_timestamp + std::string("/i");
  b_timestamp = fChain->Branch( s_timestamp.c_str(), &timestamp, s_timestamp_type.c_str() );

  std::string s_tag      = m_prefix + std::string("tag");
  std::string s_tag_type = s_tag + std::string("/i");
  b_tag = fChain->Branch( s_tag.c_str(), &tag, s_tag_type.c_str() );

  std::string s_sum      = m_prefix + std::string("totalHits");
  std::string s_sum_type = s_sum + std::string("/s");
  b_totalHits = fChain->Branch( s_sum.c_str(), &totalHits, s_sum_type.c_str() );

  std::string s_pmt1Hits      = m_prefix + std::string("PMT1Hits");
  std::string s_pmt1Hits_type = s_pmt1Hits + std::string("/s");
  b_PMT1Hits = fChain->Branch( s_pmt1Hits.c_str(), &pmt1Hits, s_pmt1Hits_type.c_str() );

  std::string s_pmt2Hits      = m_prefix + std::string("PMT2Hits");
  std::string s_pmt2Hits_type = s_pmt2Hits + std::string("/s");
  b_PMT2Hits = fChain->Branch( s_pmt2Hits.c_str(), &pmt2Hits, s_pmt2Hits_type.c_str() );



  std::string s_evid      = m_prefix + std::string("evid");
  std::string s_evid_type = s_evid + std::string("/i");
  b_evid = fChain->Branch( s_evid.c_str(), &evid, s_evid_type.c_str() );

  std::string s_pmt1      = m_prefix + std::string("PMT1");
  std::string s_pmt1_type = s_pmt1 + std::string("[64]/b");
  b_PMT1 = fChain->Branch( s_pmt1.c_str(), &PMT1, s_pmt1_type.c_str() );

  std::string s_pmt2      = m_prefix + std::string("PMT2");
  std::string s_pmt2_type = s_pmt2 + std::string("[64]/b");
  b_PMT2 = fChain->Branch( s_pmt2.c_str(), &PMT2, s_pmt2_type.c_str() );

  std::string s_mar1_adc      = m_prefix + std::string("MAR1_ADC");
  std::string s_mar1_adc_type = s_mar1_adc + std::string("[64]/s");
  b_MAR1_ADC = fChain->Branch( s_mar1_adc.c_str(), &MAR1_ADC, s_mar1_adc_type.c_str() );

  std::string s_mar2_adc      = m_prefix + std::string("MAR2_ADC");
  std::string s_mar2_adc_type = s_mar2_adc + std::string("[64]/s");
  b_MAR2_ADC = fChain->Branch( s_mar2_adc.c_str(), &MAR2_ADC, s_mar2_adc_type.c_str() );

  std::string s_pmt1_adc      = m_prefix + std::string("PMT1_ADC");
  std::string s_pmt1_adc_type = s_pmt1_adc + std::string("[64]/s");
  b_PMT1_ADC = fChain->Branch( s_pmt1_adc.c_str(), &PMT1_ADC, s_pmt1_adc_type.c_str() );

  std::string s_pmt2_adc      = m_prefix + std::string("PMT2_ADC");
  std::string s_pmt2_adc_type = s_pmt2_adc + std::string("[64]/s");
  b_PMT2_ADC = fChain->Branch( s_pmt2_adc.c_str(), &PMT2_ADC, s_pmt2_adc_type.c_str() );

  return true;
}


bool mdfTreeSeg::SetBranchAddresses(TTree *tree, std::string prefix)
{
  fChain = tree;
  m_prefix = prefix;

  std::string s_timestamp      = m_prefix + std::string("timestamp");
  fChain->SetBranchAddress( s_timestamp.c_str(), &timestamp, &b_timestamp );

  std::string s_tag      = m_prefix + std::string("tag");
  fChain->SetBranchAddress( s_tag.c_str(), &tag, &b_tag );

  std::string s_sum      = m_prefix + std::string("totalHits");
  fChain->SetBranchAddress( s_sum.c_str(), &totalHits, &b_totalHits );

  std::string s_evid      = m_prefix + std::string("evid");
  fChain->SetBranchAddress( s_evid.c_str(), &evid, &b_evid );

  std::string s_pmt1      = m_prefix + std::string("PMT1");
  fChain->SetBranchAddress( s_pmt1.c_str(), &PMT1, &b_PMT1 );

  std::string s_pmt2      = m_prefix + std::string("PMT2");
  fChain->SetBranchAddress( s_pmt2.c_str(), &PMT2, &b_PMT2 );

  std::string s_mar1_adc      = m_prefix + std::string("MAR1_ADC");
  fChain->SetBranchAddress( s_mar1_adc.c_str(), &MAR1_ADC, &b_MAR1_ADC );

  std::string s_mar2_adc      = m_prefix + std::string("MAR2_ADC");
  fChain->SetBranchAddress( s_mar2_adc.c_str(), &MAR2_ADC, &b_MAR2_ADC );

  std::string s_pmt1_adc      = m_prefix + std::string("PMT1_ADC");
  fChain->SetBranchAddress( s_pmt1_adc.c_str(), &PMT1_ADC, &b_PMT1_ADC );

  std::string s_pmt2_adc      = m_prefix + std::string("PMT2_ADC");
  fChain->SetBranchAddress( s_pmt2_adc.c_str(), &PMT2_ADC, &b_PMT2_ADC );

  std::string s_pmt1Hits      = m_prefix + std::string("PMT1Hits");
  fChain->SetBranchAddress( s_pmt1Hits.c_str(), &pmt1Hits, &b_PMT1Hits );

  std::string s_pmt2Hits      = m_prefix + std::string("PMT2Hits");
  fChain->SetBranchAddress( s_pmt2Hits.c_str(), &pmt2Hits, &b_PMT2Hits );

  return true;
}

