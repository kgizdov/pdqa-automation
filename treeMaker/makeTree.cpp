#include "mdfrich.h"
#include "mdfReader.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string>
#include <iostream>
#include <sstream>
#include <vector>
#include <iomanip>
#include <fstream>
#include <cstdlib>
#include <algorithm>

#include <TFile.h>
#include <TStopwatch.h>

using namespace std;


const char *usage_string =
"Usage:  makeTree [-dhb] -o output.root -i input1.mdf [-i input2.mdf ...] [-f <first event>] [-l <last event>] [-n <number of events>]\n\
\n\
  -o : Exactly one output file should be specified.\n\
  -i : Each input file must be preceded by a -i flag.\n\
       At least one input file must be specified.\n\
       Up to 98 input files can be specified.\n\
  -h : Print this message and exit.\n\
  -d : Turn on verbose debugging output.\n\
  -m : Choose which mapping to use (AB, CD or 2i).\n\
  -b : The input MDF files use the o[b]solete L1 event header format.\n\
  -f, -l : Zero-based indexing is used for <first event> and <last event>.\n\
\n\
  If no -f, -l, or -n flag is specified, all events will be processed.\n\
  If -f is not specified, processing always begins at event 0.\n\
  If -l is not specified, processing continues until the end of one\n\
    of the input files is reached or until <number of events> events have\n\
    been processed.\n\
  If both -l and -n are specified, the -n flag is ignored and the number\n\
    of events processed will always correspond to the interval between\n\
    the specified last event and the first event.\n\
\n\
Examples of event range specifications:\n\
  makeTree -o output.root -i input1.mdf -n 1000\n\
      Process the first 1000 events of input1.mdf, beginning with event 0\n\
      and ending with event 999.\n\
\n\
  makeTree -o output.root -i input1.mdf -f 3000 -n 1000\n\
      Process 1000 events of input1.mdf beginning with event 3000 and\n\
      ending with event 3999.\n\
\n\
  makeTree -o output.root -i input1.mdf -f 3000 -l 4000\n\
      Process 1001 events of input1.mdf beginning with event 3000 and\n\
      ending with event 4000.";


void printUsage( ostream &lclout = std::cout )
{
  lclout << usage_string << std::endl;
  return;
}


bool evIDSynchronizedReadOne( std::vector< mdfReader * > &v_mdfReaders, const bool short_evids = false )
{
  bool readsuccess = true;

  // Read the event and tabulate the evids's
  std::vector< mdfReader * >::iterator imdf;
  for( imdf = v_mdfReaders.begin(); imdf != v_mdfReaders.end(); ++imdf )
    readsuccess = readsuccess && (*imdf)->readOne();

  // Stop if the end of one of the files was reached.
  if( !readsuccess )
    return readsuccess;

  bool synchronized = false;
  while( readsuccess && !synchronized )
  {
    std::vector< unsigned int > v_evids;
    for( imdf = v_mdfReaders.begin(); imdf != v_mdfReaders.end(); ++imdf )
      v_evids.push_back((*imdf)->evid());

    std::sort( v_evids.begin(), v_evids.end() );
    if( v_evids.front() == v_evids.back() )
    {
      synchronized = true;
    }
    else if( short_evids && (v_evids.back() - v_evids.front() > 32767) )
    {
      // Special case for obsolete L1 header format in which evid is 2 bytes.
      // If the difference between the first and last evid is greater than
      //   half of the allowed range of evids, then it is likely that events
      //   were dropped from the files with the smaller evids, rather than
      //   from the files with the larger evids.
      // In this case, burn events from the files with the largest evids until
      //    they cross the evid rollover.
      std::cout << "Desynchronization at evid " << v_evids.back()
                << " that probably crosses the evid rollover to "
                << v_evids.front() << std::endl;
      for( imdf = v_mdfReaders.begin(); imdf != v_mdfReaders.end(); ++imdf )
      {
        unsigned init_evid = (*imdf)->evid();
        unsigned nev_burned = 0;
        if( init_evid - v_evids.front() > 32767 )
        {
          // Burn events to the rollover
          unsigned prev_evid = (*imdf)->evid() - 1;
          while( readsuccess && ((*imdf)->evid() > prev_evid) )
          {
            prev_evid = (*imdf)->evid();
            readsuccess = readsuccess && (*imdf)->readOne();
            ++nev_burned;
          }

          if( !readsuccess )
          {
            std::cout << "   End of file reached while burning "
                      << (*imdf)->inputFileName() << std::endl;
            break;
          }
          else if( nev_burned )
          {
            std::cout << "   " << (*imdf)->inputFileName()
                      << " burned " << nev_burned << " events from evid "
                      << init_evid << " to evid " << (*imdf)->evid() << std::endl;
          }
        }
      }
    }
    else
    {
      unsigned target_evid = v_evids.back();
      std::cout << "Desynchronization at evid " << v_evids.front()
                << ", burning to evid " << target_evid << std::endl;

      for( imdf = v_mdfReaders.begin(); imdf != v_mdfReaders.end(); ++imdf )
      {
        unsigned init_evid = (*imdf)->evid();
        unsigned nev_burned = 0;
        while( readsuccess && ((*imdf)->evid() < target_evid) )
        {
          readsuccess = readsuccess && (*imdf)->readOne();
          ++nev_burned;
        }

        if( !readsuccess )
        {
          std::cout << "   End of file reached while burning "
                    << (*imdf)->inputFileName() << std::endl;
          break;
        }
        else if( nev_burned )
        {
          std::cout << "   " << (*imdf)->inputFileName()
                    << " burned " << nev_burned << " events from evid "
                    << init_evid << " to evid " << (*imdf)->evid() << std::endl;
        }
      }
    }
  }

  return readsuccess;
}


int main(int argc, char** argv){
  bool debug = false;   // Debug flag
  bool obs_header = false;
  std::string mapping;

  std::string outfilename;
  std::string infilename;
  std::vector<std::string> v_infilenames;

  // Run range to process.
  long evFirst = 0, evLast = -1, evN = -1;

  int c;
  extern char* optarg;
  // extern int optind;  // not sure why this is here

  /*____________________________Parse Command Line___________________________*/
  while((c = getopt(argc,argv,"o:i:f:l:n:dhb:m:")) != -1){
    switch(c){
    case 'h': // help option
      printUsage();
      return 0;
      break;
    case 'o':
      outfilename = optarg;
      cout << "output filename: " << outfilename << endl;
      break;
    case 'i':
      infilename = optarg; // + std::string(".mdf");
      v_infilenames.push_back(infilename);
      break;
    case 'f':
      evFirst = std::atol(optarg);
      break;
    case 'l':
      evLast = std::atol(optarg);
      break;
    case 'n':
      evN = std::atol(optarg);
      break;
    case 'd':
      debug = true;
      break;
    case 'b':
      obs_header = true;
      break;
    case 'm':
      cout << "Using mapping: " << optarg << endl;
      mapping = optarg;
      cout << "Using mapping: " << mapping << endl;
      break;
    case '\?':
      cout<<"invalid option"<<endl;
      /* FALLTHRU */
      // __attribute__((fallthrough));  // this syntax is not supported in earlier version of GCC
    default: // unknown option flag
      printf("Error!!!! Unknown option -%c\n",c);
      cout << "Example: "<< argv[0]<<" -o output.root -i input1.mdf [-i input2.mdf]" << endl;
      return 0;
    }
  }

  if(v_infilenames.empty())
  {
    std::cerr <<"Input filename not specified." << std::endl;
    std::cerr << std::endl;
    printUsage(std::cerr);
    exit(EXIT_FAILURE);
  }
  if( outfilename.empty() )
  {
    std::cerr << "Outfile filename not specified." << std::endl;
    std::cerr << std::endl;
    printUsage(std::cerr);
    exit(EXIT_FAILURE);
  }
  if(v_infilenames.size() > 98)
  {
    std::cerr << "Too many input files!  Asked to process "
              << v_infilenames.size() << ", maximum allowed is 98." << std::endl;
    std::cerr << std::endl;
    printUsage(std::cerr);
    exit(EXIT_FAILURE);
  }


  if( evLast > 0 )
  {
    long lcl_evN = evLast - evFirst + 1;

    if( lcl_evN < 0 )
    {
      std::cerr << "Specified last event " << evLast
                << " comes before first event " << evFirst << std::endl;
    }
    else if( (evN > -1) && (lcl_evN > evN) )
    {
      std::cerr << "Overriding specified number of events " << evN
                << " to values specified by specified (first, last) = ("
                << evFirst << ", " << evLast << ")." << std::endl;
    }

    evN = lcl_evN;
  }

  if( evN > -1 )
  {
    std::cout << "Reading " << evN << " events beginning at event "
              << evFirst << std::endl;
  }
  else
  {
    std::cout << "Reading all events after event " << evFirst << std::endl;
  }


  // Start a timer.
  TStopwatch lcl_cput;
  lcl_cput.Start(true);

  // Create a mdfReader object for each input file.
  std::vector< mdfReader * > v_mdfReaders;
  std::vector<std::string>::iterator ifname;
  for( ifname = v_infilenames.begin(); ifname != v_infilenames.end(); ++ifname )
  {
    // Some diagnostic output from original
    struct stat fileSize;
    stat(ifname->c_str(), &fileSize);
    std::cout << "MaPMT input file:  " << *ifname << std::endl;
    std::cout << "  Size of file:  " << fileSize.st_size << std::endl;
    int dataNo = fileSize.st_size / 104;        // magic number
    std::cout << "  Number of data entries:  " << dataNo << std::endl;

    // Initialize the reader class
    //mdfReader *mdf = new mdfReader( *ifname, obs_header, debug );
    mdfReader *mdf = new mdfReader( *ifname, obs_header, debug, mapping );
    v_mdfReaders.push_back(mdf);
  }


  // Open the output Root file and create an empty TTree.
  TFile *fout = TFile::Open( outfilename.c_str(), "recreate" );
  TTree *tt = new TTree( "mdfTree", "mdfTree" );

  UInt_t nHits = 0;
  tt->Branch( "sumTotalHits", &nHits, "sumTotalHits/i" );

  // Initialize all of the mdfReader objects.
  // I will assume that there are at most 98 of them.
  bool initsuccess = true;
  unsigned idx = 1;
  std::vector< mdfReader * >::iterator imdf;
  for( imdf = v_mdfReaders.begin(); imdf != v_mdfReaders.end(); ++imdf )
  {
    // Prefix for TTree branch names associated with the MDF file.
    // Will just use a 1-based ordinal corresponding to its position in
    //   the command line for now.
    std::stringstream ss_pfx;
    ss_pfx << "B" << std::setfill('0') << std::setw(2) << idx;
    initsuccess = initsuccess && (*imdf)->initialize( tt, ss_pfx.str(), evFirst );
    ++idx;
  }

  if( !initsuccess )
  {
    std::cerr << "Failed to initialize at least one MDF file reader." << std::endl;
    return 0;
  }

  // Run the event loop
  bool short_evids = obs_header;
  bool readsuccess = true;
  unsigned nevts = 0;
  while( readsuccess && ((evN < 0) || (nevts < evN)) )
  {
    readsuccess = evIDSynchronizedReadOne( v_mdfReaders, short_evids );

    if( readsuccess )
    {
      // Compute total hits across all input files
      nHits = 0;
      for( imdf = v_mdfReaders.begin(); imdf != v_mdfReaders.end(); ++imdf )
        nHits += (*imdf)->sum();

      tt->Fill();

      ++nevts;
    }
  }

  std::cout << "Read " << nevts << " total events;  entries in output tree = "
            << tt->GetEntries() << std::endl;

  // Find the evid of the last event in each file if the job was to read to
  //   the end of input files.
  std::cout << std::endl;
  if( evN < 0 )
  {
    for( imdf = v_mdfReaders.begin(); imdf != v_mdfReaders.end(); ++imdf )
    {
      unsigned last_evid = (*imdf)->evid();
      unsigned nextra = 0;
      std::cout << "Last evid " << last_evid << " read and written from file "
                << (*imdf)->inputFileName() << std::endl;
      while( (*imdf)->readOne() )
      {
        last_evid = (*imdf)->evid();
        ++nextra;
      }

      std::cout << "  " << nextra
                << " events after last event written.  Last evid "
                << last_evid << " in file " << (*imdf)->inputFileName()
                << std::endl;
    }
  }
  std::cout << std::endl;


  // Clean up
  // Close the Root file.
  //tt->Write();
  //if( debug ) std::cout << "Wrote tree." << std::endl;
  fout->Write();
  if( debug ) std::cout << "Wrote file." << std::endl;
  fout->Close();
  if( debug ) std::cout << "Closed file." << std::endl;

  // Finalize and destroy the mdfReader objects.
  while( !v_mdfReaders.empty() )
  {
    mdfReader *goner = v_mdfReaders.back();
    goner->finalize();
    delete goner;
    v_mdfReaders.pop_back();
  }

  // How long did it take?
  lcl_cput.Stop();
  std::cout << "\nTook me "<< lcl_cput.CpuTime() << " secs" << std::endl;

  return 0;
}
