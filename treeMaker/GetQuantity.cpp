// Copyright 2017 Jennifer Zonneveld University of Edinburgh
// Copyright 2017 Konstantin Gizdov University of Edinburgh

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "GetQuantity.h"

/////////////////
// Constructor //
/////////////////
GetQuantity::GetQuantity() {}


////////////////
// Destructor //
////////////////
GetQuantity::~GetQuantity() {}


////////////////////
// Process HV Run //
////////////////////
void GetQuantity::processIllumRun(std::string input_filename, std::string output_filename) {
  const Float_t xmin = 0., xmax = 256.;  // spectrum histo. binning
  const Int_t nBins = xmax - xmin;
  uint16_t PMT_ADC[NPX];
  float thr = 1e-3;                    // for finding peaks w/ height >= thr*max_height
  float sigma = 5.;                    // for finding peaks separated with sigma
  TSpectrum *s = new TSpectrum(10);
  double ped, gain, peakval;

  // Vector with inputfiles
  std::vector<TFile*> inputfiles;

  // Open data file
  TFile *inputfile = TFile::Open(input_filename.c_str(), "READ");
  if (inputfile == nullptr) {
    std::cerr << "[ERROR] Cannot open datafile: " << input_filename << std::endl;
    return;
  }

  // Add inputfiles to the vector
  inputfiles.push_back(inputfile);

  // Output file
  TFile *fout = TFile::Open(output_filename.c_str(), "RECREATE");
  TTree *tree = new TTree("tree", "tree");

  // Variables to be evaluated
  tree->Branch("Gain", &gain);
  tree->Branch("Pedestal", &ped);
  tree->Branch("PeakToValley", &peakval);

  // Loop over inputfiles
  for (TFile* input : inputfiles) {
    int i = 1;   // inputfile integer for debugging

    // Load ADC values
    TTree *checktree = static_cast<TTree *>(input->Get("mdfTree"));
    TBranch *tdata = static_cast<TBranch *>(checktree->GetBranch("B01_PMT1_ADC"));
    checktree->SetBranchAddress("B01_PMT1_ADC", &PMT_ADC);
    unsigned nEvents = tdata->GetEntries();

    // Vector of spectrum histos
    std::vector<TH1F*> hspectra;
    for (unsigned ipx = 0; ipx < NPX; ipx++) {
      TString hname = Form("hdata_1_px%i", ipx);
      TH1F *h = new TH1F(hname, Form("Spectrum PMT 1 px%i", ipx), nBins, xmin, xmax);
      hspectra.push_back(h);
    }

    // Fill spectrum histos
    for (unsigned iev = 0; iev < nEvents; iev++) {
      checktree->GetEvent(iev);
      for (unsigned ipx = 0; ipx < NPX; ipx++) {
        hspectra[ipx]->Fill(Float_t(PMT_ADC[ipx]));
      }
    }

    // Process each pixel
    for (unsigned ipx = 0; ipx < NPX; ipx++) {
      TH1F *hdata = hspectra[ipx];

      // Find peaks
      int npeaks = s->Search(hdata, sigma, "nodraw", thr);
      double *x_peaks = s->GetPositionX();
      double *y_peaks = s->GetPositionY();
      ped = 0., gain = 0., peakval = 0.;
      double ypeak = 0., yvalley = 0.;
      if (npeaks == 1) ped = x_peaks[0];
      if (npeaks >= 2) {
        ped = x_peaks[0];
        gain = x_peaks[1] - ped;
        ypeak = y_peaks[1];
      }

      // Valley search
      if (npeaks >= 2) {
        int binmin = hdata->GetXaxis()->FindBin(x_peaks[0]);
        int binmax = hdata->GetXaxis()->FindBin(x_peaks[1]);
        yvalley = hdata->GetMaximum();
        for (Int_t bin = binmin; bin < binmax; bin++) {
          if (hdata->GetBinContent(bin) < yvalley) {
            yvalley = hdata->GetBinContent(bin);
          }
        }
      }
      if (gain == 0.) {  // check for dead channels if not dark run
        std::cout << "[WARNING] processData(): file " << i << " has dead pixel " << ipx << std::endl;
      }

      // Peak-to-valley ratio
      peakval = ypeak/yvalley;
      tree->Fill();
    }

    // Delete spectrum histos
    for (unsigned ipx = 0; ipx < NPX; ipx++) {
      hspectra[ipx]->Delete();
    }
    i++;
  }

  // Draw hists
  double gain_h, peakTv_h, pedestal_h;
  std::ofstream gains_f(output_filename.substr(0, output_filename.size()-5) + "_gains.txt", std::ios::out);
  std::ofstream pedestals_f(output_filename.substr(0, output_filename.size()-5) + "_pedestals.txt", std::ios::out);
  std::ofstream peaks_f(output_filename.substr(0, output_filename.size()-5) + "_PeakToValleys.txt", std::ios::out);
  gStyle->SetOptStat(kFALSE);
  tree->SetBranchAddress("Gain", &gain_h);
  tree->SetBranchAddress("Pedestal", &pedestal_h);
  tree->SetBranchAddress("PeakToValley", &peakTv_h);
  TH2D gain_hist("Gains", "Gains", 8, 0, 8, 8, 0, 8);
  TH2D pedestal_hist("Pedestals", "Pedestals", 8, 0, 8, 8, 0, 8);
  TH2D peakTv_hist("PeakToValleys", "PeakToValleys", 8, 0, 8, 8, 0, 8);
  for (unsigned i = 0; i < 8; ++i) {
    for (unsigned j = 0; j < 8; ++j) {
      tree->GetEntry(i + j*8);
      gain_hist.Fill(i, 7-j, gain_h);
      gains_f << gain_h << ",";
      pedestal_hist.Fill(i, 7-j, pedestal_h);
      pedestals_f << pedestal_h << ",";
      peakTv_hist.Fill(i, 7-j, peakTv_h);
      peaks_f << peakTv_h << ",";
    }
  }
  gains_f << std::endl;
  pedestals_f << std::endl;
  peaks_f << std::endl;
  TCanvas gain_c("Gains", "Gains", 200, 50, 800, 800),
          pedestal_c("Pedestals", "Pedestals", 200, 50, 800, 800),
          peak_c("PeakToValleys", "PeakToValleys", 200, 50, 800, 800);
  gain_c.cd();
  gain_hist.SetMinimum(30.);
  gain_hist.SetMaximum(150.);
  gain_hist.Draw("COLZ");
  gain_c.SaveAs((output_filename.substr(0, output_filename.size()-5) + "_gains.pdf").c_str());
  pedestal_c.cd();
  pedestal_hist.SetMinimum(10.);
  pedestal_hist.SetMaximum(30.);
  pedestal_hist.Draw("COLZ");
  pedestal_c.SaveAs((output_filename.substr(0, output_filename.size()-5) + "_pedestals.pdf").c_str());
  peak_c.cd();
  peakTv_hist.SetMinimum(1.);
  peakTv_hist.SetMaximum(7.);
  peakTv_hist.Draw("COLZ");
  peak_c.SaveAs((output_filename.substr(0, output_filename.size()-5) + "_peak_to_valleys.pdf").c_str());

  tree->Write();
  fout->Write();
  fout->Close();
}


//////////////////////
// Process Dark Run //
//////////////////////
void GetQuantity::processDarkRun(std::string input_filename, std::string output_filename) {
  const Float_t xmin = 0., xmax = 256.;  // spectrum histo. binning
  const Int_t nBins = xmax - xmin;
  uint16_t PMT_ADC[NPX];
  float thr = 1e-3;                    // for finding peaks w/ height >= thr*max_height
  float sigma = 5.;                    // for finding peaks separated with sigma
  TSpectrum *s = new TSpectrum(10);
  double ped;
  int darkcount;

  // Vector with inputfiles
  std::vector<TFile*> inputfiles;

  // Open data file
  TFile *inputfile = TFile::Open(input_filename.c_str(), "READ");
  if (inputfile == nullptr) {
    std::cerr << "[ERROR] Cannot open datafile: " << input_filename << std::endl;
    return;
  }

  // Add inputfiles to the vector
  inputfiles.push_back(inputfile);

  // Output file
  TFile *fout = new TFile(output_filename.c_str(), "RECREATE");
  TTree *tree = new TTree("tree", "tree");

  // Variables to be evaluated
  tree->Branch("Pedestal", &ped);
  tree->Branch("DarkCount", &darkcount);

  // Loop over inputfiles
  for (TFile* input : inputfiles) {
    // Load ADC values
    TTree *checktree = static_cast<TTree *>(input->Get("mdfTree"));
    TBranch *tdata = static_cast<TBranch *>(checktree->GetBranch("B01_PMT1_ADC"));
    checktree->SetBranchAddress("B01_PMT1_ADC", &PMT_ADC);
    unsigned nEvents = tdata->GetEntries();

    // Vector of spectrum histos
    std::vector<TH1F*> hspectra;
    for (unsigned ipx = 0; ipx < NPX; ipx++) {
      TString hname = Form("hdata_1_px%i", ipx);
      TH1F *h = new TH1F(hname, Form("Spectrum PMT 1 px%i", ipx), nBins, xmin, xmax);
      hspectra.push_back(h);
    }

    // Fill spectrum histos
    for (unsigned iev = 0; iev < nEvents; iev++) {
      checktree->GetEvent(iev);
      for (unsigned ipx = 0; ipx < NPX; ipx++) {
          hspectra[ipx]->Fill(Float_t(PMT_ADC[ipx]));
      }
    }

    // Process each pixel
    for (unsigned ipx = 0; ipx < NPX; ipx++) {
      TH1F *hdata = hspectra[ipx];

      // Find peaks
      int npeaks = s->Search(hdata, sigma, "nodraw", thr);
      double *x_peaks = s->GetPositionX();
      ped = 0., darkcount = 0.;
      if (npeaks == 1) ped = x_peaks[0];
      if (npeaks >= 2) std::cout << "No Dark Run" << std::endl;

      // Dark count is 10 ADC values below and above pedestal peak
      darkcount = nEvents - (Int_t)hdata->Integral(ped - 10., ped + 10.);
      tree->Fill();
    }

    // Delete spectrum histos
    for (unsigned ipx = 0; ipx < NPX; ipx++) {
          hspectra[ipx]->Delete();
    }
  }

  // Draw hists
  double pedestal_h;
  int darkcount_h;
  std::ofstream darkcount_f(output_filename.substr(0, output_filename.size()-5) + "_darkcounts.txt", std::ios::out);
  std::ofstream pedestal_f(output_filename.substr(0, output_filename.size()-5) + "_pedestals.txt", std::ios::out);
  gStyle->SetOptStat(kFALSE);
  tree->SetBranchAddress("DarkCount", &darkcount_h);
  tree->SetBranchAddress("Pedestal", &pedestal_h);
  TH2D darkcount_hist("Gains", "Gains", 8, 0, 8, 8, 0, 8);
  TH2D pedestal_hist("Pedestals", "Pedestals", 8, 0, 8, 8, 0, 8);
  for (unsigned i = 0; i < 8; ++i) {
    for (unsigned j = 0; j < 8; ++j) {
      tree->GetEntry(i + j*8);
      darkcount_hist.Fill(i, 7-j, darkcount_h);
      darkcount_f << darkcount_h << ",";
      pedestal_hist.Fill(i, 7-j, pedestal_h);
      pedestal_f << pedestal_h << ",";
    }
  }
  darkcount_f << std::endl;
  pedestal_f << std::endl;
  TCanvas darkcount_c("DarkCounts", "DarkCounts", 200, 50, 800, 800),
          pedestal_c("Pedestals", "Pedestals", 200, 50, 800, 800);
  darkcount_c.cd();
  darkcount_hist.SetMinimum(100);
  darkcount_hist.SetMaximum(10000);
  darkcount_hist.Draw("COLZ");
  darkcount_c.SaveAs((output_filename.substr(0, output_filename.size()-5) + "_darkcounts.pdf").c_str());
  pedestal_c.cd();
  pedestal_hist.SetMaximum(30.);
  pedestal_hist.SetMinimum(10.);
  pedestal_hist.Draw("COLZ");
  pedestal_c.SaveAs((output_filename.substr(0, output_filename.size()-5) + "_pedestals.pdf").c_str());

  tree->Write();
  fout->Write();
  fout->Close();
}
