// Copyright 2017 Jennifer Zonneveld University of Edinburgh
// Copyright 2017 Konstantin Gizdov University of Edinburgh

#include "GetQuantity.h"

int main(int argc, char const *argv[]) {
    if (argc != 4) {
        std::cerr << "Usage: " << argv[0] << " <run type> <input file> <output file>" << std::endl;
        std::cerr << "run types:" << std::endl;
        std::cerr << "        - illuminated/dark" << std::endl;
        std::cerr << "        - on/off (illuminated/dark)" << std::endl;
        std::cerr << "        - yes/no (illuminated/dark)" << std::endl;
        std::cerr << "        - 1/0    (illuminated/dark)" << std::endl;
        return 1;
    }
    std::string run_type(argv[1]), inputfile(argv[2]), ouputfile(argv[3]);
    GetQuantity quant;
    if (run_type == "1" || run_type == "on" || run_type == "yes" || run_type == "illuminated") {
        quant.processIllumRun(inputfile, ouputfile);
    } else {
        quant.processDarkRun(inputfile, ouputfile);
    }
    return 0;
}
