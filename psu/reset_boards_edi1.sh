#!/bin/bash

# WARNING!
# Only for Edinburgh station 1
# PSU topology is assumed

if [[ $# != 1 ]]; then
    exit -1
fi

INPUT_FOLDER="$1"

# The following line redirects stdout and error messages. Can be changed.
# Do not redirect stdin. It used for communication with the GUI
LOG_FILE=${INPUT_FOLDER}/log/board_reset.log
exec 1>>"${LOG_FILE}" 2>>"${LOG_FILE}"

# Change dir manually to the current folder
# cd /home/lhcbuser/lib-proxy
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# echo $DIR
cd "$DIR" || return 1

declare -a devices_=( /dev/ttyACM* )
declare -a devices=()
for device in "${devices_[@]}"; do
    if [[ $device = '/dev/ttyACM*' ]]; then
        continue
    fi
    devices+=($device)
done

echo "Will talk to PSU(s) on ports:"
echo "${devices[@]}"

# HV_VOLTAGE=24
# SL_VOLTAGE=12

# MAROC_VOLTAGE=3.55
# CHIMA_VOLTAGE=3.35

# MAROC_CHANNEL=2
# CHIMA_CHANNEL=1

echo "Devices identify as "
GET_ID1='echo -ne "*IDN?\n" | socat STDIO "${devices[0]}"'
GET_ID2='echo -ne "*IDN?\n" | socat STDIO "${devices[1]}"'
eval "${GET_ID1}"
eval "${GET_ID2}"
sleep 2

ALL_OFF1='echo -ne "OPALL 0; OPALL 0; OPALL 0\n" | socat STDIO "${devices[0]}"'
ALL_OFF2='echo -ne "OPALL 0; OPALL 0; OPALL 0\n" | socat STDIO "${devices[1]}"'

ALL_ON1='echo -ne "OPALL 1; OPALL 1; OPALL 1\n" | socat STDIO "${devices[0]}"'
ALL_ON2='echo -ne "OPALL 1; OPALL 1; OPALL 1\n" | socat STDIO "${devices[1]}"'

case "$GET_ID1" in
    *422507* )
        # turn all off
        echo "Switching off all channels..."
        eval "${ALL_OFF1}"
        sleep 1
        eval "${ALL_OFF2}"
        sleep 5

        # turn all off
        echo "Switching on all channels..."
        eval "${ALL_ON2}"
        sleep 1
        eval "${ALL_ON1}"
        sleep 5
        ;;
    * )
        # turn all off
        echo "Switching off all channels..."
        eval "${ALL_OFF2}"
        sleep 1
        eval "${ALL_OFF1}"
        sleep 5

        # turn all off
        echo "Switching on all channels..."
        eval "${ALL_ON1}"
        sleep 1
        eval "${ALL_ON2}"
        sleep 5
        ;;
esac

for device in "${devices[@]}"; do
    STATUS='echo -ne "*IDN?; V1?; V1O?; I1?; I1O?; V2?; V2O?; I2?; I2O?; V3?; V3O?; I3?; I3O?;\n" | socat STDIO "${device}"'
    echo "Device status is "
    eval "${STATUS}"
    RELEASE_TO_LOCAL='echo -ne "LOCAL\n" | socat STDIO "${device}"'
    echo "Releasing to local operation... "
    eval "${RELEASE_TO_LOCAL}"
done

echo "Done."
