#!/bin/bash

# Currently only works for Padova stations

if [[ $# != 1 ]]; then
    exit -1
fi

INPUT_FOLDER="$1"

# The following line redirects stdout and error messages. Can be changed.
# Do not redirect stdin. It used for communication with the GUI
LOG_FILE=${INPUT_FOLDER}/log/board_reset.log
exec 1>>"${LOG_FILE}" 2>>"${LOG_FILE}"

# Change dir manually to the current folder
# cd /home/lhcbuser/lib-proxy
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# echo $DIR
cd "$DIR" || return 1

declare -a devices_=( /dev/ttyACM* )
declare -a devices=()
for device in "${devices_[@]}"; do
    if [[ $device = '/dev/ttyACM*' ]]; then
        continue
    fi
    devices+=($device)
done

echo "Will talk to PSU(s) on ports:"
echo "${devices[@]}"

# HV_VOLTAGE=24
# SL_VOLTAGE=12

# MAROC_VOLTAGE=3.55
# CHIMA_VOLTAGE=3.35

# MAROC_CHANNEL=2
# CHIMA_CHANNEL=1

for device in "${devices[@]}"; do
    echo "${device} identifies as "
    GET_ID='echo -ne "*IDN?\n" | socat STDIO "${device}"'
    sleep 5
    eval "${GET_ID}"

    # turn all off
    echo "Switching off channels..."
    ALL_OFF='echo -ne "OPALL 0; OPALL 0; OPALL 0\n" | socat STDIO "${device}"'
    eval "${ALL_OFF}"
    sleep 5

    # turn MAROC ON
    echo "Powering on MAROC channels..."
    ON_1='echo -ne "OP2 1; OP2 1; OP2 1\n" | socat STDIO "${device}"'
    eval "${ON_1}"
    sleep 5
    # turn CHIMAERA ON
    echo "Powering on Chimaera channels..."
    ON_2='echo -ne "OP1 1; OP1 1; OP1 1\n" | socat STDIO "${device}"'
    eval "${ON_2}"

    # print status
    sleep 2  # wait for power to stabilise
    STATUS='echo -ne "*IDN?; V1?; V1O?; I1?; I1O?; V2?; V2O?; I2?; I2O?; V3?; V3O?; I3?; I3O?;\n" | socat STDIO "${device}"'
    echo "Device status is "
    eval "${STATUS}"
    RELEASE_TO_LOCAL='echo -ne "LOCAL\n" | socat STDIO "${device}"'
    echo "Releasing to local operation... "
    eval "${RELEASE_TO_LOCAL}"
done

echo "Done."
